<?php
function getDatePHPWithTime($dateVar,$type) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $date = date("Y-m-d", strtotime($dateVar));
    $time = date("H-i-s", strtotime("23:59:59"));

    return $date." ".$time;
}
function getTimeFormats($timeInDB)
{
     return date("G:i",strtotime($timeInDB));
}
function getTimeFormats2($timeInDB)
{
     return date("Hi",strtotime($timeInDB));
}
function getDateFormats($dateInDB)
{
     return date("d M Y",strtotime($dateInDB));
}
function getCostCenterName($ccId,$conn)
{

     $sqlA = " SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$ccId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['costCenterName'];      
               return $username;        
          }
     }
     else
     {
          return $conn->error;
     }
}
function getCompanyName($placeId,$conn)
{
     $sqlA = " SELECT companyName FROM company WHERE companyID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['companyName'];      
               return $username;        
          }
     }
     else
     {
          return $conn->error;
     }
}
function getZoneName($placeId,$conn)
{
     $sqlA = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['zonesName'];      
               return $username;        
          }
     }
     else
     {
          return $conn->error;
     }
}
     
function getPlaceName($placeId,$conn)
{
     $sqlA = " SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['pointzonePlaceName'];      
               return $username;        
          }
     }
     else
     {
          return $conn->error;
     }
}
function getTruckDetails($placeId,$conn)
{
     $truckDetail = array();

     $sqlA = " SELECT truckPlateNo,truckCapacity FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          {
               array_push($truckDetail,$row3['truckPlateNo']) ;
               array_push($truckDetail,$row3['truckCapacity']) ;      
               return $truckDetail;        
          }
     }
     else
     {
          return $conn->error;
     }
}
function getDriverName($placeId,$conn)
{
     $sqlA = " SELECT driverName FROM driver WHERE driverID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['driverName'];      
               return $username;        
          }
     }
     else
     {
          return $conn->error;
     }
}
function getPlannerName($uid,$conn)
{
     $sqlA = " SELECT userName FROM user WHERE  userID_PK = ".$uid;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['userName'];      
               return $username;        
          }
     }
     else
     {
          return $conn->error;
     }
}
function getDTM_Remark($dtmRemarks,$row)
{
     if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
     $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
     { 
          $dtmRemarks = "LOCKUP,".strtoupper($row['dtmRemarks']);
     }
     else
     { 
          $dtmRemarks = "".$row['dtmRemarks'];
     }
     return $dtmRemarks;
}
function checkLockupDates($lockupDates)
{
     $lockup = "";

     if($lockupDates)
     {
          $lockup = date("G:i",strtotime($lockupDates));
     }
      
     return $lockup;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TEMPLATE FUNCTIONS

function putBorderStyleRow($sheet,$cellAlphabet,$type = null,$colour = null)
{
     if($type && $type == 1)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
     }
     else if($type && $type == 2)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          if($colour)
          {
               $sheet->getStyle($cellAlphabet)
               ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
               $sheet->getStyle($cellAlphabet)
               ->getFill()->getStartColor()->setARGB($colour);
          }
     }
     else
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     }
}
function putWidth($alphabetFrom,$alphabetTo,$widthsize,$sheet)
{
     for($x=$alphabetFrom; $x != $alphabetTo; $x++)
     {
          $sheet->getColumnDimension($x)->setWidth($widthsize);
     }
} 
function normalTemplateTop($sheet,$fromDate,$toDate,$invoiceNo,$companyName,$costCenterName)
{
     $sheet->setCellValue('A1', 'Dates');
     $sheet->setCellValue('B1', $fromDate);
     $sheet->setCellValue('C1', 'To' );
     $sheet->setCellValue('D1', $toDate);

     $sheet->setCellValue('A2', 'VENDOR');
     $sheet->setCellValue('B2', 'VXL ALLIANCE');
     $sheet->setCellValue('C2', $companyName);
     $sheet->setCellValue('D2', $costCenterName);
     
     $sheet->setCellValue('A3', 'INVOICE NO');
     $sheet->setCellValue('B3', $invoiceNo);
     $sheet->setCellValue('A4', 'INVOICE DATE');

     $sheet->setCellValue('A6', 'Item No');
     $sheet->setCellValue('B6', 'Pickup Date');
     $sheet->setCellValue('C6', 'Truck No');
     $sheet->setCellValue('D6', 'Truck Size');
     $sheet->setCellValue('E6', 'D/O No');
     $sheet->setCellValue('F6', 'Quantity');
     $sheet->setCellValue('G6', 'Truck Charges');
     $sheet->setCellValue('H6', 'FAF (9%)');
     $sheet->setCellValue('I6', 'Overtime/Detention ');
     $sheet->setCellValue('J6', 'GPS Charge');
     $sheet->setCellValue('K6', 'Pickup/D`Point');
     $sheet->setCellValue('L6', 'Sunday/PH Pickup');
     $sheet->setCellValue('M6', 'Others');
     $sheet->setCellValue('N6', 'Total');
     $sheet->setCellValue('O6', 'Transport Remark');
     $sheet->setCellValue('P6', 'DTM Remarks');
     $sheet->setCellValue('Q6', 'DTM No');
     $sheet->setCellValue('R6', 'Consol Status');

     putBorderStyleRow($sheet,'A6:R6',2,'99AFFF');
     putWidth('A','S',20,$sheet);
     $sheet->getColumnDimension('F')->setWidth(30);
     $sheet->getStyle('A6:R6')->getAlignment()->setHorizontal('center');
     $sheet->getStyle('A1:D4')->getAlignment()->setHorizontal('center');
}
function normalTemplateBottom($sheet,$line)
{
     $sheet->setCellValue('A'.$line, "GRAND TOTAL (RM)");
     $sheet->mergeCells('A'.$line.':F'.$line);
     $sheet->setCellValue('G'.$line,'=SUM(G7:G'.($line-1).')');
     $sheet->setCellValue('H'.$line,'=SUM(H7:H'.($line-1).')');
     $sheet->setCellValue('I'.$line,'=SUM(I7:I'.($line-1).')');
     $sheet->setCellValue('J'.$line,'=SUM(J7:J'.($line-1).')');
     $sheet->setCellValue('K'.$line,'=SUM(K7:K'.($line-1).')');
     $sheet->setCellValue('L'.$line,'=SUM(L7:L'.($line-1).')');
     $sheet->setCellValue('M'.$line,'=SUM(M7:M'.($line-1).')');
     $sheet->setCellValue('N'.$line,'=SUM(N7:N'.($line-1).')');
     putDecimalPoints('G','O',$line,$sheet);
     $sheet->getStyle('G'.$line.':N'.$line)->getAlignment()->setHorizontal('center');
}
function putDecimalPoints($alphabetBefore, $alphabetAfter,$line,$sheet) 
{ 
     for($x=$alphabetBefore; $x != $alphabetAfter; $x++) 
     { 
          $sheet->getStyle($x.$line)->getNumberFormat()->setFormatCode('0.00');  
     } 
} 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
?>