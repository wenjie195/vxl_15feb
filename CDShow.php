<?php

require 'generalFunction.php';

if(isset($_POST['fromPage']))
{    
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 adminAlignThings">
            <form action="CDAdd.php" method="POST">
                <button class="btn btn-success btnWidth" value="<?php echo $_POST['fromPage'];?>" name="add">
                    <?php
                    if ($_POST['fromPage'] == 1)
                    {
                        echo "Add Places";
                    }
                    if ($_POST['fromPage'] == 2)
                    {
                        echo "Add Zones";
                    }
                    if ($_POST['fromPage'] == 3)
                    {
                        echo "Add Center";
                    }
                    if ($_POST['fromPage'] == 4)
                    {
                        echo "Add Agent";
                    }
                    if ($_POST['fromPage'] == 5)
                    {
                        echo "Add User";
                    }
                    ?>
                </button>
            </form>
            <p class="paginationClass">
                Page : 
                <select onchange="checkCondition(this.value,null,null,0,<?php echo $_POST['fromPage'];?>);" id="pagination<?php echo $_POST['fromPage']; ?>"></select> 
                of 
            </p>
            <p class="paginationClass paginationClassTotal " id="totalpages<?php echo $_POST['fromPage']; ?>"></p>
            <?php
            if ($_POST['fromPage'] != 5)
            {
                echo "<p class='filterPara'>Filter By: </p> ";
            }
            if ($_POST['fromPage'] == 5)
            {
                echo "<p class='filterPara filterParaUser'>Filter By: </p> ";
            }
            ?>
            <div class="adminAlignRight">
                <select class="filterClass" id="filter<?php echo $_POST['fromPage']; ?>" onchange="checkCondition(null,this.value,null,0,<?php echo $_POST['fromPage'];?>);">
                    <option selected disabled>Filter By</option>
                    <?php 
                        if ($_POST['fromPage'] == 1)
                        {
                            ?>
                            <option value="1">Date Created</option>
                            <option value="2">Places Name</option>
                            <?php
                        }
                        if ($_POST['fromPage'] == 2)
                        {
                            ?>
                            <option value="1">Date Created</option>
                            <option value="2">Zones Name</option>
                            <option value="3">Zones State</option>
                            <?php
                        }
                        if ($_POST['fromPage'] == 3)
                        {
                            ?>
                            <option value="1">Date Created</option>
                            <option value="2">Center Name</option>
                            <?php
                        }
                        if ($_POST['fromPage'] == 4)
                        {
                            ?>
                            <option value="1">Date Created</option>
                            <option value="2">Agent Name</option>
                            <?php
                        }
                        if ($_POST['fromPage'] == 5)
                        {
                            ?>
                            <option value="1">Date Created</option>
                            <option value="2">Name</option>
                            <option value="3">User ID</option>
                            <option value="4">IC No</option>
                            <option value="5">Position</option>
                            <?php
                        }
                    ?>
                </select>
                <input type="text" id="search<?php echo $_POST['fromPage']; ?>" class="searchClass" placeholder="Search Keyword">
                <button class="searchClass searchClassButton btn btn-primary " onclick="checkCondition(null,null,this.value,0,<?php echo $_POST['fromPage'];?>);">Search</button>
            </div>
        </div>
    </div>
    </div>
<div style="overflow-x:auto;" id="getTable<?php echo $_POST['fromPage'];?>"></div>
<?php
}
?>