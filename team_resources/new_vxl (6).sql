-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2019 at 08:43 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_vxl`
--

-- --------------------------------------------------------

--
-- Table structure for table `adddriverservicefee`
--

CREATE TABLE `adddriverservicefee` (
  `id` int(255) NOT NULL,
  `driverID_FK` int(255) NOT NULL,
  `serviceFee` double(10,2) NOT NULL,
  `dtmID_FK` int(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dsfComplete` int(10) NOT NULL DEFAULT '0',
  `dsfGross` double(10,2) NOT NULL,
  `night` varchar(10) NOT NULL,
  `detention` int(10) NOT NULL DEFAULT '0',
  `sunday` varchar(10) NOT NULL,
  `public` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adddriverservicefee`
--

INSERT INTO `adddriverservicefee` (`id`, `driverID_FK`, `serviceFee`, `dtmID_FK`, `dateCreated`, `dsfComplete`, `dsfGross`, `night`, `detention`, `sunday`, `public`) VALUES
(3, 22, 1141.00, 36, '2019-06-11 22:12:04', 0, 0.00, '0', 0, '0', '0'),
(4, 22, 1141.00, 36, '2019-06-11 22:12:04', 1, 0.00, '0', 0, '0', '0'),
(5, 22, 1156.00, 36, '2019-06-11 22:12:33', 1, 0.00, '0', 0, '0', '0'),
(6, 22, 1156.00, 36, '2019-06-11 22:12:33', 0, 0.00, '0', 0, '0', '0'),
(7, 22, 1156.00, 36, '2019-06-11 22:13:25', 0, 0.00, '0', 0, '0', '0'),
(8, 22, 1156.00, 36, '2019-06-11 22:13:25', 0, 0.00, '0', 0, '0', '0'),
(9, 22, 1156.00, 36, '2019-06-11 22:13:55', 0, 0.00, '0', 0, '0', '0'),
(10, 22, 1156.00, 36, '2019-06-11 22:13:55', 0, 0.00, '0', 0, '0', '0'),
(11, 22, 1156.00, 36, '2019-06-11 22:14:50', 0, 0.00, '0', 0, '0', '0'),
(12, 22, 1156.00, 36, '2019-06-11 22:14:50', 0, 0.00, '0', 0, '0', '0'),
(13, 22, 478.00, 31, '2019-06-11 23:24:55', 0, 0.00, '0', 0, '0', '0'),
(14, 22, 473.00, 32, '2019-06-11 23:27:01', 1, 0.00, '0', 0, '0', '0'),
(15, 22, 1121.00, 37, '2019-06-11 23:43:57', 1, 900.00, 'yes', 211, 'no', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `companyID_PK` int(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `companyShortForm` varchar(10) NOT NULL,
  `companyDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `companyDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`companyID_PK`, `companyName`, `companyShortForm`, `companyDateUpdated`, `companyDateCreated`, `showThis`) VALUES
(9, 'SCHENKER AGH', 'SCH AGH', '2019-05-29 01:51:03', '0000-00-00 00:00:00', 1),
(10, 'SCHENKER AGB', 'SCH AGB', '2019-06-11 17:41:20', '0000-00-00 00:00:00', 1),
(11, 'INTEL 3', 'INTEL', '2019-06-12 05:50:18', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `costcenter`
--

CREATE TABLE `costcenter` (
  `costCenterID_PK` int(255) NOT NULL,
  `costCenterName` varchar(255) NOT NULL,
  `costCenterDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `costCenterDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costcenter`
--

INSERT INTO `costcenter` (`costCenterID_PK`, `costCenterName`, `costCenterDateUpdated`, `costCenterDateCreated`, `showThis`) VALUES
(15, 'NONE', '2019-05-29 01:50:40', '2019-05-29 01:50:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driverID_PK` int(255) NOT NULL,
  `driverDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `driverDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `driverName` varchar(255) NOT NULL,
  `driverNickName` varchar(255) NOT NULL,
  `driverICno` varchar(255) NOT NULL,
  `driverPhoneNo` varchar(10) NOT NULL,
  `insertedByID_FK` int(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driverID_PK`, `driverDateUpdated`, `driverDateCreated`, `driverName`, `driverNickName`, `driverICno`, `driverPhoneNo`, `insertedByID_FK`, `showThis`) VALUES
(22, '2019-05-29 01:52:33', '2019-05-29 01:52:33', 'Azrul Naim', 'Azrul', '090106131762', '0195678943', 1, 1),
(23, '2019-06-12 05:48:28', '2019-06-12 05:48:11', 'Azman Arif', 'Azman', '900807021987', '0176758234', 1, 0),
(24, '2019-06-14 03:18:59', '2019-06-14 03:18:59', 'Shai', 'Shaiman Ariff', '900101102323', '0198145612', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtmlist`
--

CREATE TABLE `dtmlist` (
  `dtmID_PK` int(255) NOT NULL,
  `dtmDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dtmDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dtmDGF` varchar(255) DEFAULT NULL,
  `companyID_FK` int(255) NOT NULL,
  `dtmBookingDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dtmBookingTime` time NOT NULL,
  `dtmPickupDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `dtmPickupTime` time NOT NULL,
  `dtmAdhoc` varchar(50) NOT NULL,
  `dtmOriginPointID_FK` int(255) NOT NULL,
  `dtmOriginZoneID_FK` int(255) NOT NULL,
  `dtmDestinationPointID_FK` int(255) NOT NULL,
  `dtmDestinationZoneID_FK` int(255) NOT NULL,
  `truckID_FK` int(255) NOT NULL,
  `driverID_FK` int(255) NOT NULL,
  `driver2ID_FK` int(255) DEFAULT NULL,
  `dtmUrgentMemo` int(10) NOT NULL,
  `dtmOriginF` time DEFAULT NULL,
  `dtmOriginDockTime` time DEFAULT NULL,
  `dtmOriginCompleteLoadingTime` time DEFAULT NULL,
  `dtmOriginDepartureTimeFromPost` time DEFAULT NULL,
  `dtmDestinationArrivalTimeAtPost` time DEFAULT NULL,
  `dtmDestinationDockTime` time DEFAULT NULL,
  `dtmDestinationCompleteUnloadTime` time DEFAULT NULL,
  `dtmDestinationDepartTimeFromPost` time DEFAULT NULL,
  `loadCap` varchar(50) NOT NULL,
  `dtmQuantityCarton` int(255) DEFAULT NULL,
  `dtmIsQuantity` int(10) NOT NULL,
  `dtmQuantityPalllets` int(255) DEFAULT NULL,
  `dtmIsPallets` int(10) NOT NULL,
  `dtmQuantityCages` int(255) DEFAULT NULL,
  `dtmIsCages` int(10) NOT NULL,
  `dtmTripOnTime` int(10) NOT NULL,
  `dtmDelayReasons` varchar(255) DEFAULT NULL,
  `costCenterID_FK` int(255) DEFAULT NULL,
  `dtmPlannerID_FK` int(255) NOT NULL,
  `dtmRequestBy` varchar(255) NOT NULL,
  `dtmRemarks` varchar(255) DEFAULT NULL,
  `dtmIsPaidDriverServiceFee` int(20) NOT NULL,
  `dtmIsTransportCharge` int(10) DEFAULT NULL,
  `dtmIsFinished` int(10) NOT NULL DEFAULT '0',
  `dsfServiceFee` int(10) NOT NULL DEFAULT '0',
  `lockupf` timestamp NULL DEFAULT NULL,
  `lockupdockTime` timestamp NULL DEFAULT NULL,
  `lockupcompleteLoadingTime` timestamp NULL DEFAULT NULL,
  `lockupdepartureTimeFromPost` timestamp NULL DEFAULT NULL,
  `lockuparrivalTimeAtPost` timestamp NULL DEFAULT NULL,
  `lockupdestinationDockTime` timestamp NULL DEFAULT NULL,
  `lockupcompleteUnloadTime` timestamp NULL DEFAULT NULL,
  `lockupdepartTimeFromPost` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtmlist`
--

INSERT INTO `dtmlist` (`dtmID_PK`, `dtmDateUpdated`, `dtmDateCreated`, `dtmDGF`, `companyID_FK`, `dtmBookingDate`, `dtmBookingTime`, `dtmPickupDate`, `dtmPickupTime`, `dtmAdhoc`, `dtmOriginPointID_FK`, `dtmOriginZoneID_FK`, `dtmDestinationPointID_FK`, `dtmDestinationZoneID_FK`, `truckID_FK`, `driverID_FK`, `driver2ID_FK`, `dtmUrgentMemo`, `dtmOriginF`, `dtmOriginDockTime`, `dtmOriginCompleteLoadingTime`, `dtmOriginDepartureTimeFromPost`, `dtmDestinationArrivalTimeAtPost`, `dtmDestinationDockTime`, `dtmDestinationCompleteUnloadTime`, `dtmDestinationDepartTimeFromPost`, `loadCap`, `dtmQuantityCarton`, `dtmIsQuantity`, `dtmQuantityPalllets`, `dtmIsPallets`, `dtmQuantityCages`, `dtmIsCages`, `dtmTripOnTime`, `dtmDelayReasons`, `costCenterID_FK`, `dtmPlannerID_FK`, `dtmRequestBy`, `dtmRemarks`, `dtmIsPaidDriverServiceFee`, `dtmIsTransportCharge`, `dtmIsFinished`, `dsfServiceFee`, `lockupf`, `lockupdockTime`, `lockupcompleteLoadingTime`, `lockupdepartureTimeFromPost`, `lockuparrivalTimeAtPost`, `lockupdestinationDockTime`, `lockupcompleteUnloadTime`, `lockupdepartTimeFromPost`) VALUES
(31, '2019-06-11 23:24:55', '2019-05-29 01:54:34', 'BSDHFSD73HJSA83SDS', 9, '2019-05-28 16:00:00', '12:01:00', '2019-05-29 16:00:00', '12:02:00', 'aim', 21, 23, 21, 23, 10, 22, NULL, 1, '12:00:00', '12:01:00', '12:02:00', '12:03:00', '13:00:00', '13:01:00', '13:02:00', '13:04:00', '1 Tonner', 0, 0, 21, 1, 0, 0, 1, '', 15, 23, 'ASDK', 'SUPER HOT', 0, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '2019-06-11 23:27:01', '2019-05-29 02:16:16', 'HJHDFJ65SA5', 9, '2019-05-29 16:00:00', '12:01:00', '2019-05-29 16:00:00', '12:02:00', 'ais', 21, 23, 21, 23, 11, 22, NULL, 1, '12:00:00', '12:01:00', '12:02:00', '12:02:00', '13:00:00', '13:01:00', '13:02:00', '13:03:00', '1 Tonner', 0, 0, 0, 0, 12, 1, 1, '', 15, 23, 'ASDS', 'ZCX', 0, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '2019-06-14 04:29:17', '2019-06-11 07:32:38', 'loadcap', 9, '2019-06-19 16:00:00', '12:00:00', '2019-06-29 16:00:00', '12:00:00', 'aim', 21, 23, 21, 23, 10, 22, 22, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '1 Tonner', 22, 1, 0, 0, 0, 0, 1, '', 15, 23, 'ccc', 'zzz', 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '2019-06-11 23:43:57', '2019-06-11 23:42:10', 'asdasddsasadsadasd', 9, '2019-06-12 16:00:00', '12:00:00', '2019-06-12 16:00:00', '12:00:00', 'aim', 21, 23, 21, 24, 11, 22, NULL, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '3 Ton', 222, 1, 0, 0, 0, 0, 1, '', 15, 23, 'ads', '123', 0, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '2019-06-12 03:17:27', '2019-06-12 03:17:18', 'aaaaaaa', 9, '2019-06-11 16:00:00', '12:00:00', '2019-06-20 16:00:00', '12:00:00', 'ais', 21, 24, 21, 24, 10, 22, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20 Ft / 10 Ton', 0, 0, 0, 0, 22, 1, 1, '', 15, 23, 'ddd', '2121', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '2019-06-13 17:15:27', '2019-06-12 05:36:18', 'vvvvv', 9, '2019-06-12 16:00:00', '12:00:00', '2019-06-05 16:00:00', '12:00:00', 'aim', 21, 24, 21, 23, 10, 22, NULL, 1, '12:00:00', '12:01:00', '12:02:00', '12:03:00', '12:04:00', '12:05:00', '12:06:00', '13:00:00', '20 Ft / 10 Ton', 22, 1, 0, 0, 0, 0, 1, '', 15, 23, 'ddd', 'zzxx', 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '2019-06-13 08:35:07', '2019-06-13 04:40:37', 'cc', 9, '2019-06-13 16:00:00', '12:00:00', '2019-06-14 16:00:00', '12:00:00', 'aim', 21, 24, 21, 24, 10, 22, NULL, 0, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '20 Ft / 10 Ton', 111, 1, 0, 0, 0, 0, 1, '', 15, 23, 'ccc', 'ddd', 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '2019-06-14 03:10:59', '2019-06-13 08:22:04', '', 11, '2019-06-20 16:00:00', '22:00:00', '2019-06-18 16:00:00', '12:00:00', 'aim', 21, 23, 21, 24, 10, 22, 22, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '23:50:00', '12:00:00', '3 Ton', 12, 1, 12, 1, 0, 0, 1, '', 15, 23, 'dddd', 'cccc', 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-19 16:00:00', '2019-06-20 16:00:00'),
(42, '2019-06-14 02:00:56', '2019-06-13 08:24:22', '111111111111', 11, '2019-06-29 16:00:00', '12:00:00', '2019-06-17 16:00:00', '12:00:00', 'consol', 21, 23, 21, 24, 10, 22, NULL, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '3 Ton', 12, 1, 12, 1, 0, 0, 1, '', 15, 23, 'dddd', 'cccc', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-18 16:00:00'),
(43, '2019-06-13 15:33:12', '2019-06-13 13:25:50', '', 10, '2019-06-13 16:00:00', '12:00:00', '2019-06-13 16:00:00', '12:00:00', 'aim', 21, 23, 21, 25, 10, 22, NULL, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '3 Ton', 12, 1, 0, 0, 0, 0, 1, '', 15, 23, '123213', 'aasdasda', 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '2019-06-13 15:33:14', '2019-06-13 13:57:05', '', 10, '2019-06-13 16:00:00', '12:00:00', '2019-06-13 16:00:00', '12:00:00', 'ais', 21, 24, 21, 24, 10, 22, NULL, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '40 Ft / Q6', 666, 1, 0, 0, 0, 0, 1, '', 15, 23, 'ffg', 'hhh', 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '2019-06-13 17:31:52', '2019-06-13 17:26:52', 'consol', 10, '2019-06-13 16:00:00', '12:00:00', '2019-06-14 16:00:00', '12:00:00', 'consol', 21, 23, 21, 24, 10, 22, NULL, 1, '11:00:00', '11:01:00', '12:02:00', '12:03:00', '12:04:00', '12:05:00', '12:07:00', '12:08:00', '40 Ft / Q6 Air-Ride', 0, 0, 22, 1, 0, 0, 1, '', 15, 23, 'asd', 'aasd', 0, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-15 16:00:00'),
(46, '2019-06-14 04:18:15', '2019-06-14 03:20:42', '2nd Driver', 11, '2019-06-13 16:00:00', '12:00:00', '2019-06-13 16:00:00', '12:00:00', 'aim', 21, 24, 21, 25, 10, 22, 24, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:01:00', '12:00:00', '12:00:00', '40 Ft / Q6 Air-Ride', 11, 1, 0, 0, 0, 0, 1, '', 15, 23, '1111', 'second driver', 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, '2019-06-14 16:00:00', '2019-06-15 16:00:00', '2019-06-18 16:00:00'),
(47, '2019-06-14 04:37:16', '2019-06-14 04:36:54', 'NEW TEST', 11, '2019-06-14 16:00:00', '12:00:00', '2019-06-14 16:00:00', '16:00:00', 'consol', 21, 25, 21, 23, 10, 24, 22, 1, '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '12:00:00', '3 Ton', 0, 0, 1122, 1, 0, 0, 0, 'LOCKUP', 15, 23, 'andy', 'LOCKUP', 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(255) NOT NULL,
  `invoiceNo` int(255) NOT NULL,
  `transportID_FK` int(255) NOT NULL,
  `isSelected` int(10) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoiceNo`, `transportID_FK`, `isSelected`, `dateCreated`) VALUES
(84, 1, 32, 0, '2019-05-29 01:56:24'),
(85, 2, 33, 0, '2019-05-29 02:17:16'),
(86, 1, 38, 1, '2019-06-12 01:28:25'),
(87, 3, 40, 0, '2019-06-14 02:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `pointzone`
--

CREATE TABLE `pointzone` (
  `pointzoneID_PK` int(255) NOT NULL,
  `pointzonePlaceName` varchar(255) NOT NULL,
  `pointzoneDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pointzoneDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pointzone`
--

INSERT INTO `pointzone` (`pointzoneID_PK`, `pointzonePlaceName`, `pointzoneDateUpdated`, `pointzoneDateCreated`, `showThis`) VALUES
(21, 'SCHENKER CSH', '2019-05-29 01:50:10', '2019-05-29 01:50:10', 1),
(22, 'SCHENKER TKS', '2019-06-12 05:49:40', '2019-06-12 05:49:25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `servicefeeratesplace`
--

CREATE TABLE `servicefeeratesplace` (
  `id` int(255) NOT NULL,
  `origin` int(255) NOT NULL,
  `destination` int(255) NOT NULL,
  `loadTransport` varchar(255) NOT NULL,
  `rates` double(10,2) NOT NULL,
  `noOfDrivers` int(20) NOT NULL,
  `showThis` int(10) NOT NULL DEFAULT '1',
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicefeeratesplace`
--

INSERT INTO `servicefeeratesplace` (`id`, `origin`, `destination`, `loadTransport`, `rates`, `noOfDrivers`, `showThis`, `dateCreated`) VALUES
(2, 23, 24, '20 Ft / 10 Ton', 2222.00, 1, 0, '2019-06-11 19:48:00'),
(3, 23, 23, '20 Ft / 10 Ton', 44.00, 1, 1, '2019-06-11 19:48:00'),
(4, 23, 23, '20 Ft / 10 Ton', 111.00, 1, 1, '2019-06-11 19:48:00'),
(5, 23, 24, '20 Ft / 10 Ton', 21.00, 1, 0, '2019-06-11 19:48:00'),
(6, 23, 24, '20 Ft / 10 Ton', 3333.00, 1, 1, '2019-06-11 19:48:25'),
(7, 23, 24, '40 Ft / Q6', 55.00, 1, 0, '2019-06-11 20:48:44'),
(8, 23, 24, '1 Tonner', 22.00, 1, 0, '2019-06-11 20:49:29'),
(9, 23, 24, '40 Ft / Q6', 2112.00, 1, 1, '2019-06-11 20:50:12'),
(10, 24, 24, '40 Ft / Q6', 222.00, 2, 1, '2019-06-11 20:51:00'),
(11, 23, 23, '1 Tonner', 1111.00, 2, 1, '2019-06-11 20:59:49'),
(15, 23, 23, '1 Tonner', 433.00, 1, 1, '2019-06-11 23:24:06'),
(16, 23, 24, '3 Ton', 900.00, 1, 1, '2019-06-11 23:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `sessionID_PK` int(255) NOT NULL,
  `sessionDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sessionLogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessionLogout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessionBehaviour` varchar(50) NOT NULL DEFAULT 'NOTHING',
  `userID_FK` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`sessionID_PK`, `sessionDateUpdated`, `sessionLogin`, `sessionLogout`, `sessionBehaviour`, `userID_FK`) VALUES
(181, '2019-05-29 01:48:58', '2019-05-29 01:46:28', '2019-05-29 01:48:58', '', 1),
(182, '2019-05-29 01:49:07', '2019-05-29 01:49:07', '0000-00-00 00:00:00', '', 23),
(183, '2019-05-29 01:49:46', '2019-05-29 01:49:46', '0000-00-00 00:00:00', '', 1),
(184, '2019-05-29 02:15:18', '2019-05-29 02:15:18', '0000-00-00 00:00:00', '', 23),
(185, '2019-06-11 06:08:41', '2019-06-11 06:08:33', '2019-06-11 06:08:41', '', 1),
(186, '2019-06-11 06:09:05', '2019-06-11 06:09:05', '0000-00-00 00:00:00', '', 23),
(187, '2019-06-11 14:09:18', '2019-06-11 14:08:54', '2019-06-11 14:09:18', '', 23),
(188, '2019-06-11 14:09:33', '2019-06-11 14:09:33', '0000-00-00 00:00:00', '', 1),
(189, '2019-06-11 14:12:06', '2019-06-11 14:12:06', '0000-00-00 00:00:00', '', 23),
(190, '2019-06-11 17:40:16', '2019-06-11 17:40:16', '0000-00-00 00:00:00', '', 23),
(191, '2019-06-11 23:41:30', '2019-06-11 23:41:30', '0000-00-00 00:00:00', '', 23),
(192, '2019-06-12 03:16:27', '2019-06-12 01:21:21', '2019-06-12 03:16:27', '', 1),
(193, '2019-06-12 03:21:17', '2019-06-12 03:16:37', '2019-06-12 03:21:17', '', 23),
(194, '2019-06-12 03:21:33', '2019-06-12 03:21:33', '0000-00-00 00:00:00', '', 1),
(195, '2019-06-12 05:35:22', '2019-06-12 04:10:31', '2019-06-12 05:35:22', '', 1),
(196, '2019-06-12 05:42:46', '2019-06-12 05:35:34', '2019-06-12 05:42:46', '', 23),
(197, '2019-06-12 05:42:55', '2019-06-12 05:42:55', '0000-00-00 00:00:00', '', 1),
(198, '2019-06-12 07:26:38', '2019-06-12 07:26:38', '0000-00-00 00:00:00', '', 1),
(199, '2019-06-13 02:05:41', '2019-06-13 02:05:41', '0000-00-00 00:00:00', 'NOTHING', 1),
(200, '2019-06-13 02:26:11', '2019-06-13 02:26:11', '0000-00-00 00:00:00', 'NOTHING', 1),
(201, '2019-06-13 04:11:01', '2019-06-13 03:37:00', '2019-06-13 04:11:01', 'NOTHING', 1),
(202, '2019-06-13 04:12:41', '2019-06-13 04:12:32', '2019-06-13 04:12:41', 'NOTHING', 1),
(203, '2019-06-13 04:13:01', '2019-06-13 04:13:01', '0000-00-00 00:00:00', 'NOTHING', 23),
(204, '2019-06-13 09:07:15', '2019-06-13 09:07:09', '2019-06-13 09:07:15', 'NOTHING', 1),
(205, '2019-06-13 09:07:24', '2019-06-13 09:07:24', '0000-00-00 00:00:00', 'NOTHING', 23),
(206, '2019-06-13 13:25:00', '2019-06-13 13:03:06', '2019-06-13 13:25:00', 'NOTHING', 1),
(207, '2019-06-13 16:57:59', '2019-06-13 13:25:08', '2019-06-13 16:57:59', 'NOTHING', 23),
(208, '2019-06-13 17:24:43', '2019-06-13 16:58:11', '2019-06-13 17:24:43', 'NOTHING', 1),
(209, '2019-06-13 17:24:54', '2019-06-13 17:24:54', '0000-00-00 00:00:00', 'NOTHING', 23),
(210, '2019-06-13 17:26:06', '2019-06-13 17:26:06', '0000-00-00 00:00:00', 'NOTHING', 1),
(211, '2019-06-13 23:18:01', '2019-06-13 23:18:01', '0000-00-00 00:00:00', 'NOTHING', 1),
(212, '2019-06-13 23:32:04', '2019-06-13 23:32:04', '0000-00-00 00:00:00', 'NOTHING', 1),
(213, '2019-06-14 02:00:46', '2019-06-14 02:00:46', '0000-00-00 00:00:00', 'NOTHING', 23),
(214, '2019-06-14 03:02:44', '2019-06-14 03:02:44', '0000-00-00 00:00:00', 'NOTHING', 23);

-- --------------------------------------------------------

--
-- Table structure for table `transportcharge`
--

CREATE TABLE `transportcharge` (
  `id` int(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `operatingHour` int(10) NOT NULL,
  `transportcharge` double(20,10) NOT NULL,
  `faf` double(20,10) NOT NULL,
  `truckID_FK` int(255) NOT NULL,
  `companyID_FK` int(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dtmID_FK` int(255) NOT NULL,
  `isInvoiceNoAdded` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transportcharge`
--

INSERT INTO `transportcharge` (`id`, `remarks`, `operatingHour`, `transportcharge`, `faf`, `truckID_FK`, `companyID_FK`, `dateCreated`, `dtmID_FK`, `isInvoiceNoAdded`) VALUES
(32, 'MUST get FIRST', 4, 200.0000000000, 12.0000000000, 10, 9, '2019-05-29 01:56:24', 31, 1),
(33, 'SEDAP', 4, 300.0000000000, 2.0000000000, 11, 9, '2019-05-29 02:17:16', 32, 1),
(38, '213', 12, 555.0000000000, 3.0000000000, 10, 9, '2019-06-12 01:28:25', 36, 1),
(39, 'sadsa', 2, 112221.0000000000, -21.0000000000, 10, 9, '2019-06-13 17:15:27', 39, 0),
(40, '123', 123, 222.0000000000, -22.0000000000, 10, 10, '2019-06-14 02:45:51', 45, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transportrate`
--

CREATE TABLE `transportrate` (
  `id` int(255) NOT NULL,
  `companyID_FK` int(255) NOT NULL,
  `loadTransport` varchar(255) NOT NULL,
  `rates` double(10,2) NOT NULL,
  `consoleRate` double(10,2) NOT NULL DEFAULT '0.00',
  `showThis` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transportrate`
--

INSERT INTO `transportrate` (`id`, `companyID_FK`, `loadTransport`, `rates`, `consoleRate`, `showThis`) VALUES
(10, 9, '20 Ft / 10 Ton', 112221.00, 0.00, 1),
(11, 9, '40 Ft / Q6 Air-Ride', 3333.00, 0.00, 0),
(12, 9, '1 Tonner', 555.00, 0.00, 1),
(13, 9, '3 Ton', 666.00, 0.00, 1),
(14, 9, '40 Ft / Q6', 777.00, 0.00, 1),
(15, 9, '45 Ft / Q7', 10200.00, 0.00, 1),
(16, 11, '3 Ton', 222.00, 0.00, 1),
(17, 11, '1 Tonner', 1111.00, 221.00, 1),
(18, 10, '40 Ft / Q6 Air-Ride', 111.00, 222.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `trucks`
--

CREATE TABLE `trucks` (
  `truckID_PK` int(255) NOT NULL,
  `truckPlateNo` varchar(255) NOT NULL,
  `truckCapacity` varchar(255) NOT NULL,
  `truckModel` varchar(255) NOT NULL,
  `truckMade` varchar(255) NOT NULL,
  `truckCustomBond` varchar(255) NOT NULL,
  `truckDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `truckDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `truckCustomExpired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trucks`
--

INSERT INTO `trucks` (`truckID_PK`, `truckPlateNo`, `truckCapacity`, `truckModel`, `truckMade`, `truckCustomBond`, `truckDateUpdated`, `truckDateCreated`, `truckCustomExpired`, `showThis`) VALUES
(10, 'PMG 1234', '3 Ton', 'Galllardo', 'Lamborghini', '127389NSD12', '2019-05-29 01:53:20', '2019-05-29 01:53:20', '2019-05-28 16:00:00', 1),
(11, 'MFC 1020', '40 Ft / Q6', 'Saga', 'Proton', 'JNADS67NASDJ', '2019-05-29 02:14:37', '2019-05-29 02:14:37', '2019-05-29 16:00:00', 1),
(12, 'KLA 2343', '20 Ft / 10 Ton', 'Proton', 'AAA', 'asdasdas', '2019-06-12 05:49:12', '2019-06-12 05:48:58', '2019-06-17 16:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID_PK` int(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userNickName` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userLevel` int(10) NOT NULL,
  `userTele` varchar(10) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userInsertedBy` int(255) NOT NULL,
  `userAddress` varchar(255) NOT NULL,
  `userState` varchar(20) NOT NULL,
  `userIC` varchar(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID_PK`, `userName`, `userNickName`, `userPassword`, `userLevel`, `userTele`, `userEmail`, `userDateUpdated`, `userDateCreated`, `userInsertedBy`, `userAddress`, `userState`, `userIC`, `showThis`) VALUES
(1, 'Muhammad Hafeezul Anwar Bin Roslan', 'Hafeezul', '1', 1, '0106664914', 'hafeezdzeko374@gmail.com', '2019-06-13 04:12:11', '2019-02-08 02:22:40', 1, 'Taman Desa Tasik,Sungai Besi', 'WP Kuala Lumpur', '940311105409', 1),
(23, 'Amir', 'Muammad', '123', 2, '0123741070', 'angsa@gmail.com', '2019-05-29 01:48:47', '2019-05-29 01:48:47', 1, 'Kubang Semang,Tasek Gelugor', 'Pulau Pinang', '900310120987', 1),
(24, 'Asri Kumoi', 'Asri ', '123', 2, '0107675623', 'haf@gmail.com', '2019-06-12 05:47:32', '2019-06-12 05:46:50', 1, 'sadasdsasdads', 'Johor', '940101103409', 0);

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `zonesID_PK` int(255) NOT NULL,
  `zonesName` varchar(255) NOT NULL,
  `zonesState` varchar(255) NOT NULL,
  `zonesDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zonesDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`zonesID_PK`, `zonesName`, `zonesState`, `zonesDateUpdated`, `zonesDateCreated`, `showThis`) VALUES
(23, 'Penang', 'PENANG', '2019-05-29 01:50:30', '2019-05-29 01:50:30', 1),
(24, 'Prai', 'PRAI', '2019-06-11 17:45:15', '2019-06-11 17:45:15', 1),
(25, 'klia', 'KLIA', '2019-06-12 05:49:55', '2019-06-12 05:49:55', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driverID_FK` (`driverID_FK`),
  ADD KEY `dtmID_FK` (`dtmID_FK`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`companyID_PK`);

--
-- Indexes for table `costcenter`
--
ALTER TABLE `costcenter`
  ADD PRIMARY KEY (`costCenterID_PK`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driverID_PK`);

--
-- Indexes for table `dtmlist`
--
ALTER TABLE `dtmlist`
  ADD PRIMARY KEY (`dtmID_PK`),
  ADD KEY `companyID_FK` (`companyID_FK`),
  ADD KEY `costCenterID_FK` (`costCenterID_FK`),
  ADD KEY `driverID_FK` (`driverID_FK`),
  ADD KEY `dtmDestinationPointID_FK` (`dtmDestinationPointID_FK`),
  ADD KEY `dtmDestinationZoneID_FK` (`dtmDestinationZoneID_FK`),
  ADD KEY `dtmOriginPointID_FK` (`dtmOriginPointID_FK`),
  ADD KEY `dtmOriginZoneID_FK` (`dtmOriginZoneID_FK`),
  ADD KEY `dtmPlannerID_FK` (`dtmPlannerID_FK`),
  ADD KEY `truckID_FK` (`truckID_FK`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transportID_FK` (`transportID_FK`);

--
-- Indexes for table `pointzone`
--
ALTER TABLE `pointzone`
  ADD PRIMARY KEY (`pointzoneID_PK`);

--
-- Indexes for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `origin` (`origin`),
  ADD KEY `destination` (`destination`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`sessionID_PK`),
  ADD KEY `userID_FK` (`userID_FK`);

--
-- Indexes for table `transportcharge`
--
ALTER TABLE `transportcharge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyID_FK` (`companyID_FK`),
  ADD KEY `truckID_FK` (`truckID_FK`),
  ADD KEY `dtmID_FK` (`dtmID_FK`);

--
-- Indexes for table `transportrate`
--
ALTER TABLE `transportrate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyID_FK` (`companyID_FK`);

--
-- Indexes for table `trucks`
--
ALTER TABLE `trucks`
  ADD PRIMARY KEY (`truckID_PK`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID_PK`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`zonesID_PK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `companyID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `costcenter`
--
ALTER TABLE `costcenter`
  MODIFY `costCenterID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driverID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `dtmlist`
--
ALTER TABLE `dtmlist`
  MODIFY `dtmID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `pointzone`
--
ALTER TABLE `pointzone`
  MODIFY `pointzoneID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `sessionID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `transportcharge`
--
ALTER TABLE `transportcharge`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `transportrate`
--
ALTER TABLE `transportrate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `trucks`
--
ALTER TABLE `trucks`
  MODIFY `truckID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `zonesID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  ADD CONSTRAINT `adddriverservicefee_ibfk_1` FOREIGN KEY (`driverID_FK`) REFERENCES `driver` (`driverID_PK`),
  ADD CONSTRAINT `adddriverservicefee_ibfk_2` FOREIGN KEY (`dtmID_FK`) REFERENCES `dtmlist` (`dtmID_PK`);

--
-- Constraints for table `dtmlist`
--
ALTER TABLE `dtmlist`
  ADD CONSTRAINT `dtmlist_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_2` FOREIGN KEY (`costCenterID_FK`) REFERENCES `costcenter` (`costCenterID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_3` FOREIGN KEY (`driverID_FK`) REFERENCES `driver` (`driverID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_4` FOREIGN KEY (`dtmDestinationPointID_FK`) REFERENCES `pointzone` (`pointzoneID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_5` FOREIGN KEY (`dtmDestinationZoneID_FK`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_6` FOREIGN KEY (`dtmOriginPointID_FK`) REFERENCES `pointzone` (`pointzoneID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_7` FOREIGN KEY (`dtmOriginZoneID_FK`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_8` FOREIGN KEY (`dtmPlannerID_FK`) REFERENCES `user` (`userID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_9` FOREIGN KEY (`truckID_FK`) REFERENCES `trucks` (`truckID_PK`);

--
-- Constraints for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  ADD CONSTRAINT `servicefeeratesplace_ibfk_1` FOREIGN KEY (`origin`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `servicefeeratesplace_ibfk_2` FOREIGN KEY (`destination`) REFERENCES `zones` (`zonesID_PK`);

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`userID_FK`) REFERENCES `user` (`userID_PK`);

--
-- Constraints for table `transportcharge`
--
ALTER TABLE `transportcharge`
  ADD CONSTRAINT `transportcharge_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`),
  ADD CONSTRAINT `transportcharge_ibfk_2` FOREIGN KEY (`truckID_FK`) REFERENCES `trucks` (`truckID_PK`),
  ADD CONSTRAINT `transportcharge_ibfk_3` FOREIGN KEY (`dtmID_FK`) REFERENCES `dtmlist` (`dtmID_PK`);

--
-- Constraints for table `transportrate`
--
ALTER TABLE `transportrate`
  ADD CONSTRAINT `transportrate_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
