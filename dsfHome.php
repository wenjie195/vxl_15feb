<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Driver Service Fee</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>Drivers Service Fee</h3>
                    </div>
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingZero">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed adminAccordionButton" type="button" data-toggle="collapse" data-target="#collapseZero" aria-expanded="false" aria-controls="collapseZero">
                                        Unpaid Service Fee
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseZero" class="collapse" aria-labelledby="headingZero" data-parent="#accordionExample">
                                <div class="card-body" id="showUnselectedDriverServiceFee"></div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed adminAccordionButton" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Paid Service Fee
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body" id="showDriverServiceFee"></div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
            $(document).ready(function()
            {
                ajaxDriverServiceFee(1);
                ajaxDriverServiceFee(2);
            });
        </script>
    </body>
</html>
<?php
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>