<?php
require 'generalFunction.php';
$conn = connDB();
$companyID = $_POST['companyID'];
$type = $_POST['type'];
if(isset($_POST['costID']))
{
     $costID = $_POST['costID'];
}
if(isset($_POST['formType']) && $_POST['formType'] == 2)
{
?>
<label class="cb-container cb-padding">
     <input type="checkbox" name="isCostCenter" id="isCostCenter" >
     <span class="checkmark"></span>
</label>
<?php 
}

if(isset($_POST['formType']) && $_POST['formType'] == 1)
{
?>
<label for="input-dtm-costCenter" >Cost Centre</label>
<select class="form-control adminAddSetPadding" id="input-dtm-costCenter" name="costcenter">
<?php 
}

if(isset($_POST['formType']) && $_POST['formType'] == 2)
{
?>
<label for="input-dtm-costCenter1" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCost Centre</label>
<select class="form-control adminAddSetPadding" id="input-dtm-costCenter1" name="costcenter1">
<?php 
}

?>
<option disabled selected hidden>-- Pick one of the following --</option>
<?php 

$sql_select_costCenter = "SELECT * FROM costcenter WHERE showThis = 1 AND companyID_FK = '".$companyID."'" ;
$result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

if (mysqli_num_rows($result_select_costCenter) > 0) 
{
     // output data of each row
     while($row = mysqli_fetch_assoc($result_select_costCenter)) 
     {
          if($type == 1)
          {
               echo '<option value="'.$row["costCenterID_PK"].'">'.$row["costCenterName"].'</option>';
          }
          else
          {
               if($row["costCenterID_PK"] == $costID)
               {
                    echo '<option value="'.$row["costCenterID_PK"].'" selected>'.$row["costCenterName"].'</option>';
               }
               else
               {
                    echo '<option value="'.$row["costCenterID_PK"].'">'.$row["costCenterName"].'</option>';
               }
          }
     }
}
?>
</select>