<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 32)
    {
        $orderBy = "dtmPickupDate";
    }
}
if($filter == 2)
{
    if($fromPage == 32)
    {
        $orderBy = "dtmPickupDate";
    }
}


$sql = "";
$sql2 = "";

if($fromPage == 32)
{
    $sql .= " SELECT * FROM dtmlist WHERE dtmIsFinished = 1 AND dsfServiceFee = 0 ";
    $sql2 .= " SELECT COUNT(*) as total2 FROM dtmlist WHERE dtmIsFinished = 1 AND dsfServiceFee = 0 ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 32)
    {
        $sql .= " AND companyName LIKE '%".$searchWord."%' ";
        $sql2 .= " AND companyName LIKE '%".$searchWord."%' ";
    }
}

if ($orderBy != "") 
{
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." DESC , driverID_FK ASC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC , driverID_FK ASC ";
    }
    else if($filter == 2)
    {
        $sql .= " ORDER BY ".$orderBy." ASC , driverID_FK ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC , driverID_FK ASC ";
    } 
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{

    $initialSql = "SELECT COUNT(*) as total from dtmlist WHERE dtmIsFinished = 1 AND dsfServiceFee = 0";


    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm dtmTableNoWrap table-hovered table-striped table-responsive-xl removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 32)
            {
                ?>
                    <th >Selection</th>
                    <th >DTM No</th>
                    <th >Pickup Date</th>
                    <th >Pickup Time</th>
                    <!-- <th >From </th>
                    <th >To</th>
                    <th >Agent</th> -->
                    <th >Consol</th>
                    <th >Truck No</th>
                    <th >Load Capacity</th>
                    <th >Driver 1</th>
                    <th >Driver 2</th>
                    <th >From Zones</th>
                    <th >To Zones</th>
                    <!-- <th >Gross DSF</th>
                    <th >Night</th>
                    <th >Detention</th>
                    <th >Sunday</th>
                    <th >Public Holiday</th>
                    <th >Total DSF</th> -->
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
    <tr>
        <?php 

            
            if($fromPage == 32)
            {
                
                ?>
                    <td class="">
                        <div class="">
                            <form action="addServiceFee.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                                <button class="btn btn-success edtOpt" value="<?php echo $row['dtmID_PK'];?>" name="add">Add</button>
                            </form>
                         </div>
                    </td>
                    <td class="text-center">
                    <?php 
                         echo $row['dtmID_PK'];
                    ?>
                    </td>
                    <td>
                    <?php 
                         $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                         echo $pickupDate;
                    ?>
                    </td>
                    <td>
                        <?php 
                        
                            $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                            echo $pickupTime;
                        ?>
                    </td>
                    <!-- <td>
                    <?php 
                        // $costCenterDisplay = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                        // $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        // if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        // {
                        //     while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                        //     {
                        //         echo $urow1['pointzonePlaceName'];
                        //     }
                        // }
                    ?>
                </td> -->
                <!-- <td>
                    <?php 
                        // $costCenterDisplay = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                        // $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        // if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        // {
                        //     while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                        //     {
                        //         echo $urow1['pointzonePlaceName'];
                        //     }
                        // }
                    ?>
                </td> -->
                <!-- <td>
                    <?php 
                        // $costCenterDisplay = "SELECT * FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                        // $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        // if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        // {
                        //     while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                        //     {
                        //         echo $urow1['companyName'];
                        //     }
                        // }
                    ?>
                </td> -->
                <td>
                    <?php 
                    if($row['isConsol'] == 1)
                    {
                        echo "CONSOL";
                    }
                    else
                    {
                        echo "-";
                    }
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT * FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['truckPlateNo'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['loadCap'];
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT * FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['driverName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php
                        if($row['driver2ID_FK'])
                        {
                            $costCenterDisplay = "SELECT * FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                            $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                            if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                            {
                                while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                {
                                    echo $urow1['driverName'];
                                }
                            }
                        }
                        else
                        {
                            echo "-";
                        }
                        
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['zonesName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmDestinationZoneID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['zonesName'];
                            }
                        }
                    ?>
                </td>
               
                <?php
            }
                ?>
        </tr>
    <?php 
            }
        }
        else
        {
            echo  $conn->error;
            if($fromPage == 32)
            {
                ?>
                    <tr>
                        <td colspan="13" style="text-align:center;">No Records Found</td>
                    </tr>
                <?php
            }
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 