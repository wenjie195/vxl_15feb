<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['add']))
        {   
            $add = $_POST['add'];
            require 'generalFunction.php';
            $conn = connDB();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Add DTM</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($add == 8)
                            {
                                echo "Coordinator Add DTM";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row">
                        <div class="col-xl-12 row">
                        <?php 
                            if($add == 8)
                            {
                                ?>                            
                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">DTM Info</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-dgfRef">DGF Reference</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-ssip-dgfRef">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-company">Select Agent</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-company" onchange="getThisCompanyToChangeCostCenter(this,1,null,1);">
                                        <option disabled selected hidden>-- Pick one company --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM company WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["companyID_PK"].'">'.$row["companyName"].' </option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-bookingDate" >Booking Date (YYYYMMDD)</label>
                                    <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="input-ssip-bookingDate" >
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-bookingTime" >Booking Time</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding timepicker" id="input-ssip-bookingTime" placeholder="hhmm" >
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originPoint">Origin Point</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-originPoint">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 

                                        $sql_select_costCenter = "SELECT * FROM pointzone WHERE showThis = 1 ORDER BY pointzonePlaceName ASC ";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                            {
                                                echo '<option value="'.$row["pointzoneID_PK"].'">'.$row["pointzonePlaceName"].'</option>';
                                            }
                                        } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destinationPoint" >Destination Point</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-destinationPoint">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 

                                        $sql_select_costCenter = "SELECT * FROM pointzone WHERE showThis = 1 ORDER BY pointzonePlaceName ASC ";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                            {
                                                echo '<option value="'.$row["pointzoneID_PK"].'">'.$row["pointzonePlaceName"].'</option>';
                                            }
                                        }  
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originZone" >Origin Zone</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-originZone">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destinationZone" >Destination Zone</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-destinationZone">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM zones WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["zonesID_PK"].'">'.$row["zonesName"].','.$row["zonesState"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5" id="showCostCenterByCompany"></div>
                                <div class="col-xl-6"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-pickupDate" >Pickup Date YYYYMMDD</label>
                                    <input type="text" class="form-control adminAddSetPadding " placeholder="YYYYMMDD" id="input-ssip-pickupDate">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-pickupTime" >Pickup Time (hh:mm AM/PM)</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" placeholder="hhmm" id="input-ssip-pickupTime">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-truckPlateNo" >Truck Plate No</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-truckPlateNo">
                                        <option disabled selected hidden>-- Pick one Truck --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM trucks WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                            {
                                                echo '<option value="'.$row["truckID_PK"].'">'.$row["truckPlateNo"].'</option>';
                                            }
                                        } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-urgentMemo" >Urgent Memo</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-urgentMemo">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                   
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-driverName" >Driver 1 Name</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-driverName">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php

                                        $sql_select_costCenter = "SELECT * FROM driver WHERE showThis = 1";
                                        $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        if (mysqli_num_rows($result_select_costCenter) > 0)
                                        {
                                            // output data of each row
                                            while($row = mysqli_fetch_assoc($result_select_costCenter))
                                            {
                                                echo '<option value="'.$row["driverID_PK"].'">'.$row["driverNickName"].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xl-6"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <input type="checkbox" name="secDriver" id="secDriver" onchange="showsecDriver();">
                                    <label for="secDriver" id="secDriverLabel">Add Another Driver ?</label>
                                    <div id="showSecond" style="display:none;">
                                        <label for="input-ssip-driverName2" >Driver 2 Name (Optional)</label>
                                        <select class="form-control adminAddSetPadding" id="input-ssip-driverName2">
                                            <option disabled selected hidden>-- Pick one of the following --</option>
                                            <?php

                                            $sql_select_costCenter = "SELECT * FROM driver WHERE showThis = 1";
                                            $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                            if (mysqli_num_rows($result_select_costCenter) > 0)
                                            {
                                                // output data of each row
                                                while($row = mysqli_fetch_assoc($result_select_costCenter))
                                                {
                                                    echo '<option value="'.$row["driverID_PK"].'">'.$row["driverNickName"].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6"></div>

                                <!-- <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Origin</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-f" >F</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-f" placeholder="hh:mm">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originDockTime" >Dock Time</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-originDockTime" placeholder="hh:mm" >
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originCompleteLoadingTime" >Complete Loading Time</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-originCompleteLoadingTime" placeholder="hh:mm">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-originDepartureTimeFromPost" >Departure Time From Security Post</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-originDepartureTimeFromPost" placeholder="hh:mm">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Destination</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destArrivalTimeAtPost" >Arrival Time at Security Post</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destArrivalTimeAtPost" placeholder="hh:mm">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destDockTime" >Dock Time</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destDockTime" placeholder="hh:mm">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destCompleteUnloadTime" >Complete Unloading Time</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destCompleteUnloadTime" placeholder="hh:mm">
                                </div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-destDepartTimeFromPost" >Departure Time from Security Post</label>
                                    <input type="text" name="timepicker" class="form-control adminAddSetPadding  timepicker" id="input-ssip-destDepartTimeFromPost" placeholder="hh:mm">
                                </div>
                                <div class="col-xl-1"></div> -->

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Quantity</p>
                                </div>
                                <div class="col-xl-8"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-10">
                                    <label for="input-ssip-quantityType" >Quantity Type</label>
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group displayBlockQuantity">
                                                <label class="cb-container cb-padding">Cartons
                                                    <input type="checkbox" id="input-ssip-quantityTypeCartons">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <input type="number" id="input-ssip-quantityCartons" class=" form-control adminAddSetPadding " min="0" style="width: auto;">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group displayBlockQuantity">
                                                <label class="cb-container cb-padding">Pallets
                                                    <input type="checkbox" id="input-ssip-quantityTypePallets">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <input type="number" id="input-ssip-quantityPallets" class=" form-control adminAddSetPadding " min="0" style="width: auto;">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group displayBlockQuantity">
                                                <label class="cb-container cb-padding">Cages/Crates
                                                    <input type="checkbox" id="input-ssip-quantityTypeCages">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <input type="number" id="input-ssip-quantityCages" class=" form-control adminAddSetPadding " min="0" style="width: auto;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-tripOnTime" >Trip On Time??</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-tripOnTime" onchange="reasonForLate();">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                                <div class="form-group col-xl-5">
                                    <div class="form-group dtmListingAdd-padInsideFG" id="hideThis" style="display: none;">
                                        <label for="input-ssip-delayReason" >Delay Reason</label>
                                        <input type="text" class="form-control adminAddSetPadding" id="input-ssip-delayReason">
                                    </div>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-ssip-tripOnTime" >Load Capacity</label>
                                    <select class="form-control adminAddSetPadding" id="input-ssip-loadCap">
                                        <option disabled selected hidden>-- Pick one capacity --</option>
                                        <option value="20 Ft / 10 Ton">20 Ft / 10 Ton</option>
                                        <option value="1 Tonner">1 Tonner</option>
                                        <option value="3 Ton"> 3 Ton</option>
                                        <option value="40 Ft / Q6"> 40 Ft / Q6</option>
                                        <option value="40 Ft / Q6 Air-Ride">40 Ft / Q6 Air-Ride</option>
                                        <option value="45 Ft / Q7">45 Ft / Q7</option>
                                    </select>
                                </div>
                                
                                <div class="form-group col-xl-5" >
                                    <label class="cb-container cb-padding " style="margin-top:2.5rem;">Is Consol ?
                                        <input type="checkbox" id="input-ssip-isConsol">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="dtmTrapezoid col-xl-4">
                                    <p class="dtmTrapezoidPara">Additional DTM</p>
                                </div>
                                <div class="col-xl-8"></div>
<!-- 
                                <div class="col-xl-1"></div> -->
                                <!-- <div class="form-group col-xl-5"> -->
                                    <!-- <label for="input-dtm-costCenter" >Cost Center</label>
                                    <select class="form-control adminAddSetPadding" id="input-dtm-costCenter">
                                        <option disabled selected hidden>-- Pick one of the following --</option>
                                        <?php 

                                        // $sql_select_costCenter = "SELECT * FROM costcenter WHERE showThis = 1";
                                        // $result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);

                                        // if (mysqli_num_rows($result_select_costCenter) > 0) 
                                        // {
                                        //     // output data of each row
                                        //     while($row = mysqli_fetch_assoc($result_select_costCenter)) 
                                        //     {
                                        //         echo '<option value="'.$row["costCenterID_PK"].'">'.$row["costCenterName"].'</option>';
                                        //     }
                                        // } 
                                        ?>
                                    </select> -->
                                <!-- </div> -->
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                    <label for="input-dtm-requestBy" >Requested By</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-dtm-requestBy">
                                </div>
                                

                                <div class="form-group col-xl-5">
                                    <label for="input-dtm-remark" >Remark</label>
                                    <input type="text" class="form-control adminAddSetPadding" id="input-dtm-remark">
                                </div>
                                <div class="col-xl-1"></div>

                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton dtmMarginSubmitButton" onclick="addDTMData(<?php echo $add;?>,1);">Create DTM</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
        $(document).ready(function()
        {
            //reasonForLate();
            
            var bookingDatePicker = new Pikaday({
                field: document.getElementById('input-ssip-bookingDate'),
                format: 'YYYYMMDD',
                onSelect: function() {
                    // console.log(this.getMoment().format('Do MMMM YYYY'));
                }
            });

            var pickupDatePicker = new Pikaday({
                field: document.getElementById('input-ssip-pickupDate'),
                format: 'YYYYMMDD',
                onSelect: function() {
                    // console.log(this.getMoment().format('Do MMMM YYYY'));
                }
            });

            // intializeTimePicker();
        });

        function intializeTimePicker() {
            $('.timepicker').timepicki({
                show_meridian:false,
                min_hour_value:0,
                max_hour_value:23,
                step_size_minutes: 1,
                overflow_minutes: true,
                increase_direction: 'up',
                start_time: ["12", "00", "PM"]
            });
        }
        function showsecDriver()
        {
            if($('#secDriver').prop('checked'))
            {
                $('#showSecond').css('display','block');
                $('#secDriverLabel').html("Uncheck to remove this driver");
            }
            else
            {
                $('#secDriverLabel').html("Add Another Driver ?");
                $('#showSecond').css('display','none');
            }
        }

        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>