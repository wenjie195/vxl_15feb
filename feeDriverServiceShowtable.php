<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

// echo $fromPage;
// echo $condition;
// echo $pageNo;
// echo $filter;
// echo " =".$searchWord."=";

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}

$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 33)
    {
        $orderBy = "adddriverservicefee.dateCreated";
    }
}
if($filter == 2)
{
    if($fromPage == 33)
    {
        $orderBy = "dtmlist.dtmPickupDate";
    }
}

if($filter == 3)
{
    if($fromPage == 33)
    {
        $orderBy = "driver.driverName";
    }
}



$sql = "";
$sql2 = "";

if($fromPage == 33)
{
    $sql .= " SELECT * FROM ((adddriverservicefee 
    INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
    INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK) ";

    $sql2 .= " SELECT COUNT(*) as total2 FROM ((adddriverservicefee 
    INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
    INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK) ";
}


if($searchWord != null && $searchWord != "")
{
    if($fromPage == 33)
    {
        if($filter == 1)
        {
            $searchWord = date("Y-m-d", strtotime($searchWord));

            $sql .= " WHERE adddriverservicefee.dateCreated LIKE '".$searchWord." 00:00:00'  ";
            $sql2 .= " WHERE adddriverservicefee.dateCreated LIKE '".$searchWord." 00:00:00'  ";
        }
        else if($filter == 2)
        {
            $searchWord = date("Y-m-d", strtotime($searchWord));
            
            $sql .= " WHERE dtmlist.dtmPickupDate LIKE '".$searchWord." 00:00:00'  ";
            $sql2 .= " WHERE dtmlist.dtmPickupDate LIKE '".$searchWord." 00:00:00'  ";
        }
        else if($filter == 3)
        {
            $sql .= " WHERE driver.driverName LIKE '%".$searchWord."%'  ";
            $sql2 .= " WHERE driver.driverName LIKE '%".$searchWord."%'  ";
        }
    }
}

if ($orderBy != "") 
{
    if($filter == 1)
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
    else if($filter == 2)
    {
        $sql .= " ORDER BY ".$orderBy." DESC , dtmPickupTime DESC";
        $sql2 .= " ORDER BY ".$orderBy." DESC , dtmPickupTime DESC";
    }
    else
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";
// echo "<br>'".$sql. "'<br>'".$sql2."'";

if($condition == 1)
{
    if($fromPage == 33)
    {
        $initialSql = " SELECT COUNT(*) as total from ((adddriverservicefee 
        INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
        INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK) ";
    }

    $result = mysqli_query($conn,$initialSql);
    $data = mysqli_fetch_assoc($result);
    $no_of_pages = 0;
    $no_of_pages = ceil($data['total'] / 10);
}
else
{
    $result2 = mysqli_query($conn,$sql2);
    $dataCount = mysqli_fetch_assoc($result2);
    $no_of_pages = 0;
    $no_of_pages = ceil($dataCount['total2'] / 10);
    
}

$querylisting = mysqli_query($conn,$sql);


generateConfirmationModal();
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered table-striped table-responsive-xl dtmTableNoWrap removebottommargin">
    <thead>
        <tr>
        <?php 
            if($fromPage == 33)
            {
                ?>
                    <th colspan="2">Selection</th>
                    <th>DTM ID</th>
                    <th>DSF No</th>
                    <th>Driver Name</th>
                    <th>Gross Service Fee<br/>/Consol Fee</th>

                    <!-- <th>Detention</th>
                    <th>Overnight</th> -->
                    <th>Night Deliveries</th>
                    <!-- <th>Sunday</th>
                    <th>Public</th>
                    <th>Cancellation</th>
                    <th>Lockup(Day)</th>
                    <th>Lockup(Night)</th> -->
                    <th>Sunday</th>
                    <th>Public</th>

                   
                    <th>Total Service Fee</th>
                    <th>Pickup Date</th>
                    <th>Pickup Time</th>
                    <!-- <th>From</th>
                    <th>To</th>
                    <th>Agent</th> -->
                    <th>Truck No</th>
                    <!-- <th>CAP</th> -->
                    <th>Consol Status</th>
                    <th>From Zone</th>
                    <th>To Zone</th>
                <?php
            }
        ?>
        </tr>
  </thead>
  <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
                $a = "SELECT * FROM additionalservicefee WHERE loadTrans = '".$row['loadCap']."'";
                $aa = mysqli_query($conn,$a);
                if (mysqli_num_rows($aa) > 0) 
                {
                    while($urow1 = mysqli_fetch_array($aa))
                    {
                        $overnight = $urow1['overnight'];
                        $nightDeliveries = $urow1['nightDeliveries'];
                        $sunday = $urow1['sunday'];
                        $public = $urow1['public'];
                        $lockupNight = $urow1['lockupNight'];
                        $lockupDay = $urow1['lockupDay'];
                        $cancellation = $urow1['cancel'];
                    }
                }

    ?>
    <tr>
            <?php 
                if($fromPage == 33)
                {  

                    $totalSer = 0;
            ?>
               <td class="">
               <div class="adminAlignOptionInline">
                    <form method ="POST">
                        <?php if($row['dsfComplete'] == 1)
                        {
                            ?> <input type="checkbox" value="<?php echo $row['id'];?>"  onchange="handleServiceFee(this);" name="invoiceID" checked ><?php
                        }
                        else
                        {
                            ?> <input type="checkbox" value="<?php echo $row['id'];?>"  onchange="handleServiceFee(this);" name="invoiceID"><?php
                        }
                        ?>
                        
                    </form>
               </div>
               </td>
               <td class="">
               <div class="adminAlignOptionInline">
                        <?php if($row['dsfComplete'] == 0)
                        {
                            ?><form action="dsfEdit.php" method="POST" class="adminformEdit">
                            <input type="hidden" name="tableType" value="<?php echo $fromPage;?>">
                            <button class="btn btn-warning edtOpt" value="<?php echo $row['id'];?>" name="edit">Update</button>
                        </form><?php
                        }
                        ?>
               </div>
               </td>
               <td>
                    <?php
                        echo $row['dtmID_FK'];
                    ?>
               </td>
               <td>
               <?php
                        echo $row['id'];
                    ?>
               </td>
               <td>
                <?php
                        echo $row['driverName'];
                    ?>
               </td>
               <td class="text-center">
                    <?php
                        $totalSer += $row['dsfGross'];
                        echo "RM ".$row['dsfGross'];
                    ?>
               </td>
               <!-- <td class="text-center">
                    <?php
                        // if($row['detention'] != 0)
                        // {
                        //     $totalSer += $row['detention'];
                        //     echo "RM ".$row['detention'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td>
               <td class="text-center">
                    <?php
                        // if($row['isovernight'] == 1)
                        // {
                        //     $totalSer += $row['dsfOvernight'];
                        //     echo "RM ".$row['dsfOvernight'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td> -->
               <td class="text-center">
                    <?php
                        if($row['isnightDeliveries'] == 1)
                        {
                            $totalSer += $row['dsfNightDelivery'];
                            echo "RM ".$row['dsfNightDelivery'] ;
                        }
                        else
                        {
                            echo "-";
                        }
                    ?>
               </td>
               <!-- <td class="text-center">
                <?php
                        // if($row['issunday'] == 1)
                        // {
                        //     $totalSer += $row['dsfSunday'];
                        //     echo "RM ".$row['dsfSunday'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td>
               <td class="text-center">
                    <?php
                        // if($row['ispublic'] == 1)
                        // {
                        //     $totalSer += $row['dsfPublic'];
                        //     echo "RM ".$row['dsfPublic'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td>
               <td class="text-center">
                    <?php
                        // if($row['iscancelllation'] == 1)
                        // {
                        //     $totalSer += $row['dsfCancellation'];
                        //     echo "RM ".$row['dsfCancellation'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td>
               <td class="text-center">
               <?php
                        // if($row['islockupDay'] == 1)
                        // {
                        //     $totalSer += $row['dsfLockupDay'];
                        //     echo "RM ".$row['dsfLockupDay'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td>
               <td class="text-center">
               <?php
                        // if($row['islockupNight'] == 1)
                        // {
                        //     $totalSer += $row['dsfLockupNight'];
                        //     echo "RM ".$row['dsfLockupNight'] ;
                        // }
                        // else
                        // {
                        //     echo "-";
                        // }
                    ?>
               </td> -->
               <td class="text-center">
                <?php
                        if($row['issunday'] == 1)
                        {
                            $totalSer += $row['dsfSunday'];
                            echo "RM ".$row['dsfSunday'] ;
                        }
                        else
                        {
                            echo "-";
                        }
                    ?>
               </td>
               <td class="text-center">
                    <?php
                        if($row['ispublic'] == 1)
                        {
                            $totalSer += $row['dsfPublic'];
                            echo "RM ".$row['dsfPublic'] ;
                        }
                        else
                        {
                            echo "-";
                        }
                    ?>
               </td>
               <td class="text-center">
                    <?php
                    echo "RM ".sprintf('%0.2f',$totalSer);
                    ?>
               </td>
               <td>
                    <?php 
                    $a = "SELECT dtmPickupDate FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                    $aa = mysqli_query($conn,$a);
                    if (mysqli_num_rows($aa) > 0) 
                    {
                        while($urow1 = mysqli_fetch_array($aa))
                        {
                            $pickupDate = date("d M Y",strtotime($urow1['dtmPickupDate']));
                            echo $pickupDate;
                        }
                    }
                         
                    ?>
                    </td>
                    <td class="text-center">
                        <?php 
                         $a = "SELECT dtmPickupTime FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                         $aa = mysqli_query($conn,$a);
                         if (mysqli_num_rows($aa) > 0) 
                         {
                             while($urow1 = mysqli_fetch_array($aa))
                             {
                                 $pickupDate = date("G:i",strtotime($urow1['dtmPickupTime']));
                                 echo $pickupDate;
                             }
                         }
                        ?>
                    </td>
                    <!-- <td>
                        <?php 
                        //  $a = "SELECT dtmOriginPointID_FK FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                        //  $aa = mysqli_query($conn,$a);
                        //  if (mysqli_num_rows($aa) > 0) 
                        //  {
                        //      while($urow1 = mysqli_fetch_array($aa))
                        //      {
                        //         $ab = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$urow1['dtmOriginPointID_FK'];
                        //         $aab = mysqli_query($conn,$ab);
                        //         if (mysqli_num_rows($aab) > 0) 
                        //         {
                        //             while($urow2 = mysqli_fetch_array($aab))
                        //             {
                                        
                        //                 echo $urow2['pointzonePlaceName'];
                        //             }
                        //         }
                        //      }
                        //  }
                        ?>
                    </td>
                    <td>
                        <?php 
                        //  $a = "SELECT dtmDestinationPointID_FK FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                        //  $aa = mysqli_query($conn,$a);
                        //  if (mysqli_num_rows($aa) > 0) 
                        //  {
                        //      while($urow1 = mysqli_fetch_array($aa))
                        //      {
                        //         $ab = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$urow1['dtmDestinationPointID_FK'];
                        //         $aab = mysqli_query($conn,$ab);
                        //         if (mysqli_num_rows($aab) > 0) 
                        //         {
                        //             while($urow2 = mysqli_fetch_array($aab))
                        //             {
                                        
                        //                 echo $urow2['pointzonePlaceName'];
                        //             }
                        //         }
                        //      }
                        //  }
                        ?>
                    </td> -->
                    <!-- <td>
                        <?php 
                        //  $a = "SELECT companyID_FK FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                        //  $aa = mysqli_query($conn,$a);
                        //  if (mysqli_num_rows($aa) > 0) 
                        //  {
                        //      while($urow1 = mysqli_fetch_array($aa))
                        //      {
                        //         $ab = "SELECT companyName FROM company WHERE companyID_PK = ".$urow1['companyID_FK'];
                        //         $aab = mysqli_query($conn,$ab);
                        //         if (mysqli_num_rows($aab) > 0) 
                        //         {
                        //             while($urow2 = mysqli_fetch_array($aab))
                        //             {
                                        
                        //                 echo $urow2['companyName'];
                        //             }
                        //         }
                        //      }
                        //  }
                        ?>
                    </td> -->
                    <td>
                        <?php 
                         $a = "SELECT truckID_FK FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                         $aa = mysqli_query($conn,$a);
                         if (mysqli_num_rows($aa) > 0) 
                         {
                             while($urow1 = mysqli_fetch_array($aa))
                             {
                                $ab = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$urow1['truckID_FK'];
                                $aab = mysqli_query($conn,$ab);
                                if (mysqli_num_rows($aab) > 0) 
                                {
                                    while($urow2 = mysqli_fetch_array($aab))
                                    {
                                        
                                        echo $urow2['truckPlateNo'];
                                    }
                                }
                             }
                         }
                        ?>
                    </td>
                    <!-- <td>
                        <?php 
                        //  $a = "SELECT loadCap FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                        //  $aa = mysqli_query($conn,$a);
                        //  if (mysqli_num_rows($aa) > 0) 
                        //  {
                        //      while($urow1 = mysqli_fetch_array($aa))
                        //      {
                        //         echo $urow1['loadCap']; 
                        //      }
                        //  }
                        ?>
                    </td> -->
                    <td class="text-center">
                        <?php 
                         $a = "SELECT isConsol FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                         $aa = mysqli_query($conn,$a);
                         if (mysqli_num_rows($aa) > 0) 
                         {
                             while($urow1 = mysqli_fetch_array($aa))
                             {
                                 if($urow1['isConsol'] == 1)
                                 {
                                    echo "CONSOL"; 
                                 }
                                 else
                                 {
                                    echo "-"; 
                                 }
                             }
                         }
                        ?>
                    </td>
                    <td>
                        <?php 
                         $a = "SELECT dtmOriginZoneID_FK FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                         $aa = mysqli_query($conn,$a);
                         if (mysqli_num_rows($aa) > 0) 
                         {
                             while($urow1 = mysqli_fetch_array($aa))
                             {
                                $ab = "SELECT zonesName FROM zones WHERE zonesID_PK = ".$urow1['dtmOriginZoneID_FK'];
                                $aab = mysqli_query($conn,$ab);
                                if (mysqli_num_rows($aab) > 0) 
                                {
                                    while($urow2 = mysqli_fetch_array($aab))
                                    {
                                        
                                        echo $urow2['zonesName'];
                                    }
                                }
                             }
                         }
                        ?>
                    </td>
                    <td>
                        <?php 
                         $a = "SELECT dtmDestinationZoneID_FK FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                         $aa = mysqli_query($conn,$a);
                         if (mysqli_num_rows($aa) > 0) 
                         {
                             while($urow1 = mysqli_fetch_array($aa))
                             {
                                $ab = "SELECT zonesName FROM zones WHERE zonesID_PK = ".$urow1['dtmDestinationZoneID_FK'];
                                $aab = mysqli_query($conn,$ab);
                                if (mysqli_num_rows($aab) > 0) 
                                {
                                    while($urow2 = mysqli_fetch_array($aab))
                                    {
                                        
                                        echo $urow2['zonesName'];
                                    }
                                }
                             }
                         }
                        ?>
                    </td>
               <?php 
                    
                }
            ?>
        </tr>
    <?php 
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="15" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
        }
    ?>
  </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>
 