<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>HR Home</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>Driver And Truck Dashboard</h3>
                    </div>
                    <div class="row">
                        <div class="col adminAlignCenter">
                            <a href="driverHome.php" class="adminAlignCenterGrid">
                                <img src="./img/adminUsers.png" width="180px" height="180px">
                                <p class="adminLinkRef">Drivers</p>
                            </a>
                        </div>
                        <div class="col adminAlignCenter">
                            <a href="truckHome.php" class="adminAlignCenterGrid">
                                <img src="./img/hrDriver.png" width="220px" height="130px" style="margin-top:50px;">
                                <p class="adminLinkRef">Trucks</p>
                            </a>
                        </div>
                        <!-- <div class="col adminAlignCenter">
                            <a href="dsfHome.php" class="adminAlignCenterGrid">
                                <img src="./img/adminUsers.png" width="180px" height="180px">
                                <p class="adminLinkRef">Driver Service Fee</p>
                            </a>
                        </div> -->
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>