-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2019 at 08:29 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_vxl`
--

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driverID_PK` int(255) NOT NULL,
  `driverDateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `driverDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `driverName` varchar(255) NOT NULL,
  `driverNickName` varchar(255) NOT NULL,
  `driverICno` varchar(255) NOT NULL,
  `driverPhoneNo` varchar(10) NOT NULL,
  `insertedByID_FK` int(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driverID_PK`, `driverDateUpdated`, `driverDateCreated`, `driverName`, `driverNickName`, `driverICno`, `driverPhoneNo`, `insertedByID_FK`, `showThis`) VALUES
(0, '2019-06-17 06:22:20', '2019-06-17 03:49:44', 'Muhammad Azmi Bin Shapee', 'Azmi', '970126075049', '0164095049', 1, 1),
(25, '2019-06-17 06:25:38', '2019-06-17 03:33:39', 'Zainuddin Bin Mat Said', 'Zainuddin', '841104075645', '0111249998', 1, 1),
(26, '2019-06-17 06:27:11', '2019-06-17 03:34:15', 'Adam Bin Omar', 'Adam', '920328075543', '0174460560', 1, 1),
(27, '2019-06-17 06:27:01', '2019-06-17 03:34:49', 'Ku Risyam Bin ku Bahador', 'Risyam', '840522025945', '0194770338', 1, 1),
(28, '2019-06-17 06:26:50', '2019-06-17 03:35:47', 'Ahmad Nazri Bin Basir', 'Nazri ', '750530075695', '0194401338', 1, 1),
(29, '2019-06-17 06:26:39', '2019-06-17 03:36:22', 'Mohd Hatta Bin Mohd Isa', 'Hatta', '820617025149', '0112055119', 1, 1),
(30, '2019-06-17 06:26:16', '2019-06-17 03:37:03', 'Muhammad Fifi Azri Bin Mazlan', 'Azri', '960428075547', '0194736742', 1, 1),
(31, '2019-06-17 06:26:04', '2019-06-17 03:37:52', 'Mohd Nasir Bin Ramli', 'Nasir', '860403355683', '0135973061', 1, 1),
(32, '2019-06-17 06:25:54', '2019-06-17 03:38:28', 'Muhamat Faisal Bin Othman', 'Faisal', '831111025137', '0124431338', 1, 1),
(33, '2019-06-17 06:25:27', '2019-06-17 03:39:15', 'Norhazuan Bin Zakaria', 'Norhazuan', '761215086101', '0194402338', 1, 1),
(34, '2019-06-17 06:25:17', '2019-06-17 03:39:54', 'Muhammad Adib Bin Mohd Arshad', 'Adib', '910622075537', '0189192625', 1, 1),
(35, '2019-06-17 06:25:08', '2019-06-17 03:40:37', 'Muhamad Hanafi Bin Hamzi', 'Hanafi', '931209085515', '0172006399', 1, 1),
(36, '2019-06-17 06:24:59', '2019-06-17 03:41:30', 'Shah Rizal Bin Abdul Sahak', 'Rizal', '870427385341', '0124706339', 1, 1),
(37, '2019-06-17 06:24:49', '2019-06-17 03:42:14', 'Adli Shafaril Bin Shafaiee', 'Shafaril', '910914086609', '0124719338', 1, 1),
(38, '2019-06-17 06:24:38', '2019-06-17 03:43:01', 'Muhamad Zambri Bin Jamil', 'Zambri', '810421075647', '0174285977', 1, 1),
(39, '2019-06-17 06:24:28', '2019-06-17 03:43:43', 'Mohd Ezad Bin Ellias', 'Ezad', '830302075415', '0194090338', 1, 1),
(40, '2019-06-17 06:24:16', '2019-06-17 03:44:14', 'Mohd Noor Bin Bakar', 'Noor', '860331265637', '0194736754', 1, 1),
(41, '2019-06-17 06:24:04', '2019-06-17 03:44:58', 'Hidayat Bin Abd Latif', 'Hidayat', '750422085191', '0124856338', 1, 1),
(42, '2019-06-17 06:23:47', '2019-06-17 03:45:39', 'Fazli Bin Jamaludin ', 'Fazli', '770808075293', '0135120239', 1, 1),
(43, '2019-06-17 06:23:33', '2019-06-17 03:46:28', 'Mohd Nazrul Hafiz Bin Abdul Latif', 'Hafiz', '891009026103', '0124838399', 1, 1),
(44, '2019-06-17 06:23:12', '2019-06-17 03:47:00', 'Azizul Bin Md Nawi', 'Azizul', '961215295049', '0124694883', 1, 1),
(45, '2019-06-17 06:22:54', '2019-06-17 03:47:41', 'Muhamad Husni Bin Che Mat Nazan', 'Husni', '940226035645', '0122284339', 1, 1),
(46, '2019-06-17 06:22:43', '2019-06-17 03:48:21', 'Mohd Alhafiz Shukiran Bin Norahaisham', 'Shukiran', '941212075736', '0194736746', 1, 1),
(47, '2019-06-17 06:22:28', '2019-06-17 03:49:07', 'Muhammad Hamizan Bin Razalli', 'Hamizan', '930226086099', '0194491203', 1, 1),
(49, '2019-06-17 06:22:05', '2019-06-17 03:50:15', 'Samsuri Bin Hussain', 'Samsuri', '761203075085', '0194760338', 1, 1),
(50, '2019-06-17 06:21:58', '2019-06-17 03:50:52', 'Mohd Aizat Bin Mohd Noor', 'Aizat', '860829355541', '0174408399', 1, 1),
(51, '2019-06-17 06:21:49', '2019-06-17 03:52:02', 'Ezlee Bin Abdul Manap', 'Ezlee', '810528025623', '0194736764', 1, 1),
(52, '2019-06-17 06:21:39', '2019-06-17 03:52:47', 'Mohd Hafeez Bin Housni', 'Hafeez', '920924075275', '0124716338', 1, 1),
(53, '2019-06-17 06:21:30', '2019-06-17 03:53:34', 'Mohamad Efendi Bin Abdul Halim', 'Efendi', '780905025895', '0179791339', 1, 1),
(54, '2019-06-17 06:21:20', '2019-06-17 03:54:04', 'Mohd Zahir Bin Hashim', 'Zahir', '831011075583', '0124341338', 1, 1),
(55, '2019-06-17 06:21:06', '2019-06-17 03:54:36', 'Mohamad Ridzuan Bin Wan Min', 'Ridzuan', '900105086511', '0124445338', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driverID_PK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driverID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
