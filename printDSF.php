<?php
require 'generalFunction.php';
$fromDate = $_POST['fromDate'];
$toDate = $_POST['toDate'];
$driverID = $_POST['driverID'];
$conn = connDB();
function getDatePHPWithTime($dateVar,$type) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $date = date("Y-m-d", strtotime($dateVar));
    $time = date("H-i-s", strtotime("23:59:59"));

    return $date." ".$time;
}

date_default_timezone_set("Asia/Kuala_Lumpur");
$todayDate = date("Ymd_his");
$filename = 'filename="DSF_'.getDriver($driverID).'_'.$fromDate.'_'.$toDate.'.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;


require 'phpexcel/vendor/autoload.php';
ob_end_clean();




$fromDateFormatted = getDatePHP($fromDate);
$toDateFormatted = getDatePHPWithTime($toDate,0);
// $toDateFormatted = getDatePHP($toDate);
function putDecimalPoints($alphabetBefore, $alphabetAfter,$line,$sheet)
{
     for($x=$alphabetBefore; $x != $alphabetAfter; $x++)
     {
          $sheet->getStyle($x.$line)->getNumberFormat()->setFormatCode('0.00'); 
     }
}
function getUser($uid)
{
     $conn = connDB();
     $sqlA = " SELECT userName FROM user WHERE  userID_PK = ".$uid;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['userName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getPlace($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['pointzonePlaceName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCompany($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT companyName FROM company WHERE companyID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['companyName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getDriver($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT driverName FROM driver WHERE driverID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['driverName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getTruckPlateNo($placeId)
{
     $conn = connDB();
     $sqlA = " SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$placeId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['truckPlateNo'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getCostCenter($ccId)
{
     $conn = connDB();
     $sqlA = " SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$ccId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['costCenterName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function getZones($zonesId)
{
     $conn = connDB();
     $sqlA = " SELECT zonesName FROM zones WHERE zonesID_PK = ".$zonesId;
     $result1A = mysqli_query($conn,$sqlA);
     
     if (mysqli_num_rows($result1A) > 0) 
     {
          while($row3 = mysqli_fetch_array($result1A))
          { 
               $username = $row3['zonesName'];      
               return $username;        
          }
     }
     else
     {
          return "";
     }
}
function putBorderStyleRow($sheet,$cellAlphabet,$type = null,$colour = null)
{
     if($type && $type == 1)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
     }
     if($type && $type == 2)
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          if($colour)
          {
               $sheet->getStyle($cellAlphabet)
               ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
               $sheet->getStyle($cellAlphabet)
               ->getFill()->getStartColor()->setARGB($colour);
          }

     }
     else
     {
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          $sheet->getStyle($cellAlphabet)
          ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     }
     
} 

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$compareDate = null;
$lineBefore = null;
$dailyTotal = 0;
$calculateGrossDSF = 0;

$sql_select_costCenter = "SELECT * FROM ((adddriverservicefee 
INNER JOIN dtmlist ON adddriverservicefee.dtmID_FK = dtmlist.dtmID_PK)
INNER JOIN driver ON adddriverservicefee.driverID_FK = driver.driverID_PK)
WHERE adddriverservicefee.driverID_FK = '$driverID'
AND adddriverservicefee.dsfComplete = 1 
AND dtmlist.dtmPickupDate BETWEEN '$fromDateFormatted' AND '$toDateFormatted' 
ORDER BY dtmlist.dtmPickupDate ASC ";
$result_select_costCenter = mysqli_query($conn, $sql_select_costCenter);
$queryResult = mysqli_num_rows($result_select_costCenter);
if ($queryResult > 0)
{
    // output data of each row
     $line = 7;
     while($row = mysqli_fetch_assoc($result_select_costCenter))
     {
          $dsfID = $row['id'];
          $dtmID = $row['dtmID_PK'];
          $dtmPickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
          
          $dtmPickupTime = $row['dtmPickupTime'];
          $dtmOriginPointID_FK = getPlace($row['dtmOriginPointID_FK']);
          $dtmDestinationPointID_FK = getPlace($row['dtmDestinationPointID_FK']);
          $companyName = getCompany($row['companyID_FK']);
          $truckNo = getTruckPlateNo($row['truckID_FK']);
          $loadCap = $row['loadCap'];
          $driverName = $row['driverName'];
          $costcenterTemp = getCostCenter($row['costCenterID_FK']);
          $remarkDsf = $row['remarkDsf'];
          $dtmRemarks = $row['dtmRemarks'];
          $dsfTotal = 0;
          
          $dsfGross = $row['dsfGross'];
          $detention = $row['detention'];

          $dsfTotal+=$dsfGross ;

          $dsfTotal+=$detention ;
          // $isovernight = $row['isovernight'];
          $isnightDeliveries = $row['isnightDeliveries'];
          $issunday = $row['issunday'];
          $ispublic = $row['ispublic'];
          // $iscancelllation = $row['iscancelllation'];
          $islockupDay = $row['islockupDay'];
          $islockupNight = $row['islockupNight'];

          $dsfOvernight = $row['dsfOvernight'];
          $dsfNightDelivery = $row['dsfNightDelivery'];
          $dsfSunday = $row['dsfSunday'];
          $dsfPublic = $row['dsfPublic'];
          $dsfCancellation = $row['dsfCancellation'];
          $dsfLockupDay = $row['dsfLockupDay'];
          $dsfLockupNight = $row['dsfLockupNight'];

          $showConsolDP = "-";
          $isConsol = $row['isConsol'];
          if($isConsol == 1)
          {
               $showConsolDP = "CONSOL";
          }
          $dtmOriginZoneID_FK = getZones($row['dtmOriginZoneID_FK']);
          $dtmDestinationZoneID_FK = getZones($row['dtmDestinationZoneID_FK']);

          $sqlA = " SELECT * FROM additionalservicefee WHERE loadTrans = '".$loadCap."'";
          $result1A = mysqli_query($conn,$sqlA);
          
          if (mysqli_num_rows($result1A) > 0) 
          {
               while($row3 = mysqli_fetch_array($result1A))
               {    
                    // if($isovernight == 1)
                    // {
                    //      $overnight = $dsfOvernight;
                    //      $dsfTotal+=$overnight;
                    // }
                    // else
                    // {
                    //      $overnight = 0;
                    // }
                    if($isnightDeliveries == 1)
                    {
                        $nightDeliveries = $dsfNightDelivery;
                         $dsfTotal+=$nightDeliveries;
                    }
                    else
                    {
                         $nightDeliveries=0;
                    }

                    if($issunday == 1)
                    {
                         $sunday = $dsfSunday;
                         $dsfTotal+=$sunday;
                    }
                    else
                    {
                         $sunday=0;
                    }

                    if($ispublic == 1)
                    {
                         $public = $dsfPublic;
                         $dsfTotal+=$public;
                    }
                    else
                    {
                         $public=0;
                    }

                    if($islockupNight == 1)
                    {
                         $lockupNight = $dsfLockupNight;
                         $dsfTotal+=$lockupNight;
                    }
                    else
                    {
                         $lockupNight=0;
                    }

                    if($islockupDay == 1)
                    {
                         $lockupDay = $dsfLockupDay;
                         $dsfTotal+=$lockupDay;
                    }
                    else
                    {
                         $lockupDay=0;
                    }

                    // if($iscancelllation == 1)
                    // {
                    //      $cancel = $dsfCancellation;
                    //      $dsfTotal+=$cancel;
                    // }
                    // else
                    // {
                    //      $cancel=0;
                    // }            
               }
          }
          // $sheet->setCellValue('V'.($line-1),'=SUM(U'.($lineBefore).':U'.($line-1).')');
          // echo $compareDate." AND ".$dtmPickupDate."<br>";
          if($compareDate == null)
          {
               $lineBefore = $line;
               $compareDate = $dtmPickupDate ;
               $dailyTotal+= $dsfTotal;
               $calculateGrossDSF+=$dsfGross;

               if(($line-6) == $queryResult)
               {
                    $sheet->setCellValue('L'.$line, sprintf('%0.2f',$calculateGrossDSF));
                    $sheet->setCellValue('V'.$line,'=SUM(U'.($lineBefore).':U'.$line.')');
                    $sheet->getStyle('V'.$line)->getNumberFormat()->setFormatCode('0.00'); 
                    // echo "4 null<br>";
               }
               // echo "1<br>";
          }
          else
          {
               if($compareDate != $dtmPickupDate)
               {
                    $sheet->setCellValue('L'.($line-1), sprintf('%0.2f',$calculateGrossDSF));
                    $sheet->setCellValue('V'.($line-1),'=SUM(U'.($lineBefore).':U'.($line-1).')');
                    $sheet->getStyle('V'.($line-1))->getNumberFormat()->setFormatCode('0.00'); 
                    $compareDate = $dtmPickupDate ;
                    $lineBefore = $line;
                    $dailyTotal = 0;
                    $calculateGrossDSF=0;
                    $dailyTotal+= $dsfTotal;
                    $calculateGrossDSF+=$dsfGross;
                    if(($line-6) == $queryResult)
                    {
                         $sheet->setCellValue('L'.$line, sprintf('%0.2f',$calculateGrossDSF));
                         $sheet->setCellValue('V'.$line,'=SUM(U'.($lineBefore).':U'.$line.')');
                         $sheet->getStyle('V'.$line)->getNumberFormat()->setFormatCode('0.00'); 
                         // echo "4 top<br>";
                    }
                    // echo "2<br>";
               }
               else 
               {
                    $dailyTotal+= $dsfTotal;
                    $calculateGrossDSF+=$dsfGross;
                   
                    if(($line-6) == $queryResult)
                    {
                         $sheet->setCellValue('L'.$line, sprintf('%0.2f',$calculateGrossDSF));
                         $sheet->setCellValue('V'.$line,'=SUM(U'.($lineBefore).':U'.$line.')');
                         $sheet->getStyle('V'.$line)->getNumberFormat()->setFormatCode('0.00');
                         // echo "4<br>";
                    }
                    // echo "3<br>";
               }
          }

          // echo $dailyTotal."<br>";


          $sheet->setCellValue('A'.$line, $dtmPickupDate);
          $sheet->setCellValue('B'.$line, $dtmPickupTime);
          $sheet->setCellValue('C'.$line, $dtmOriginPointID_FK);
          $sheet->setCellValue('D'.$line, $dtmDestinationPointID_FK);
          $sheet->setCellValue('E'.$line, $companyName);
          $sheet->setCellValue('F'.$line, $truckNo);
          $sheet->setCellValue('G'.$line, $loadCap);
          $sheet->setCellValue('H'.$line, $showConsolDP);
          $sheet->setCellValue('I'.$line, $remarkDsf);
          $sheet->setCellValue('J'.$line, $dtmRemarks);
          $sheet->setCellValue('K'.$line, sprintf('%0.2f',$dsfGross));

          $sheet->setCellValue('M'.$line, sprintf('%0.2f',$detention));
          // $sheet->setCellValue('N'.$line, sprintf('%0.2f',$overnight));
          $sheet->setCellValue('O'.$line, sprintf('%0.2f',$nightDeliveries));
          $sheet->setCellValue('P'.$line, sprintf('%0.2f',$sunday));
          $sheet->setCellValue('Q'.$line, sprintf('%0.2f',$public));
          // $sheet->setCellValue('R'.$line, sprintf('%0.2f',$cancel));
          $sheet->setCellValue('S'.$line, sprintf('%0.2f',$lockupDay));
          $sheet->setCellValue('T'.$line, sprintf('%0.2f',$lockupNight));
     
          $sheet->setCellValue('U'.$line, '=SUM(M'.$line.':T'.$line.')+K'.$line);
          $sheet->getStyle('A'.$line.':W'.$line)->getAlignment()->setHorizontal('center');
          $sheet->getStyle('U'.$line)->getAlignment()->setHorizontal('center');
          $sheet->setCellValue('W'.$line, $dtmOriginZoneID_FK);
          $sheet->setCellValue('X'.$line, $dtmDestinationZoneID_FK);

          putDecimalPoints('K','V',$line,$sheet);

          $line++;
     }
}
else
{
     echo $conn->error;
}


if($compareDate == null)
{
     $compareDate = $dtmPickupDate ;
}
else
{
     if($compareDate != $dtmPickupDate)
     {
          $compareDate = $dtmPickupDate ;
     }
}



$sheet->setCellValue('A1', 'Driver Name');
$sheet->setCellValue('B1', $driverName);
$sheet->setCellValue('A2', 'Date From');
$sheet->setCellValue('B2', $fromDate);
$sheet->setCellValue('C2', 'To');
$sheet->setCellValue('D2', $toDate);

$sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
$sheet->getStyle('C2')->getAlignment()->setHorizontal('center');
$sheet->getStyle('D2')->getAlignment()->setHorizontal('center');

$sheet->setCellValue('A6', 'Pickup Date');
$sheet->getColumnDimension('A')->setWidth(15);
$sheet->setCellValue('B6', 'Pickup Time');
$sheet->getColumnDimension('B')->setWidth(25);
$sheet->setCellValue('C6', 'From');
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->setCellValue('D6', 'To');
$sheet->getColumnDimension('D')->setWidth(20);
$sheet->setCellValue('E6', 'Agent');
$sheet->getColumnDimension('E')->setWidth(25);
$sheet->setCellValue('F6', 'Truck No');
$sheet->getColumnDimension('F')->setWidth(20);
$sheet->setCellValue('G6', 'Load Capacity');
$sheet->getColumnDimension('G')->setWidth(20);
$sheet->setCellValue('H6', 'Consol Status');
$sheet->getColumnDimension('H')->setWidth(25);
$sheet->setCellValue('I6', 'DSF Remark');
$sheet->getColumnDimension('I')->setWidth(25);
$sheet->setCellValue('J6', 'DTM Remark');
$sheet->getColumnDimension('J')->setWidth(25);
$sheet->setCellValue('K6', 'DSF Gross (RM)');
$sheet->getColumnDimension('K')->setWidth(25);
$sheet->setCellValue('L6', 'NETT Gross (RM)');
$sheet->getColumnDimension('L')->setWidth(25);
$sheet->setCellValue('M6', 'Detention (RM)');
$sheet->getColumnDimension('M')->setWidth(25);
// $sheet->setCellValue('N6', 'Overnight (RM)');
// $sheet->getColumnDimension('N')->setWidth(25);
$sheet->setCellValue('N6', '');
$sheet->getColumnDimension('N')->setWidth(0);
$sheet->setCellValue('O6', 'Night Deliveries (RM)');
$sheet->getColumnDimension('O')->setWidth(25);
$sheet->setCellValue('P6', 'Sunday/Rest Day (RM)');
$sheet->getColumnDimension('P')->setWidth(25);
$sheet->setCellValue('Q6', 'Public Holiday (RM)');
$sheet->getColumnDimension('Q')->setWidth(25);
// $sheet->setCellValue('R6', 'Cancellation (RM)');
// $sheet->getColumnDimension('R')->setWidth(20);
$sheet->setCellValue('R6', '');
$sheet->getColumnDimension('R')->setWidth(0);
$sheet->setCellValue('S6', 'Lockup Day (RM)');
$sheet->getColumnDimension('S')->setWidth(20);
$sheet->setCellValue('T6', 'Lockup Night (RM)');
$sheet->getColumnDimension('T')->setWidth(20);

$sheet->setCellValue('U6', 'TOTAL (RM)');
$sheet->getColumnDimension('U')->setWidth(20);
$sheet->setCellValue('V6', 'NETT TOTAL (RM)');
$sheet->getColumnDimension('V')->setWidth(25);
$sheet->setCellValue('W6', 'From Zone');
$sheet->getColumnDimension('W')->setWidth(25);
$sheet->setCellValue('X6', 'To Zone');
$sheet->getColumnDimension('X')->setWidth(25);
$sheet->getStyle('A6:X6')->getAlignment()->setHorizontal('center');
putBorderStyleRow($sheet,'A6:X6',2,'99AFFF');


$sheet->setCellValue('A'.($queryResult+7), 'Grand Total DSF');
putBorderStyleRow($sheet,'A'.($queryResult+7).':T'.($queryResult+7),2,'00FF7F');
$sheet->mergeCells('A'.($queryResult+7).':T'.($queryResult+7));
$sheet->setCellValue('U'.($queryResult+7), '=SUM(U4:U'.($queryResult+6).')');
$sheet->setCellValue('V'.($queryResult+7), '=SUM(V4:V'.($queryResult+6).')');
$sheet->getStyle('U'.($queryResult+7))->getNumberFormat()->setFormatCode('0.00'); 
$sheet->getStyle('U'.($queryResult+7))->getAlignment()->setHorizontal('center');


$sheet->getStyle('V'.($queryResult+7))->getNumberFormat()->setFormatCode('0.00'); 
$sheet->getStyle('V'.($queryResult+7))->getAlignment()->setHorizontal('center');
$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

?>