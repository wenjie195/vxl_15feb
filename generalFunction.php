<?php
class TransportCharge 
{
     var $transportCharge,$dtmBookingDate,$companyName,$faf,$isConsol,$costCenterID_FK;
     function getTransportChargeAll($transportCharge,$dtmBookingDate,$companyName,$faf,$isConsol,$costCenterID_FK) 
     {
         $this->transportCharge = $transportCharge;
         $this->dtmBookingDate = $dtmBookingDate;
         $this->companyName = $companyName;
         $this->faf = $faf;
         $this->isConsol = $isConsol;
         $this->costCenterID_FK = $costCenterID_FK;
     }
     function getTransportCharge()
     {
          return $this->transportCharge;
     }
     function getDtmBookingDate()
     {
          return $this->dtmBookingDate;
     }
     function getCompanyName()
     {
          return $this->companyName;
     }
     function getFaf()
     {
          return $this->faf;
     }
     function getIsConsol()
     {
          return $this->isConsol;
     }
     function getCostCenterID_FK()
     {
          return $this->costCenterID_FK;
     }
}

class TransportChargeByTrucks 
{
     var $transportCharge,$dtmBookingDate,$truckPlateNo,$faf,$truckCapacity,$isConsol;
     function getTransportChargeAll($transportCharge,$dtmBookingDate,$truckPlateNo,$faf,$truckCapacity,$isConsol) 
     {
         $this->transportCharge = $transportCharge;
         $this->dtmBookingDate = $dtmBookingDate;
         $this->truckPlateNo = $truckPlateNo;
         $this->faf = $faf;
         $this->truckCapacity = $truckCapacity;
         $this->isConsol = $isConsol;
     }
     function getTransportCharge()
     {
          return $this->transportCharge;
     }
     function getDtmBookingDate()
     {
          return $this->dtmBookingDate;
     }
     function getTruckPlateNo()
     {
          return $this->truckPlateNo;
     }
     function getFaf()
     {
          return $this->faf;
     }
     function getCap()
     {
          return $this->truckCapacity;
     }
     function getIsConsol()
     {
          return $this->isConsol;
     }
}
// DB SQL Connection
function connDB()
{
    // $conn = mysqli_connect("localhost", "root", "","new_vxl4");
    // $conn = mysqli_connect("localhost", "root", "","new_vxl5");
    $conn = mysqli_connect("localhost", "ichibang_vxluser", "0hnQVhAqwzpb","ichibang_vxl");
    
    // $conn = mysqli_connect("localhost", "root", "","new_vxl");
    // $conn = mysqli_connect("localhost", "root", "","new_vxl2"); //27 JUNE 2018
    // $conn = mysqli_connect("localhost", "root", "","new_vxl3"); //28 JUNE 2018
    return $conn;
}

function checkLogin()
{
    if($_SERVER['REQUEST_METHOD']=='POST')
    {
        $conn = connDB();
        session_start();
        $email = $_POST['field_1'];
        $password = $_POST['field_2'];

        $loginQuery = "SELECT * FROM user WHERE showThis = 1 AND userEmail = '$email' AND userPassword = '$password'";

        //Get all result(data) from querying
        $loginResult = mysqli_query($conn, $loginQuery);
                        
        //Check if there is data or not
        if (mysqli_num_rows($loginResult) > 0) 
        {
            //Getting all specific data inside the database
            while($loginRow = mysqli_fetch_assoc($loginResult)) 
            {
                $_SESSION['userName'] = $loginRow["userName"];
                $_SESSION['userID_PK'] = $loginRow["userID_PK"];
                $_SESSION['userLevel'] = $loginRow["userLevel"];
                $_SESSION['userStartSessionTime'] = time();
            }
            if(isset($_SESSION['userLevel']))
            {
                //Insert login timestamp
                $sessionQuery = "INSERT INTO session (userID_FK,sessionLogin) VALUES (".$_SESSION['userID_PK'].",now())";
                //check whether the sql exists or not
                if(mysqli_query($conn, $sessionQuery))
                {
                    $_SESSION['sessionID_PK'] = mysqli_insert_id($conn);
                    header('Location:indexDashboard.php');
                }
                else
                {
                   echo "ERROR: Could not able to execute $sessionQuery. " . mysqli_error($conn);
                }
            }
        }
        else
        {
            echo "<script>
            var header = 'Notice !!!';
            var text = 'Please enter the correct email OR Password';

            document.getElementById('noticeHeader').innerHTML = header;
            document.getElementById('noticeText').innerHTML = text;
            
            // Get the modal
            var modal = document.getElementById('noticeModal');
        
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName('closeNoticeModal')[0];
        
            modal.style.display = 'block';
            
        
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
            modal.style.display = 'none';
            }
        
            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = 'none';
            }
            }
            </script>";
        }
         
    }
}
function generateSimpleModal()
{
    echo "
        <div id='noticeModal' class='noticeModal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <span class='closeNoticeModal'>&times;</span>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <h4 class='modalHeader' id='noticeHeader'></h5>
                        <p id='noticeText'></p>
                    </div>
                    <div class='col'></div>
                </div>
               
            </div>
        </div>
    ";
}
function generateDeleteModal($fromPage)
{
    echo "
        <div id='noticeModal".$fromPage."' class='noticeModal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <span class='closeNoticeModal".$fromPage."'>&times;</span>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <h4 class='modalHeader' id='noticeHeader".$fromPage."'></h5>
                        <p id='noticeText".$fromPage."'></p>
                    </div>
                    <div class='col'></div>
                </div>
               
            </div>
        </div>
    ";
}
function putNotice($title,$paragraph)
{
    echo "<script>
    var header = '".$title."';
    var text = '".$paragraph."';

    document.getElementById('noticeHeader').innerHTML = header;
    document.getElementById('noticeText').innerHTML = text;
    
    // Get the modal
    var modal = document.getElementById('noticeModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName('closeNoticeModal')[0];

    modal.style.display = 'block';
    

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = 'none';
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
    }
    </script>";
}
function generateConfirmationDeleteModal($fromPage)
{
    echo "
        <div id='myModal".$fromPage."' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-1'>
                        <span class='close".$fromPage."'>&times;</span>
                    </div>
                </div>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <h5 class='index-textAlign index-modalHeader' id='header".$fromPage."'></h5>
                    </div>
                    <div class='col'></div>
                </div>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <p class='index-textAlign' id='text".$fromPage."'></p>
                    </div>
                    <div class='col'></div>
                </div>
                <div class='row' style='text-align:center;'>
                    <div class='col'></div>
                    <div class='col-xl-5'>
                        <button class='btn btn-success btnsize' name='confirm' id='confirm".$fromPage."'>Confirm</button>
                    </div>
                    <div class='col-xl-5'>
                        <button class='btn btn-danger btnsize' name='cancel' id='cancel".$fromPage."'>Cancel</button>
                    </div>
                    <div class='col'></div>
                </div>
            </div>
        </div>
    ";
}
function generateConfirmationModal()
{
    echo "
        <div id='myModal' class='modal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-1'>
                        <span class='close'>&times;</span>
                    </div>
                </div>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <h5 class='index-textAlign index-modalHeader' id='header'></h5>
                    </div>
                    <div class='col'></div>
                </div>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <p class='index-textAlign' id='text'></p>
                    </div>
                    <div class='col'></div>
                </div>
                <div class='row' style='text-align:center;'>
                    <div class='col'></div>
                    <div class='col-xl-5'>
                        <button class='btn btn-success btnsize' name='confirm' id='confirm'>Confirm</button>
                    </div>
                    <div class='col-xl-5'>
                        <button class='btn btn-danger btnsize' name='cancel' id='cancel'>Cancel</button>
                    </div>
                    <div class='col'></div>
                </div>
            </div>
        </div>
    ";
}
function position($level)
{
    $position = "";
    if($level == 1)
    {
        $position .= "Admin";
    }

    if($level == 2)
    {
        $position .= "Coordinator";
    }
    
    if($level == 3)
    {
        $position .= "HR";
    }
    return $position;
}
function noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$totalCounts)
{
    echo " <script> \n";
    echo " var x = document.getElementById('pagination".$fromPage."');\n";
    echo " var y = document.getElementById('filter".$fromPage."');\n";

    for($cnt = 1 ; $cnt <= $no_of_pages ; $cnt++)
    {
        echo "  var option = document.createElement('option'); \n";
        echo "  option.text = '".$cnt."';  \n";
        echo "  option.value = '".$cnt."';  \n";
        echo "  x.add(option);  \n";
    }
    echo " x.selectedIndex = '".($pageNo-1)."';\n";
    echo " y.selectedIndex = '".($filter)."';\n";
    

    echo " document.getElementById('totalpages".$fromPage."').innerHTML ='".$no_of_pages." from ".$totalCounts." results';\n";
    echo " </script> ";
}
function displayPosition($level)
{
    if($level == 1)
    {
        echo "<option value='1' selected>Admin</option>
        <option value='2'>Staff Operational</option>";
    }
    if($level == 2)
    {
        echo "<option value='1' >Admin</option>
        <option value='2' selected>Staff Operational</option>";
    }
}
function displayState($state)
{
    if($state == 'Kuala Lumpur')
    {
        echo "
            <option value='Kuala Lumpur' selected>WP Kuala Lumpur</option>
            <option value='Labuan'>WP Labuan</option>
            <option value='Putrajaya'>WP Putrajaya</option>
            <option value='Perlis'>Perlis</option>
            <option value='Kedah'>Kedah</option>
            <option value='Terengganu'>Terengganu</option>
            <option value='Kelantan'>Kelantan</option>
            <option value='Pulau Pinang'>Pulau Pinang</option>
            <option value='Perak'>Perak</option>
            <option value='Pahang'>Pahang</option>
            <option value='Selangor'>Selangor</option>
            <option value='Negeri Sembilan'>Negeri Sembilan</option>
            <option value='Melaka'>Melaka</option>
            <option value='Johor'>Johor</option>
            <option value='Sabah'>Sabah</option>
            <option value='Sarawak'>Sarawak</option>
        ";
    }
    if($state == 'Labuan')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan' selected>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Putrajaya')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya' selected>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Perlis')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis' selected>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Kedah')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah' selected>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Terengganu')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu' selected>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Kelantan')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan' selected>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Pulau Pinang')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang' selected>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Perak')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak' selected>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Pahang')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang' selected>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Selangor')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor' selected>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Negeri Sembilan')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan' selected>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Melaka')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka' selected>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Johor')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor' selected>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Sabah')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor' >Johor</option>
        <option value='Sabah' selected>Sabah</option>
        <option value='Sarawak'>Sarawak</option>
    ";
    }
    if($state == 'Sarawak')
    {
        echo "
        <option value='Kuala Lumpur' >WP Kuala Lumpur</option>
        <option value='Labuan'>WP Labuan</option>
        <option value='Putrajaya'>WP Putrajaya</option>
        <option value='Perlis'>Perlis</option>
        <option value='Kedah'>Kedah</option>
        <option value='Terengganu'>Terengganu</option>
        <option value='Kelantan'>Kelantan</option>
        <option value='Pulau Pinang'>Pulau Pinang</option>
        <option value='Perak'>Perak</option>
        <option value='Pahang'>Pahang</option>
        <option value='Selangor'>Selangor</option>
        <option value='Negeri Sembilan'>Negeri Sembilan</option>
        <option value='Melaka'>Melaka</option>
        <option value='Johor'>Johor</option>
        <option value='Sabah'>Sabah</option>
        <option value='Sarawak'selected>Sarawak</option>
    ";
    }
}
function displayCapacity($truckCapacity)
{
    if($truckCapacity == '1 Ton')
    {
        echo
        "
            <option value='1 Ton' selected>1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '20 Ft / 10 Ton')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton' selected>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '3 Ton')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton' selected> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '40 Ft / Q6')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6' selected> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '40 Ft / Q6 Air-Ride')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride' selected>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '45 Ft / Q7')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7' selected>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '50 Ft')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft' selected>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == '55 Ft')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft' selected>55 Ft</option>
            <option value='OTHERS'>OTHERS</option>
        ";
    }
    if($truckCapacity == 'OTHERS')
    {
        echo
        "
            <option value='1 Ton' >1 Ton</option>
            <option value='20 Ft / 10 Ton'>20 Ft / 10 Ton</option>
            <option value='3 Ton'> 3 Ton</option>
            <option value='40 Ft / Q6'> 40 Ft / Q6</option>
            <option value='40 Ft / Q6 Air-Ride'>40 Ft / Q6 Air-Ride</option>
            <option value='45 Ft / Q7'>45 Ft / Q7</option>
            <option value='50 Ft'>50 Ft</option>
            <option value='55 Ft'>55 Ft</option>
            <option value='OTHERS' selected>OTHERS</option>
        ";
    }
   
}
function getDatePHP($dateVar) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $date = date("Y-m-d H-i-s", strtotime($dateVar));

    return $date;
}
function getTimePHP($timeVar) 
{
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $time = date("H:i:s", strtotime($timeVar));
    
    return $time;
}
function quantityCheck($varrr)
{
    $isItem = 0;

    if($varrr == true)
    {
        $isItem = 1;
    }
    else 
    {
        $isItem = 0;
    }
    return $isItem;
}
function compareValue($a1,$b1,$a2,$b2,$pickupDate)
{
    if(!isset($a2) && !isset($b2))
    {
        if($a1 && $b1)
        {
            $a = strtotime($a1); 
            $b = strtotime($b1);    
        
            $difference = $a - $b;
            $h = ensure2Digit(floor($difference / 3600));
            $m = ensure2Digit(floor(($difference / 60) % 60));
            $time_taken =  $h.":".$m;
        }
        else
        {
            $time_taken =  "00:00";
        }
    }
    else if(isset($a2) && !isset($b2))
    {
        $splitdateA2 = explode(" ",$a2);
        $splitdateB2 = explode(" ",$pickupDate);

        $newDateA2 = $splitdateA2[0]." ".$a1;
        $newDateB2 = $splitdateB2[0]." ".$b1;

        $a = strtotime($newDateA2); 
        $b = strtotime($newDateB2);   

        $difference = $a - $b;
        $h = ensure2Digit(floor($difference / 3600));
        $m = ensure2Digit(floor(($difference / 60) % 60));
        $time_taken =  $h.":".$m;
    }
    
    else if(!isset($a2) && isset($b2))
    {
        $splitdateA2 = explode(" ",$pickupDate);
        $splitdateB2 = explode(" ",$b2);

        $newDateA2 = $splitdateA2[0]." ".$a1;
        $newDateB2 = $splitdateB2[0]." ".$b1;

        $a = strtotime($newDateA2); 
        $b = strtotime($newDateB2);   

        $difference = $a - $b;
        $h = ensure2Digit(floor($difference / 3600));
        $m = ensure2Digit(floor(($difference / 60) % 60));
        $time_taken =  $h.":".$m;
    }
    else if(isset($a2) && isset($b2))
    {
        $splitdateA2 = explode(" ",$a2);
        $splitdateB2 = explode(" ",$b2);

        $newDateA2 = $splitdateA2[0]." ".$a1;
        $newDateB2 = $splitdateB2[0]." ".$b1;

        $a = strtotime($newDateA2); 
        $b = strtotime($newDateB2);   

        $difference = $a - $b;
        $h = ensure2Digit(floor($difference / 3600));
        $m = ensure2Digit(floor(($difference / 60) % 60));
        $time_taken =  $h.":".$m;
    }
    
    return $time_taken;
}
function ensure2Digit($number)
{
    if($number < 10) 
    {
        $number = '0'.$number;
    }
    return $number;
}
?>
