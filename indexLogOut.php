<?php
    require 'generalFunction.php';
    $conn = connDB();
	//Start the session
	session_start();
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
         //Insert login timestamp
         $sql = "UPDATE session SET sessionLogout = now() WHERE sessionID_PK = ".$_SESSION['sessionID_PK'];
         //check whether the sql exists or not
         if(mysqli_query($conn, $sql))
         {
            // remove all session variables
            session_unset();

            // destroy the session 
            session_destroy(); 

            // Go back to index page 
            // NOTE : MUST PROMPT ERROR
            header('Location:index.php');
         }
         else
         {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
         }
		
	}
	exit;
?>