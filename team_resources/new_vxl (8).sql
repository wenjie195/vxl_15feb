-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2019 at 03:54 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_vxl`
--

-- --------------------------------------------------------

--
-- Table structure for table `adddriverservicefee`
--

CREATE TABLE `adddriverservicefee` (
  `id` int(255) NOT NULL,
  `driverID_FK` int(255) NOT NULL,
  `dtmID_FK` int(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dsfComplete` int(10) NOT NULL DEFAULT '0',
  `dsfGross` double(10,2) NOT NULL,
  `detention` double(10,2) NOT NULL DEFAULT '0.00',
  `isovernight` int(10) NOT NULL,
  `isnightDeliveries` int(10) NOT NULL,
  `issunday` int(10) NOT NULL,
  `ispublic` int(10) NOT NULL,
  `iscancelllation` int(10) NOT NULL,
  `islockupDay` int(10) NOT NULL,
  `islockupNight` int(10) NOT NULL,
  `remarkDsf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `additionalservicefee`
--

CREATE TABLE `additionalservicefee` (
  `id` int(255) NOT NULL,
  `loadTrans` varchar(255) NOT NULL,
  `overnight` double(10,2) NOT NULL DEFAULT '0.00',
  `nightDeliveries` double(10,2) NOT NULL DEFAULT '0.00',
  `sunday` double(10,2) NOT NULL DEFAULT '0.00',
  `public` double(10,2) NOT NULL DEFAULT '0.00',
  `lockupNight` double(10,2) NOT NULL DEFAULT '0.00',
  `lockupDay` double(10,2) NOT NULL DEFAULT '0.00',
  `cancel` double(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `companyID_PK` int(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `companyShortForm` varchar(10) NOT NULL,
  `companyDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `companyDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `costcenter`
--

CREATE TABLE `costcenter` (
  `costCenterID_PK` int(255) NOT NULL,
  `costCenterName` varchar(255) NOT NULL,
  `costCenterDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `costCenterDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costcenter`
--

INSERT INTO `costcenter` (`costCenterID_PK`, `costCenterName`, `costCenterDateUpdated`, `costCenterDateCreated`, `showThis`) VALUES
(15, 'NONE', '2019-05-29 01:50:40', '2019-05-29 01:50:40', 1),
(16, 'AIM', '2019-06-17 01:46:12', '2019-06-17 01:46:12', 1),
(17, 'AIS', '2019-06-17 01:46:22', '2019-06-17 01:46:22', 1),
(18, 'CONSOL', '2019-06-17 01:46:28', '2019-06-17 01:46:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driverID_PK` int(255) NOT NULL,
  `driverDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `driverDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `driverName` varchar(255) NOT NULL,
  `driverNickName` varchar(255) NOT NULL,
  `driverICno` varchar(255) NOT NULL,
  `driverPhoneNo` varchar(15) NOT NULL,
  `insertedByID_FK` int(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dtmlist`
--

CREATE TABLE `dtmlist` (
  `dtmID_PK` int(255) NOT NULL,
  `dtmDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dtmDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dtmDGF` varchar(255) DEFAULT NULL,
  `companyID_FK` int(255) NOT NULL,
  `dtmBookingDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dtmBookingTime` time NOT NULL,
  `dtmPickupDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `dtmPickupTime` time NOT NULL,
  `dtmAdhoc` varchar(50) NOT NULL,
  `dtmOriginPointID_FK` int(255) NOT NULL,
  `dtmOriginZoneID_FK` int(255) NOT NULL,
  `dtmDestinationPointID_FK` int(255) NOT NULL,
  `dtmDestinationZoneID_FK` int(255) NOT NULL,
  `truckID_FK` int(255) NOT NULL,
  `driverID_FK` int(255) NOT NULL,
  `driver2ID_FK` int(255) DEFAULT NULL,
  `dtmUrgentMemo` int(10) NOT NULL,
  `dtmOriginF` time DEFAULT NULL,
  `dtmOriginDockTime` time DEFAULT NULL,
  `dtmOriginCompleteLoadingTime` time DEFAULT NULL,
  `dtmOriginDepartureTimeFromPost` time DEFAULT NULL,
  `dtmDestinationArrivalTimeAtPost` time DEFAULT NULL,
  `dtmDestinationDockTime` time DEFAULT NULL,
  `dtmDestinationCompleteUnloadTime` time DEFAULT NULL,
  `dtmDestinationDepartTimeFromPost` time DEFAULT NULL,
  `loadCap` varchar(50) NOT NULL,
  `dtmQuantityCarton` int(255) DEFAULT NULL,
  `dtmIsQuantity` int(10) NOT NULL,
  `dtmQuantityPalllets` int(255) DEFAULT NULL,
  `dtmIsPallets` int(10) NOT NULL,
  `dtmQuantityCages` int(255) DEFAULT NULL,
  `dtmIsCages` int(10) NOT NULL,
  `dtmTripOnTime` int(10) NOT NULL,
  `dtmDelayReasons` varchar(255) DEFAULT NULL,
  `costCenterID_FK` int(255) DEFAULT NULL,
  `dtmPlannerID_FK` int(255) NOT NULL,
  `dtmRequestBy` varchar(255) NOT NULL,
  `dtmRemarks` varchar(255) DEFAULT NULL,
  `dtmIsPaidDriverServiceFee` int(20) NOT NULL,
  `dtmIsTransportCharge` int(10) DEFAULT NULL,
  `dtmIsFinished` int(10) NOT NULL DEFAULT '0',
  `dsfServiceFee` int(10) NOT NULL DEFAULT '0',
  `lockupf` timestamp NULL DEFAULT NULL,
  `lockupdockTime` timestamp NULL DEFAULT NULL,
  `lockupcompleteLoadingTime` timestamp NULL DEFAULT NULL,
  `lockupdepartureTimeFromPost` timestamp NULL DEFAULT NULL,
  `lockuparrivalTimeAtPost` timestamp NULL DEFAULT NULL,
  `lockupdestinationDockTime` timestamp NULL DEFAULT NULL,
  `lockupcompleteUnloadTime` timestamp NULL DEFAULT NULL,
  `lockupdepartTimeFromPost` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(255) NOT NULL,
  `invoiceNo` int(255) NOT NULL,
  `transportID_FK` int(255) NOT NULL,
  `isSelected` int(10) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pointzone`
--

CREATE TABLE `pointzone` (
  `pointzoneID_PK` int(255) NOT NULL,
  `pointzonePlaceName` varchar(255) NOT NULL,
  `pointzoneDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pointzoneDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `servicefeeratesplace`
--

CREATE TABLE `servicefeeratesplace` (
  `id` int(255) NOT NULL,
  `origin` int(255) NOT NULL,
  `destination` int(255) NOT NULL,
  `loadTransport` varchar(255) NOT NULL,
  `rates` double(10,2) NOT NULL,
  `noOfDrivers` int(20) NOT NULL,
  `showThis` int(10) NOT NULL DEFAULT '1',
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `sessionID_PK` int(255) NOT NULL,
  `sessionDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sessionLogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessionLogout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessionBehaviour` varchar(50) NOT NULL DEFAULT 'NOTHING',
  `userID_FK` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transportcharge`
--

CREATE TABLE `transportcharge` (
  `id` int(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `operatingHour` int(10) NOT NULL,
  `transportcharge` double(20,10) NOT NULL,
  `faf` double(20,10) NOT NULL,
  `truckID_FK` int(255) NOT NULL,
  `companyID_FK` int(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dtmID_FK` int(255) NOT NULL,
  `isInvoiceNoAdded` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transportrate`
--

CREATE TABLE `transportrate` (
  `id` int(255) NOT NULL,
  `companyID_FK` int(255) NOT NULL,
  `loadTransport` varchar(255) NOT NULL,
  `rates` double(10,2) NOT NULL,
  `consoleRate` double(10,2) NOT NULL DEFAULT '0.00',
  `showThis` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trucks`
--

CREATE TABLE `trucks` (
  `truckID_PK` int(255) NOT NULL,
  `truckPlateNo` varchar(255) NOT NULL,
  `truckCapacity` varchar(255) NOT NULL,
  `truckModel` varchar(255) NOT NULL,
  `truckMade` varchar(255) NOT NULL,
  `truckCustomBond` varchar(255) NOT NULL,
  `truckDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `truckDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `truckCustomExpired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID_PK` int(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userNickName` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userLevel` int(10) NOT NULL,
  `userTele` varchar(10) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userInsertedBy` int(255) NOT NULL,
  `userAddress` varchar(255) NOT NULL,
  `userState` varchar(20) NOT NULL,
  `userIC` varchar(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID_PK`, `userName`, `userNickName`, `userPassword`, `userLevel`, `userTele`, `userEmail`, `userDateUpdated`, `userDateCreated`, `userInsertedBy`, `userAddress`, `userState`, `userIC`, `showThis`) VALUES
(1, 'Muhammad Hafeezul Anwar Bin Roslan', 'Hafeezul', '1', 1, '0106664914', 'hafeezdzeko374@gmail.com', '2019-06-13 04:12:11', '2019-02-08 02:22:40', 1, 'Taman Desa Tasik,Sungai Besi', 'WP Kuala Lumpur', '940311105409', 1),
(23, 'Amir', 'Muammad', '123', 2, '0123741070', 'angsa@gmail.com', '2019-05-29 01:48:47', '2019-05-29 01:48:47', 1, 'Kubang Semang,Tasek Gelugor', 'Pulau Pinang', '900310120987', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `zonesID_PK` int(255) NOT NULL,
  `zonesName` varchar(255) NOT NULL,
  `zonesState` varchar(255) NOT NULL,
  `zonesDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zonesDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driverID_FK` (`driverID_FK`),
  ADD KEY `dtmID_FK` (`dtmID_FK`);

--
-- Indexes for table `additionalservicefee`
--
ALTER TABLE `additionalservicefee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`companyID_PK`);

--
-- Indexes for table `costcenter`
--
ALTER TABLE `costcenter`
  ADD PRIMARY KEY (`costCenterID_PK`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driverID_PK`);

--
-- Indexes for table `dtmlist`
--
ALTER TABLE `dtmlist`
  ADD PRIMARY KEY (`dtmID_PK`),
  ADD KEY `companyID_FK` (`companyID_FK`),
  ADD KEY `costCenterID_FK` (`costCenterID_FK`),
  ADD KEY `driverID_FK` (`driverID_FK`),
  ADD KEY `dtmDestinationPointID_FK` (`dtmDestinationPointID_FK`),
  ADD KEY `dtmDestinationZoneID_FK` (`dtmDestinationZoneID_FK`),
  ADD KEY `dtmOriginPointID_FK` (`dtmOriginPointID_FK`),
  ADD KEY `dtmOriginZoneID_FK` (`dtmOriginZoneID_FK`),
  ADD KEY `dtmPlannerID_FK` (`dtmPlannerID_FK`),
  ADD KEY `truckID_FK` (`truckID_FK`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transportID_FK` (`transportID_FK`);

--
-- Indexes for table `pointzone`
--
ALTER TABLE `pointzone`
  ADD PRIMARY KEY (`pointzoneID_PK`);

--
-- Indexes for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `origin` (`origin`),
  ADD KEY `destination` (`destination`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`sessionID_PK`),
  ADD KEY `userID_FK` (`userID_FK`);

--
-- Indexes for table `transportcharge`
--
ALTER TABLE `transportcharge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyID_FK` (`companyID_FK`),
  ADD KEY `truckID_FK` (`truckID_FK`),
  ADD KEY `dtmID_FK` (`dtmID_FK`);

--
-- Indexes for table `transportrate`
--
ALTER TABLE `transportrate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyID_FK` (`companyID_FK`);

--
-- Indexes for table `trucks`
--
ALTER TABLE `trucks`
  ADD PRIMARY KEY (`truckID_PK`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID_PK`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`zonesID_PK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `additionalservicefee`
--
ALTER TABLE `additionalservicefee`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `companyID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `costcenter`
--
ALTER TABLE `costcenter`
  MODIFY `costCenterID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driverID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `dtmlist`
--
ALTER TABLE `dtmlist`
  MODIFY `dtmID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `pointzone`
--
ALTER TABLE `pointzone`
  MODIFY `pointzoneID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `sessionID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT for table `transportcharge`
--
ALTER TABLE `transportcharge`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `transportrate`
--
ALTER TABLE `transportrate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `trucks`
--
ALTER TABLE `trucks`
  MODIFY `truckID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `zonesID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  ADD CONSTRAINT `adddriverservicefee_ibfk_1` FOREIGN KEY (`driverID_FK`) REFERENCES `driver` (`driverID_PK`),
  ADD CONSTRAINT `adddriverservicefee_ibfk_2` FOREIGN KEY (`dtmID_FK`) REFERENCES `dtmlist` (`dtmID_PK`);

--
-- Constraints for table `dtmlist`
--
ALTER TABLE `dtmlist`
  ADD CONSTRAINT `dtmlist_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_2` FOREIGN KEY (`costCenterID_FK`) REFERENCES `costcenter` (`costCenterID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_3` FOREIGN KEY (`driverID_FK`) REFERENCES `driver` (`driverID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_4` FOREIGN KEY (`dtmDestinationPointID_FK`) REFERENCES `pointzone` (`pointzoneID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_5` FOREIGN KEY (`dtmDestinationZoneID_FK`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_6` FOREIGN KEY (`dtmOriginPointID_FK`) REFERENCES `pointzone` (`pointzoneID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_7` FOREIGN KEY (`dtmOriginZoneID_FK`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_8` FOREIGN KEY (`dtmPlannerID_FK`) REFERENCES `user` (`userID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_9` FOREIGN KEY (`truckID_FK`) REFERENCES `trucks` (`truckID_PK`);

--
-- Constraints for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  ADD CONSTRAINT `servicefeeratesplace_ibfk_1` FOREIGN KEY (`origin`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `servicefeeratesplace_ibfk_2` FOREIGN KEY (`destination`) REFERENCES `zones` (`zonesID_PK`);

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`userID_FK`) REFERENCES `user` (`userID_PK`);

--
-- Constraints for table `transportcharge`
--
ALTER TABLE `transportcharge`
  ADD CONSTRAINT `transportcharge_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`),
  ADD CONSTRAINT `transportcharge_ibfk_2` FOREIGN KEY (`truckID_FK`) REFERENCES `trucks` (`truckID_PK`),
  ADD CONSTRAINT `transportcharge_ibfk_3` FOREIGN KEY (`dtmID_FK`) REFERENCES `dtmlist` (`dtmID_PK`);

--
-- Constraints for table `transportrate`
--
ALTER TABLE `transportrate`
  ADD CONSTRAINT `transportrate_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
