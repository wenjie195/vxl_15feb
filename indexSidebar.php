<?php
if(!empty($_SESSION))
{
?>
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
             <?php
              
              if($_SESSION['userLevel'] != 3)
              {
                ?>
              <li class="nav-item">
                <a class="nav-link " href="dtmHome.php">
                  <span data-feather="home"></span>
                  <!-- Dashboard <span class="sr-only">(current)</span> -->
                  DTM
                </a>
              </li>
              <?php
              }
              if($_SESSION['userLevel'] != 2 && $_SESSION['userLevel'] != 3)
              {
                ?>
              <li class="nav-item">
                <a class="nav-link" href="usersHome.php">
                  <span data-feather="file"></span>
                  System Users
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="financeHome.php">
                  <span data-feather="shopping-cart"></span>
                  Finance
                </a>
              </li>
              <?php
              }
              if($_SESSION['userLevel'] != 2)
              {
                ?>
              <li class="nav-item">
                <a class="nav-link" href="HRHome.php">
                  <span data-feather="users"></span>
                  Drivers & Trucks
                </a>
              </li>
              <?php
              }
              if($_SESSION['userLevel'] != 2 && $_SESSION['userLevel'] != 3)
              {
                ?>
              <li class="nav-item">
                <a class="nav-link" href="CDHome.php">
                  <span data-feather="bar-chart-2"></span>
                  Coordinator Data
                </a>
              </li>
                <?php
              }
              ?>
              
              <li class="nav-item">
                <a class="nav-link" href="settingsHome.php">
                  <span data-feather="layers"></span>
                  Settings
                </a>
              </li>
            </ul>
          </div>
        </nav>
<?php
}
?>