<?php

require 'generalFunction.php';

if(isset($_POST['fromPage']))
{    
?>
<div class="container-fluid">
     <div class="row">
          <div class="col-md-1">
               <button class="btn btn-success" name="add" onclick="clickThisSales(<?php echo $_POST['fromPage'];?>);">
                    Show
               </button>
          </div>
          <div class="col-md-3">
               <select name="month" id="month" class="form-control adminAddSetPadding" style="margin-right: 5px;">
                    <option value ="1">January</option></option>
                    <option value ="2">February</option></option>
                    <option value ="3">March</option></option>
                    <option value ="4">April</option></option>
                    <option value ="5">May</option></option>
                    <option value ="6">June</option></option>
                    <option value ="7">July</option></option>
                    <option value ="8">August</option></option>
                    <option value ="9">September</option></option>
                    <option value ="10">October</option></option>
                    <option value ="11">November</option></option>
                    <option value ="12">December</option></option>
               </select>
          </div>
          <div class="col-md-3">
               <select name="year" id="year" class="form-control adminAddSetPadding">
                    <?php for($super = 0;$super < 100;$super++)
                    {
                         $yearToPick = 2019 + $super;
                         if($yearToPick <= idate('Y'))
                         {?>
                              <option value="<?php echo $yearToPick;?>"><?php echo $yearToPick;?></option>
                         <?php
                         }
                         else
                         {
                         ?>
                              <option value="<?php echo $yearToPick;?>" disabled><?php echo $yearToPick;?></option>
                         <?php
                         }
                    ?>
                         
                    <?php 
                    }?>
               </select>
          </div>
          <div class="col-md-5"></div>
          </div>
     </div>
</div>
<div style="overflow-x:auto;margin-top:20px;" id="getSales">
<?php
}
?>