<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['tableType']))
        {   
			$fromPage = $_POST['tableType'];
			$driverName = $_POST['driverName'];
			$driverIC = $_POST['driverIC'];
			$driverNickname = $_POST['driverNickname'];
			$dtmID_PK = $_POST['add'];
            require 'generalFunction.php';
            $conn = connDB();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Add Service Fee</title>
        <?php require 'indexHeader.php';?>
    </head>
	<body>
		<?php require 'indexNavbar.php';?>
			<div class="container-fluid">
				<div class="row">
					<?php require 'indexSidebar.php';?>
					<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
						<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
							<h3>
								<?php 
								if($fromPage == 9)
								{
									echo "Add Driver Service Fee";
								}
								?>
							</h3>
							<?php   
								generateConfirmationModal();
								generateSimpleModal();
							?>
						</div>
						<div class="row">
							<div class="col-xl-12">
								<?php 
								if($fromPage == 9)
								{
									$sqlgetInfo = "SELECT * FROM (((((((dtmlist
									INNER JOIN company ON dtmlist.companyID_FK = company.companyID_PK)
									INNER JOIN driver ON dtmlist.driverID_FK = driver.driverID_PK)
									INNER JOIN pointzone ON dtmlist.dtmDestinationPointID_FK = pointzone.pointzoneID_PK)
									INNER JOIN trucks ON dtmlist.truckID_FK = trucks.truckID_PK)
									INNER JOIN user ON dtmlist.dtmPlannerID_FK = user.userID_PK)
									INNER JOIN zones ON dtmlist.dtmDestinationZoneID_FK = zones.zonesID_PK)
									INNER JOIN costcenter ON dtmlist.costCenterID_FK = costcenter.costCenterID_PK) 
									WHERE dtmID_PK = ".$dtmID_PK;

									$result = mysqli_query($conn,$sqlgetInfo);
									if (mysqli_num_rows($result) > 0) 
									{
										while($row = mysqli_fetch_array($result))
										{
								?>  
										<!-- <div class="row">
											<div class="col-xl-2"></div>
											<div class="col-xl-2 dsfBorderLeft dsfBorderTop">
												<h5>
													Company Name :
												</h5>
											</div>
											<div class="col-xl-2 dsfBorderTop">
												<p>
													
												</p>
											</div>
											<div class="col-xl-4 dsfBorderTop dsfBorderRight"></div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-2 dsfBorderLeft dsfBorderTop">
												<h5>
													Shipment Date :
												</h5>
											</div>
											<div class="col-xl-2 dsfBorderTop">
												<p>
													<?php 
														 $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
														 echo $pickupDate;
													?>
												</p>
											</div>
											<div class="col-xl-4 dsfBorderTop dsfBorderRight"></div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-2 dsfBorderLeft">
												<h5>
													Driver Name :
												</h5>
											</div>
											<div class="col-xl-2 ">
												<p>
													<?php 
														echo $row['driverName'];
													?>
													<?php 
														if($row['driver2ID_FK'] && $row['driver2ID_FK'] !=0)
														{
															$costCenterDisplay = "SELECT * FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
															$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
															if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
															{
																while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
																{
																	echo "&".$urow1['driverName']."<br>";
																}
															}
														}
													?>
												</p>
											</div>
											<div class="col-xl-2 ">
												<h5>
													Truck Plate No :
												</h5>
											</div>
											<div class="col-xl-2  dsfBorderRight">
												<p>
													<?php 
														echo $row['truckPlateNo'];
													?>
												</p>
											</div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-2 dsfBorderLeft">
												<h5>
													Pickup From :
												</h5>
											</div>
											<div class="col-xl-2">
												<p>
													<?php 
														$costCenterDisplay = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
														$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
														if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
														{
															while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
															{
																echo $urow1['pointzonePlaceName']."<br>";
															}
														}
														$costCenterDisplay1 = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
														$costCenterDisplayQuery1 = mysqli_query($conn,$costCenterDisplay1);
														if (mysqli_num_rows($costCenterDisplayQuery1) > 0) 
														{
															while($urow2 = mysqli_fetch_array($costCenterDisplayQuery1))
															{
																echo $urow2['zonesName'];
															}
														}
													?>
												</p>
											</div>

											<div class="col-xl-2">
												<h5>
													Delivery To :
												</h5>
											</div>
											<div class="col-xl-2 dsfBorderRight">
												<p>
													<?php 
														 echo $row['pointzonePlaceName']."<br>".$row['zonesName'];
													?>
												</p>
											</div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-8 dsfBorderLeft dsfBorderRight"><h5>Quantity</h5></div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-1 dsfBorderLeft"></div>
											<div class="col-xl-1 dsfTextAlignCenter dsfBorderLeft dsfBorderTop "><h5>Cartons</h5></div>
											<div class="col-xl-1 dsfTextAlignCenter dsfBorderLeft dsfBorderTop "><h5>Pallets</h5></div>
											<div class="col-xl-1 dsfTextAlignCenter dsfBorderLeft dsfBorderTop  dsfBorderRight"><h5>Cages</h5></div>
											<div class="col-xl-4 dsfBorderRight"></div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-1 dsfBorderLeft"></div>
											<div class="col-xl-1 dsfTextAlignCenter dsfBorderLeft dsfBorderTop dsfBorderBottom">
												<p>
													<?php 
														echo $row['dtmQuantityCarton'];
													?>
												</p>
											</div>
											<div class="col-xl-1 dsfTextAlignCenter dsfBorderLeft dsfBorderTop dsfBorderBottom">
												<p>
													<?php 
														echo $row['dtmQuantityPalllets'];
													?>
												</p>
											</div>
											<div class="col-xl-1 dsfTextAlignCenter dsfBorderLeft dsfBorderTop dsfBorderBottom dsfBorderRight">
												<p>
													<?php 
														echo $row['dtmQuantityCages'];
													?>
												</p>
											</div>
											<div class="col-xl-4 dsfBorderRight"></div>
											<div class="col-xl-2"></div>

											<div class="col-xl-2"></div>
											<div class="col-xl-8 dsfBorderLeft dsfBorderBottom dsfBorderRight dsfStatement"></div>
											<div class="col-xl-2"></div>
										</div>
							 -->
							<div class="row">
								<div class="col-xl-12">
									<div class="card">
										<div class="card-header">
											<h5 class="dsfCompanyName">
												<?php 
													echo $row['truckCapacity'];
												?>
											</h5>
										</div>
										<div class="card-body">
											<div class="dsfAlignCenter">
												
													<div class="row ">
														<div class="col-xl-6 dsfCompanyMargin">
															<p class="card-text">
																Driver 1 : 
																<?php 
																		echo $row['driverName'];
																?>
															</p>
														</div> 
														<div class="col-xl-6 dsfCompanyMargin">
															<p class="card-text">
																<?php 
																if($row['driver2ID_FK'] && $row['driver2ID_FK'] !=0 && $row['driver2ID_FK']!="")
																{
																	echo "Driver 2 : ";
																	$costCenterDisplay = "SELECT * FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
																	$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
																	if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
																	{
																		while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
																		{
																			echo $urow1['driverName']."<br>";
																		}
																	}
																}
																?>
															</p>

														</div>
													</div>
													<div class="row ">
														<div class="col-xl-6" style="margin-bottom:20px;">
															<p class="card-text">
																 Agent : 
																<?php 
																		echo $row['companyName'];
																?>
															</p>
														</div> 
														<div class="col-xl-6 " style="margin-bottom:20px;">
															<p class="card-text">
																Shipment Date : 
																<?php 
																		$pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
																		echo $pickupDate;
																?>
															</p>
														</div>
													</div>
													<div class="row">
														<div class="col-xl-6">
															<p class="card-text">
																Pickup From : 
																<?php 
																	$costCenterDisplay = "SELECT * FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
																	$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
																	if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
																	{
																		while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
																		{
																			echo $urow1['pointzonePlaceName'].",";
																		}
																	}
																	$costCenterDisplay1 = "SELECT * FROM zones WHERE zonesID_PK = ".$row['dtmOriginZoneID_FK'];
																	$costCenterDisplayQuery1 = mysqli_query($conn,$costCenterDisplay1);
																	if (mysqli_num_rows($costCenterDisplayQuery1) > 0) 
																	{
																		while($urow2 = mysqli_fetch_array($costCenterDisplayQuery1))
																		{
																			echo $urow2['zonesName'];
																		}
																	}
																?>
															</p>
														</div>
														<div class="col-xl-6">
															<p class="card-text">
																Pickup To : 
																<?php 
																		 echo $row['pointzonePlaceName'].",".$row['zonesName'];
																?>
															</p>
														</div>
													</div>
													<div class="row mt-5">

														<div class="col"></div>
														<div class="col-xl-2 dsfTextAlignCenter"><h5>Cartons</h5></div>
														<div class="col-xl-2 dsfTextAlignCenter"><h5>Pallets</h5></div>
														<div class="col-xl-2 dsfTextAlignCenter"><h5>Cages</h5></div>
														<div class="col"></div>
													</div>
													<div class="row">
														<div class="col"></div>
														<div class="col-xl-2 dsfTextAlignCenter dsfBorderLeft dsfBorderTop dsfBorderBottom">
															<p>
																<?php 
																	echo $row['dtmQuantityCarton'];
																?>
															</p>
														</div>
														<div class="col-xl-2 dsfTextAlignCenter dsfBorderLeft dsfBorderTop dsfBorderBottom">
															<p>
																<?php 
																	echo $row['dtmQuantityPalllets'];
																?>
															</p>
														</div>
														<div class="col-xl-2 dsfTextAlignCenter dsfBorderLeft dsfBorderTop dsfBorderBottom dsfBorderRight">
															<p>
																<?php 
																	echo $row['dtmQuantityCages'];
																?>
															</p>
														</div>
														<div class="col"></div>
													</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row dsfField">
								<div class="col-xl-1"></div>
								<div class="form-group col-xl-5">
									<label for="input-ssip-doNo">DO Number</label>
									<input type="text" class="form-control adminAddSetPadding" id="input-dsf-doNo">
									<input type="hidden" id="input-dsf-dtmID_FK" value="<?php echo $dtmID_PK;?>">
								</div>
								<div class="form-group col-xl-5">
									<?php if($row['truckCapacity'] == "45 Ft / Q7")
									{
										?>
											<label for="input-dsf-load" >Truck Capacity Load</label>
											<select class="form-control adminAddSetPadding" id="input-dsf-load">
												<option disabled selected hidden>-- Pick one capacity --</option>
												<option value="Q6">Q6</option>
												<option value="Q7">Q7</option>
											</select>
										<?php
									}
									?>
									<!-- <label for="input-ssip-doNo">DO Number</label>
									<input type="text" class="form-control adminAddSetPadding" id="input-dsf-doNo"> -->
								</div>
								<div class="col-xl-1"></div>
							</div>
							<div class="row">
								<div class="col-xl-1"></div>
								<div class="form-group col-xl-5">
									<input type="checkbox" name="secDriver" id="secDriver" onchange="showsecDriver(this.value);" value="1">
									<label for="secDriver" id="secDriverLabel">Detention Charges ?</label>
									<div id="showSecond" style="display:none;">
										<label for="input-ssip-dc">Detention Charges (Hour)</label>
										<input type="number" class="form-control adminAddSetPadding" id="input-dsf-dc" min="0">
									</div>
								</div>
								<div class="col-xl-6"></div>
								<div class="col-xl-1"></div>
								<div class="form-group col-xl-5">
									<label for="input-dsf-night" >Night Delivery ?</label>
									<select class="form-control adminAddSetPadding" id="input-dsf-night">
										<option value="no">No</option>
										<option value="yes">Yes</option>
									</select> 
								</div>
								<div class="form-group col-xl-5">
									<label for="input-dsf-public" >Public Holiday ?</label>
									<select class="form-control adminAddSetPadding" id="input-dsf-public">
										<option value="no">No</option>
										<option value="yes">Yes</option>
									</select> 
								</div>
								<div class="col-xl-5"></div>
							</div>
							<div class="row">
								<div class="col-xl-3"></div>
								<div class="col-xl-6 adminAddUserButton">
									<button class="btn formButtonPrimary indexSubmitButton dtmMarginSubmitButton" onclick="payToThisDriver('<?php echo $driverNickname;?>',<?php echo $fromPage;?>,1);">Pay Service Fee</button>
								</div>
								<div class="col-xl-3"></div>
							</div>
								<?php 
										}
									}
								}
								?>  
							</div>
						</div>
					</main>
            	</div>
			</div>
		<?php require 'indexFooter.php';?>
		<script>
		 function showsecDriver()
		{
			if($('#secDriver').prop('checked'))
			{
				$('#showSecond').css('display','block');
				$('#secDriverLabel').html("Uncheck to remove detention charges");
			}
			else
			{
				$('#secDriverLabel').html("Detention Charges ?");
				$('#showSecond').css('display','none');
			}
		}
		</script>
	</body>     
</html>

<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>