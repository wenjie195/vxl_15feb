<!-- JQuery Library --> 
<script src="https://code.jquery.com/jquery-3.3.1.min.js"integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<!-- Firebase Library (Required) --> 
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase.js"></script>

<!-- Firebase Library (Optional) --> 
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-functions.js"></script>

<!-- Initialize Firebase -->
<script>
    var config = {
    apiKey: "AIzaSyBlvP-giwqhXMabLCUnvkdZFofgEILEfoM",
    authDomain: "vxl-system.firebaseapp.com",
    databaseURL: "https://vxl-system.firebaseio.com",
    projectId: "vxl-system",
    storageBucket: "vxl-system.appspot.com",
    messagingSenderId: "883448832739"
    };
    firebase.initializeApp(config);
</script>
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>
<!-- Others --> 
<script src="js/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<script src="js/mdtimepicker.js"></script>

<script type='text/javascript'src='js/timepicki.js'></script>
<!-- Main Script --> 
<script type="text/javascript" src="js/main.js?version=1.0.0.8"></script>

