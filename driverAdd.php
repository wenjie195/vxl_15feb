<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['add']))
        {   
            $add = $_POST['add'];
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>HR Add Driver</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                            <?php 
                            if($add == 6)
                            {
                                echo "HR Add Drivers";
                            }
                            ?>
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 
                            if($add == 6)
                            {
                                ?>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_1" >Driver Name</label>
                                        <input  type="text" class="form-control adminAddFormControl" id="field_1" >
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_2" >Driver Nickname</label>
                                        <input  type="text" class="form-control adminAddFormControl"  id="field_2" >
                                    
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-1"></div>
                                <div class="form-group col-xl-5">
                                        <label for="field_3" >Driver IC No</label>
                                        <input  type="text" class="form-control adminAddFormControl"  id="field_3">
                                
                                </div>
                                <div class="form-group col-xl-5">
                                        <label for="field_4" >Driver Phone No</label>
                                        <input  type="text" class="form-control adminAddFormControl" id="field_4">
                               
                                </div>
                                <div class="col-xl-1"></div>
                                <div class="col-xl-3"></div>
                                <div class="col-xl-6 adminAddUserButton">
                                    <button class="btn formButtonPrimary indexSubmitButton" onclick="addHRData(<?php echo $add;?>,1);">Create New Driver</button>
                                </div>
                                <div class="col-xl-3"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>