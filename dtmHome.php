<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>DTM Home</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php 
            require 'indexNavbar.php';
            generateSimpleModal();
         ?>
        
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>DTM Dashboard</h3>
                    </div>
                    <div class="row">
                        <div class="col-xl-12" id="showDTM"></div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
            $(document).ready(function()
            {
                ajaxDTM();
            });
        </script>
    </body>
</html>
<?php
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>