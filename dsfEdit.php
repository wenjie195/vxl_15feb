<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
        if(isset($_POST['edit']))
        {   
            echo $add = $_POST['edit'];// --> dtmID_FK
            require 'generalFunction.php';

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Edit Service Fee</title>
        <?php require 'indexHeader.php';?>
    </head>     
    <body>
        <?php require 'indexNavbar.php';?>
        <div class="container-fluid">
            <div class="row">
                <?php require 'indexSidebar.php';?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 ">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h3>
                              Edit Service Fee
                        </h3>
                    </div>
                    <?php   
                        generateConfirmationModal();
                        generateSimpleModal();
                    ?>
                    <?php
                         $conn = connDB(); 
                         $costCenterDisplay = "SELECT * FROM adddriverservicefee WHERE id = ".$add;
                         $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                         if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                         {    
                              while($row = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                   $dsfGross = $row['dsfGross'];
                                   $detention = $row['detention'];

                                   $dsfOvernight = $row['dsfOvernight'];
                                   $dsfNightDelivery = $row['dsfNightDelivery'];
                                   $dsfSunday = $row['dsfSunday'];
                                   $dsfPublic = $row['dsfPublic'];
                                   $dsfCancellation = $row['dsfCancellation'];
                                   $dsfLockupDay = $row['dsfLockupDay'];
                                   $dsfLockupNight = $row['dsfLockupNight'];

                                   $remarkDsf = $row['remarkDsf'];
                                   $isovernight = $row['isovernight'];
                                   $isnightDeliveries = $row['isnightDeliveries'];
                                   $issunday = $row['issunday'];
                                   $ispublic = $row['ispublic'];
                                   $islockupNight = $row['islockupNight'];
                                   $islockupDay = $row['islockupDay'];
                                   $iscancelllation = $row['iscancelllation'];

                                   $total_value = $dsfGross;
                                   if($isovernight == 1)
                                   {
                                        $total_value+=$dsfOvernight;
                                   }
                                   if($isnightDeliveries == 1)
                                   {
                                        $total_value+=$dsfNightDelivery;
                                   }
                                   if($issunday == 1)
                                   {
                                        $total_value+=$dsfSunday;
                                   }
                                   if($ispublic == 1)
                                   {
                                        $total_value+=$dsfPublic;
                                   }
                                   if($islockupNight == 1)
                                   {
                                        $total_value+=$dsfLockupNight;
                                   }
                                   if($islockupDay == 1)
                                   {
                                        $total_value+=$dsfLockupDay;
                                   }
                                   if($iscancelllation == 1)
                                   {
                                        $total_value+=$dsfCancellation;
                                   }

                                 

                                   $dtmID_FK = $row['dtmID_FK'];

                                   $costCenterDisplay1 = "SELECT * FROM dtmlist WHERE dtmID_PK = ".$row['dtmID_FK'];
                                   $costCenterDisplayQuery1 = mysqli_query($conn,$costCenterDisplay1);
                                   if (mysqli_num_rows($costCenterDisplayQuery1) > 0) 
                                   {    
                                        while($row2 = mysqli_fetch_array($costCenterDisplayQuery1))
                                        {
                                   ?>
                                   <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                <th >Pickup Date</th>
                                                <th >Pickup Time</th>
                                                <th >From</th>
                                                <th >To</th>
                                                <th >Agent</th>
                                                <th >Truck No</th>
                                                <th >Load Capacity</th>
                                                <th >Consol Status</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  <td>
                                                       <?php 
                                                            $pickupDate = date("d M Y",strtotime($row2['dtmPickupDate']));
                                                            echo $pickupDate;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                            $pickupTime = date("G:i",strtotime($row2['dtmPickupTime']));
                                                            echo $pickupTime;
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row2['dtmOriginPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row2['dtmDestinationPointID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['pointzonePlaceName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php
                                                       $companyID_FK =  $row2['companyID_FK']; 
                                                       $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row2['companyID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['companyName'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php
                                                       $truckID_FK =  $row2['truckID_FK'];
                                                       $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$row2['truckID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo $urow1['truckPlateNo'];
                                                            }
                                                       }
                                                       ?>
                                                  </td>
                                                  <td>
                                                       <?php 
                                                       echo $loadCap = $row2['loadCap'];
                                                       ?>
                                                  </td>
                                                  <td> 
                                                       <?php  
                                                       $isConsol = $row2['isConsol']; 
                                                         if($row2['isConsol'] == 1) 
                                                         { 
                                                             echo "CONSOL"; 
                                                         } 
                                                         else 
                                                         { 
                                                             echo "-"; 
                                                         } 
                                                       ?> 
                                                  </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <table class="table dtmTableNoWrap table-sm table-hovered table-bordered table-striped table-responsive-xl  dtmTableNoWrap"style="text-align:center;">
                                        <thead>
                                             <tr>
                                                <th >Driver Name</th>
                                                <th >From Zones</th>
                                                <th >To Zones</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <tr>
                                                  
                                                  <td class="text-center">
                                                       <?php 
                                                       $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                                                       $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                       if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                       {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                 echo  $urow1['driverName'];
                                                            }
                                                       }
                                                    ?>
                                                    </td>
                                                    <?php
                                                       if($row2['driver2ID_FK'])
                                                       {
                                                            $secDriver = $row2['driver2ID_FK'];
                                                            $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row2['driver2ID_FK'];
                                                            $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                            if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                            {
                                                                while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                                {
                                                                 //    echo $urow1['driverName'];
                                                                    $d2= $row2['driver2ID_FK'];
                                                                }
                                                            }
                                                       }
                                                       ?>
                                                  <td>
                                                    <?php 
                                                    $or = $row2['dtmOriginZoneID_FK'];
                                                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row2['dtmOriginZoneID_FK'];
                                                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                        {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                echo $urow1['zonesName'];
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $dest = $row2['dtmDestinationZoneID_FK'];
                                                        $costCenterDisplay = "SELECT * FROM zones WHERE zonesID_PK = ".$row2['dtmDestinationZoneID_FK'];
                                                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                                                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                                                        {
                                                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                                            {
                                                                echo $urow1['zonesName'];
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                             </tr>
                                        </tbody>
                                   </table>
                                   <?php
                                        }
                                   }
                              }
                         }
                         if(isset($secDriver))
                         {
                             $driverN =2;
                            $costCenterDisplay = "SELECT * FROM servicefeeratesplace WHERE origin = '".$or."' AND destination = '".$dest."' AND loadTransport = '".$loadCap."' AND noOfDrivers = '2' ORDER BY dateCreated DESC LIMIT 1";
                         }
                         else
                         {
                            $driverN =1;
                            $costCenterDisplay = "SELECT * FROM servicefeeratesplace WHERE origin = '".$or."' AND destination = '".$dest."' AND loadTransport = '".$loadCap."' AND noOfDrivers = '1' ORDER BY dateCreated DESC LIMIT 1";
                         }
                       
                        
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {    
                              while($row2 = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                 $rateOfservice = $row2['rates'];
                              }
                        }
                        else
                        {
                            $rateOfservice = 0;
                        }
                    ?>
                    <div class="row adminAddMarginTop">
                        <div class="col-xl-12 row">
                        <?php 

                         // $detention = $row['detention'];
                         // $isovernight = $row['isovernight'];
                         // $isnightDeliveries = $row['isnightDeliveries'];
                         // $issunday = $row['issunday'];
                         // $ispublic = $row['ispublic'];
                         // $islockupNight = $row['islockupNight'];
                         // $islockupDay = $row['islockupDay'];
                         // $iscancelllation = $row['iscancelllation'];
                         $costCenterDisplay = "SELECT * FROM additionalservicefee WHERE loadTrans = '".$loadCap."'";
                         $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                         if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                         {    
                              while($row2 = mysqli_fetch_array($costCenterDisplayQuery))
                              {
                                   $overnight = $row2['overnight'];
                                   $nightDeliveries = $row2['nightDeliveries'];
                                   $sunday = $row2['sunday'];
                                   $public = $row2['public'];
                                   $cancel = $row2['cancel'];
                                   $lockupDay = $row2['lockupDay'];
                                   $lockupNight = $row2['lockupNight'];
                                   $consolVal = $row2['consol']; 
                              }
                         }
                         else
                         {
                                   $overnight = 0;
                                   $nightDeliveries = 0;
                                   $sunday = 0;
                                   $public = 0;
                                   $cancel = 0;
                                   $lockupDay = 0;
                                   $lockupNight = 0;
                                   $consolVal = 0; 
                                   echo $conn->error;
                         }
                         
                         ?>
                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <?php  
                              if($isConsol == 1) 
                              { 
                                   ?>  
                                        <p>Consol Rates DSF:</p>   
                                   <?php  
                              } 
                              else 
                              { 
                                   ?>  
                                        <p>Gross Rates DSF :</p> 
                                   <?php  
                              } 
                              ?> 
                         </div>
                         <div class="col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="dsfGross" min="0" value ="<?php echo sprintf('%0.2f',$dsfGross);?>" onchange="detentionCharges(this.value,document.getElementById('detentioncharge'))">
                         </div>     
                         <div class="col-xl-4"></div>

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Overnight Charges :</p>
                         </div>
                         <div class="col-xl-2">
                              <?php if($isovernight == 1)
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfOvernight" min="0" value="<?php echo $dsfOvernight;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" ><?php
                                   }
                                   else
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfOvernight" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <?php if($isovernight == 1)
                                   {
                                        ?><input type="checkbox" name="overnight" id="overnight" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $overnight;?>',this,document.getElementById('dsfOvernight'));" value="<?php echo $overnight;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="overnight" id="overnight" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $overnight;?>',this,document.getElementById('dsfOvernight'));" value="<?php echo $overnight;?>"><?php
                                   }?>
                                   <span class="checkmark"></span>
                              </label>
                         </div>

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Night Deliveries Charges :</p>
                         </div>
                         <div class="col-xl-2">
                                   <?php if($isnightDeliveries == 1)
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfNightDelivery" min="0" value="<?php echo $dsfNightDelivery;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))"><?php
                                   }
                                   else
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfNightDelivery" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                              <?php if($isnightDeliveries == 1)
                                   {
                                        ?><input type="checkbox" name="nightDeliveries" id="nightDeliveries" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $nightDeliveries;?>',this,document.getElementById('dsfNightDelivery'));" value="<?php echo $nightDeliveries;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="nightDeliveries" id="nightDeliveries" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $nightDeliveries;?>',this,document.getElementById('dsfNightDelivery'));" value="<?php echo $nightDeliveries;?>"><?php
                                   }?>
                                   
                                   <span class="checkmark"></span>
                              </label>
                         </div>  

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Sunday/Rest Day Charges :</p>
                         </div>
                         <div class="col-xl-2">
                                   <?php if($issunday == 1)
                                   {
                                        ?> <input type= "number" class="form-control adminAddSetPadding" id="dsfSunday" min="0" value="<?php echo $dsfSunday;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))"><?php
                                   }
                                   else
                                   {
                                        ?> <input type= "number" class="form-control adminAddSetPadding" id="dsfSunday" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                              <?php if($issunday == 1)
                                   {
                                        ?><input type="checkbox" name="sunday" id="sunday" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $sunday;?>',this,document.getElementById('dsfSunday'));" value="<?php echo $sunday;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="sunday" id="sunday" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $sunday;?>',this,document.getElementById('dsfSunday'));" value="<?php echo $sunday;?>"><?php
                                   }?>
                                   
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Public Day Charges :</p>
                         </div>
                         <div class="col-xl-2">
                         <?php if($ispublic == 1)
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfPublic" min="0" value="<?php echo $dsfPublic;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))"><?php
                                   }
                                   else
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfPublic" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                              <?php if($ispublic == 1)
                                   {
                                        ?><input type="checkbox" name="public" id="public" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $public;?>',this,document.getElementById('dsfPublic'));" value="<?php echo $public;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="public" id="public" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $public;?>',this,document.getElementById('dsfPublic'));" value="<?php echo $public;?>"><?php
                                   }?>
                                   
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Cancellation Charges :</p>
                         </div>
                         <div class="col-xl-2">
                                   <?php if($iscancelllation == 1)
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfCancellation" min="0" value="<?php echo $dsfCancellation;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))"><?php
                                   }
                                   else
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfCancellation" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                              <?php if($iscancelllation == 1)
                                   {
                                        ?><input type="checkbox" name="cancelllation" id="cancelllation" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $cancel;?>',this,document.getElementById('dsfCancellation'));" value="<?php echo $cancel;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="cancelllation" id="cancelllation" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $cancel;?>',this,document.getElementById('dsfCancellation'));" value="<?php echo $cancel;?>"><?php
                                   }?>
                                   
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Lockup <br>(Day) :</p>
                         </div>
                         <div class="col-xl-2">
                                   <?php if($islockupDay == 1)
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfLockupDay" min="0" value="<?php echo $dsfLockupDay;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" ><?php
                                   }
                                   else
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfLockupDay" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                                   <?php if($islockupDay == 1)
                                   {
                                        ?><input type="checkbox" name="lockupDay" id="lockupDay" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $lockupDay;?>',this,document.getElementById('dsfLockupDay'));" value="<?php echo $lockupDay;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="lockupDay" id="lockupDay" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $lockupDay;?>',this,document.getElementById('dsfLockupDay'));" value="<?php echo $lockupDay;?>"><?php
                                   }?>
                                   
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>Lockup <br>(Night):</p>
                         </div>
                         <div class="col-xl-2">
                         <?php if($islockupNight == 1)
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfLockupNight" min="0" value="<?php echo $dsfLockupNight;?>" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" ><?php
                                   }
                                   else
                                   {
                                        ?><input type="number" class="form-control adminAddSetPadding" id="dsfLockupNight" min="0" value="0" onchange="detentionCharges(document.getElementById('dsfGross').value,document.getElementById('detentioncharge'))" disabled><?php
                                   }?>
                         </div>     
                         <div class="col-xl-4">
                              <label class="cb-container cb-padding">
                              <?php if($islockupNight == 1)
                                   {
                                        ?><input type="checkbox" name="lockupNight" id="lockupNight" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $lockupNight;?>',this,document.getElementById('dsfLockupNight'));" value="<?php echo $lockupNight;?>" checked><?php
                                   }
                                   else
                                   {
                                        ?><input type="checkbox" name="lockupNight" id="lockupNight" onchange="dsfOptions(document.getElementById('dsfGross').value,'<?php echo $lockupNight;?>',this,document.getElementById('dsfLockupNight'));" value="<?php echo $lockupNight;?>"><?php
                                   }?>
                                   
                                   <span class="checkmark"></span>
                              </label>
                         </div> 

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2"><label for="detentioncharge" >Detention </label></div>
                         <div class="form-group col-xl-2">
                              <input type="number" class="form-control adminAddSetPadding" id="detentioncharge" min="0" onchange="detentionCharges(document.getElementById('dsfGross').value,this)" value="<?php echo $detention;?>">
                         </div>
                         <div class="col-xl-4"></div>
                         <?php echo $row['detention'];?>
                          

                         <div class="col-xl-4"></div>
                         <div class="col-xl-2">
                              <p>TOTAL:</p>
                         </div>
                         <div class="col-xl-2">
                              <p id="option8" >$ <?php echo sprintf('%0.2f',$total_value);?></p>
                         </div>     
                         <div class="col-xl-4"></div>

                         <div class="col-xl-3"></div>
                         <div class="form-group col-xl-6">
                              <label for="remarkDsf" >Remark</label>
                              <input type="text" class="form-control adminAddSetPadding" id="remarkDsf" value="<?php echo $remarkDsf;?>">
                         </div>
                         <div class="col-xl-3"></div>
                          
                         <div class="col-xl-3"></div>
                         <div class="col-xl-6 text-center mt-3 mb-5">
                              <input type="hidden" id="loadCap" value="<?php echo $loadCap;?>">
                              <input type="hidden" id="grossDsf" value="<?php echo $rateOfservice;?>">
                              <input type="hidden" id="dtmID" value="<?php echo $dtmID_FK;?>">
                              <input type="hidden" id="driver1" value="<?php echo $d1;?>">
                              <input type="hidden" id="driver2" value="<?php  if(isset($d2)){echo $d2;}?>">
                              <input type="hidden" id="driverNo" value="<?php echo $driverN;?>">
                              <button class="btn formButtonPrimary indexSubmitButton" onclick="addSerFee(<?php echo $add;?>,2);" value="<?php echo $add;?>" id="idpk">Edit Service Fee</button>
                         </div>     
                         <div class="col-xl-3"></div> 
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <?php require 'indexFooter.php';?>
        <script>
        $( document ).ready(function() {

          // dsfOptions(document.getElementById('dsfGross').value,'<?php //echo $overnight;?>',this,document.getElementById('dsfOvernight'));" 
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $overnight;?>',document.getElementById('overnight'),document.getElementById('option1'));
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $nightDeliveries;?>',document.getElementById('nightDeliveries'),document.getElementById('option2'));
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $sunday;?>',document.getElementById('sunday'),document.getElementById('option3'));
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $public;?>',document.getElementById('public'),document.getElementById('option4'));
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $cancel;?>',document.getElementById('cancelllation'),document.getElementById('option5'));
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $lockupDay;?>',document.getElementById('lockupDay'),document.getElementById('option6'));
          // dsfOptions('<?php //echo $rateOfservice;?>','<?php //echo $lockupNight;?>',document.getElementById('lockupNight'),document.getElementById('option7'));
          // detentionCharges('<?php //echo $rateOfservice;?>',document.getElementById('detentioncharge'));

          });
        </script>
    </body>
</html>
<?php
        }
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>