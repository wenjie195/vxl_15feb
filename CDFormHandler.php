<?php
//Start the session
session_start();

//Check f the session is empty/exist or not
if(!empty($_SESSION))
{
	if ($_SERVER["REQUEST_METHOD"] == "POST") 
	{
        require 'generalFunction.php';
        $conn = connDB();

        $fromPage = $_POST['fromPage'];
        if(isset($_POST['addOrEdit']))
        {
            $addOrEdit = $_POST['addOrEdit'];
        }

        if($fromPage == 1 && $addOrEdit != 3)
        {
            $pointzonePlaceName = $_POST['pointzonePlaceName'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO pointzone (
                    pointzonePlaceName,pointzoneDateCreated,
                    showThis
                    )
                VALUES (
                '".$pointzonePlaceName."',now(),1)";
    
                if (mysqli_query($conn, $sql)) 
                {
                    echo "Place created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];
                
                $sql = "UPDATE pointzone SET 
                pointzonePlaceName ='".$pointzonePlaceName."'
                WHERE pointzoneID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {

                    echo "Place updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 2 && $addOrEdit != 3)
        {
            $zonesName = $_POST['zonesName'];
            $zonesState = $_POST['zonesState'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO zones (
                    zonesName,zonesState,zonesDateCreated,
                    showThis
                    )
                VALUES (
                '".$zonesName."','".$zonesState."',now(),1)";
    
                if (mysqli_query($conn, $sql)) 
                {
                    echo "Zone created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE zones SET 
                zonesName ='".$zonesName."',
                zonesState ='".$zonesState."'
                WHERE zonesID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Zone updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 3 && $addOrEdit != 3)
        {
            $costCenterName = $_POST['costCenterName'];
            $companyID_FK = $_POST['companyID_FK'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO costcenter (
                    costCenterName,companyID_FK,costCenterDateCreated,
                    showThis
                    )
                VALUES (
                '".$costCenterName."','".$companyID_FK."',now(),1)";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Cost center created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE costcenter SET 
                costCenterName = '".$costCenterName."',
                companyID_FK = '".$companyID_FK."'
                WHERE costCenterID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {

                    echo "Cost center updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 4 && $addOrEdit != 3)
        {
            $companyName = $_POST['companyName'];
            $companyShortForm = $_POST['companyShortForm'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO company (
                    companyName,companyShortForm,showThis
                )
                VALUES (
                '".$companyName."','".$companyShortForm."',1)";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Agent created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE company SET 
                companyName = '".$companyName."',
                companyShortForm = '".$companyShortForm."'
                WHERE companyID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Agent updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 5 && $addOrEdit != 3)
        {
            $userName = $_POST['userName'];
            $userNickName = $_POST['userNickName'];
            $userLevel = $_POST['userPosition'];
            $userIC = $_POST['userIC'];
            $userTele = $_POST['userTele'];
            $userEmail = $_POST['userEmail'];
            $userAddress = $_POST['userAddress'];
            $userState = $_POST['userState'];
            $userInsertedBy = $_SESSION['userID_PK'];
            $userPassword = "123";
            
            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO user (
                    userName,userNickName,userPassword,
                    userLevel,userIC,userTele,
                    userEmail,userAddress,userState,
                    userInsertedBy,userDateCreated,showThis
                    )
                VALUES (
                '".$userName."','".$userNickName."','".$userPassword."',
                '".$userLevel."','".$userIC."','".$userTele."',
                '".$userEmail."','".$userAddress."','".$userState."',
                '".$userInsertedBy."',now(),1)";
                if (mysqli_query($conn, $sql)) 
                {
                    echo "User created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE user SET 
                userName ='".$userName."',
                userNickName ='".$userNickName."',
                userLevel ='".$userLevel."',
                userIC ='".$userIC."',
                userTele ='".$userTele."',
                userEmail ='".$userEmail."',
                userAddress ='".$userAddress."',
                userState ='".$userState."',
                userInsertedBy ='".$userInsertedBy."'
                WHERE userID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "This record updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 6 && $addOrEdit != 3)
        {
            $driverName = $_POST['driverName'];
            $driverNickName = $_POST['driverNickName'];
            $driverICno= $_POST['driverICno'];
            $driverPhoneNo= $_POST['driverPhoneNo'];
            $insertedByID_FK=$_SESSION['userID_PK'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO driver (
                    driverName,driverNickName,driverICno,
                    driverPhoneNo,insertedByID_FK,
                    driverDateCreated,showThis
                    )
                VALUES (
                '".$driverName."','".$driverNickName."','".$driverICno."',
                '".$driverPhoneNo."','".$insertedByID_FK."',now(),1)";
    
                if (mysqli_query($conn, $sql)) 
                {
                    echo "Driver created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE driver SET 
                driverName ='".$driverName."',
                driverNickName ='".$driverNickName."',
                driverICno ='".$driverICno."',
                driverPhoneNo ='".$driverPhoneNo."',
                insertedByID_FK ='".$insertedByID_FK."'
                WHERE driverID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Driver updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 7 && $addOrEdit != 3)
        {
            $truckPlateNo = $_POST['truckPlateNo'];
            $truckCapacity = $_POST['truckCapacity'];
            $truckMade = $_POST['truckMade'];
            $truckModel = $_POST['truckModel'];
            $truckCustomBond = $_POST['truckCustomBond'];
            $truckCustomExpired = $_POST['truckCustomExpired'];

            $truckCustomExpiredFormatted = getDatePHP($truckCustomExpired);

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO trucks (
                    truckPlateNo,truckCapacity,truckModel,
                    truckMade,truckCustomBond,truckDateCreated,
                    truckCustomExpired,showThis
                )
                VALUES (
                '".$truckPlateNo."','".$truckCapacity."','".$truckModel."',
                '".$truckMade."','".$truckCustomBond."',now(),
                '".$truckCustomExpiredFormatted."',1)";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Truck records created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE trucks SET 
                truckPlateNo = '".$truckPlateNo."',
                truckCapacity = '".$truckCapacity."',
                truckModel = '".$truckModel."',
                truckMade = '".$truckMade."',
                truckCustomBond = '".$truckCustomBond."',
                truckCustomExpired = '".$truckCustomExpiredFormatted."',
                showThis = '1'
                WHERE truckID_PK = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Truck records updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }

            }
        }
        if($fromPage == 8 && $addOrEdit != 3)
        {
            $dgfRef = $_POST['dgfRef'];
            $company = $_POST['company'];
            $bookingDate = $_POST['bookingDate'];
            $bookingTime = $_POST['bookingTime'];

            $originPoint = $_POST['originPoint'];
            $destinationPoint = $_POST['destinationPoint'];
            $originZone = $_POST['originZone'];
            $destinationZone = $_POST['destinationZone'];

            $adhoc = $_POST['adhoc'];
            $pickupDate = $_POST['pickupDate'];
            $pickupTime = $_POST['pickupTime'];
            $truckPlateNo = $_POST['truckPlateNo'];
            $driverName = $_POST['driverName'];
            $driverName2 = $_POST['driverName2'];
            $urgentMemo = $_POST['urgentMemo'];

            $quantityTypeCartons = $_POST['quantityTypeCartons'];
            $quantityCartons = $_POST['quantityCartons'];
            $quantityTypePallets = $_POST['quantityTypePallets'];
            $quantityPallets = $_POST['quantityPallets'];
            $quantityTypeCages = $_POST['quantityTypeCages'];
            $quantityCages = $_POST['quantityCages'];

            $tripOnTime = $_POST['tripOnTime'];
            $delayReason = $_POST['delayReason'];
            $dtmCostCenter = $_POST['dtmCostCenter'];
            $dtmPlanner = $_SESSION['userID_PK'];
            $dtmRequestBy = $_POST['dtmRequestBy'];
            $dtmRemark = $_POST['dtmRemark'];
            $loadCap = $_POST['loadCap'];
            $isConsol = $_POST['isConsol'];

            $bookingDateFormatted = getDatePHP($bookingDate);
            if(isset($pickupDate) && $pickupDate != "")
            {
                $pickupDateFormatted = getDatePHP($pickupDate);
            }

            $bookingTimeFormatted = getTimePHP($bookingTime) ;
            $pickupTimeFormatted = getTimePHP($pickupTime) ;

            if($tripOnTime == "yes")
            {
                $trip = 1;
            }
            else
            {
                $trip = 0;
            }

            if($urgentMemo == "yes")
            {
                $memo = 1;
            }
            else
            {
                $memo = 0;
            }

            if($addOrEdit == 1)
            {
                echo $driverName2;
                $sql = "INSERT INTO dtmlist (
                    dtmDateCreated,dtmDGF,companyID_FK,
                    dtmBookingDate,dtmBookingTime,dtmPickupDate,
                    dtmPickupTime,dtmAdhoc,dtmOriginPointID_FK,
                    dtmOriginZoneID_FK,dtmDestinationPointID_FK,dtmDestinationZoneID_FK,
                    truckID_FK,driverID_FK,dtmUrgentMemo,loadCap,
                    dtmQuantityCarton,
                    dtmIsQuantity,dtmQuantityPalllets,dtmIsPallets,
                    dtmQuantityCages,dtmIsCages,dtmTripOnTime,
                    dtmDelayReasons,costCenterID_FK,dtmPlannerID_FK,
                    dtmRequestBy,dtmRemarks,dtmIsPaidDriverServiceFee,isConsol";
                    if($driverName2 != null)
                    {
                        $sql.=",driver2ID_FK";
                    }
                $sql.=")
                VALUES (
                    now(),'".$dgfRef."','".$company."',
                    '".$bookingDateFormatted."','".$bookingTimeFormatted."','".$pickupDateFormatted."',
                    '".$pickupTimeFormatted."','".$adhoc."','".$originPoint."',
                    '".$originZone."','".$destinationPoint."','".$destinationZone."',
                    '".$truckPlateNo."','".$driverName."','".$memo."','".$loadCap."',
                    '".$quantityCartons."','".$quantityTypeCartons."','".$quantityPallets."',
                    '".$quantityTypePallets."',
                    '".$quantityCages."','".$quantityTypeCages."','".$trip."',
                    '".$delayReason."','".$dtmCostCenter."','".$dtmPlanner."',
                    '".$dtmRequestBy."','".$dtmRemark."',0,'".$isConsol."'";
                    if($driverName2 != null)
                    {
                        $sql.=",'".$driverName2."'";
                    }
                    $sql.=" )";
    
                if (mysqli_query($conn, $sql)) 
                {
                    echo "DTM created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $f = $_POST['f'];
                $originDockTime = $_POST['originDockTime'];
                $originCompleteLoadingTime = $_POST['originCompleteLoadingTime'];
                $originDepartureTimeFromPost = $_POST['originDepartureTimeFromPost'];

                $destArrivalTimeAtPost = $_POST['destArrivalTimeAtPost'];
                $destDockTime = $_POST['destDockTime'];
                $destCompleteUnloadTime = $_POST['destCompleteUnloadTime'];
                $destDepartTimeFromPost = $_POST['destDepartTimeFromPost'];
                $id_PK = $_POST['id'];

                $fFormatted = getTimePHP($f) ;
                $originDockTimeFormatted = getTimePHP($originDockTime) ;
                $originCompleteLoadingTimeFormatted = getTimePHP($originCompleteLoadingTime) ;
                $originDepartureTimeFromPostFormatted = getTimePHP($originDepartureTimeFromPost) ;

                $destArrivalTimeAtPostFormatted = getTimePHP($destArrivalTimeAtPost) ;
                $destDockTimeFormatted = getTimePHP($destDockTime) ;
                $destCompleteUnloadTimeFormatted = getTimePHP($destCompleteUnloadTime) ;
                $destDepartTimeFromPostFormatted = getTimePHP($destDepartTimeFromPost) ;

                

                $sql = "UPDATE dtmlist SET 
                dtmDGF = '".$dgfRef."',
                companyID_FK  = '".$company."',
                dtmBookingDate  = '".$bookingDateFormatted."',
                dtmBookingTime  =  '".$bookingTimeFormatted."',
                dtmPickupDate = '".$pickupDateFormatted."',
                dtmPickupTime =  '".$pickupTimeFormatted."',
                dtmAdhoc =  '".$adhoc."',
                dtmOriginPointID_FK = '".$originPoint."',
                dtmOriginZoneID_FK =  '".$originZone."',
                dtmDestinationPointID_FK = '".$destinationPoint."',
                dtmDestinationZoneID_FK ='".$destinationZone."',
                truckID_FK = '".$truckPlateNo."',
                driverID_FK = '".$driverName."',
                dtmUrgentMemo = '".$memo."',
                dtmOriginF = '".$fFormatted."',
                dtmOriginDockTime = '".$originDockTimeFormatted."',
                dtmOriginCompleteLoadingTime = '".$originCompleteLoadingTimeFormatted."',
                dtmOriginDepartureTimeFromPost ='".$originDepartureTimeFromPostFormatted."',
                dtmDestinationArrivalTimeAtPost = '".$destArrivalTimeAtPostFormatted."',
                dtmDestinationDockTime = '".$destDockTimeFormatted."',
                dtmDestinationCompleteUnloadTime = '".$destCompleteUnloadTimeFormatted."',
                dtmDestinationDepartTimeFromPost = '".$destDepartTimeFromPostFormatted."',
                loadCap ='".$loadCap."',
                dtmQuantityCarton = '".$quantityCartons."',
                dtmIsQuantity = '".$quantityTypeCartons."',
                dtmQuantityPalllets = '".$quantityPallets."',
                dtmIsPallets = '".$quantityTypePallets."',
                dtmQuantityCages = '".$quantityCages."',
                dtmIsCages = '".$quantityTypeCages."',
                dtmTripOnTime = '".$trip."',
                dtmDelayReasons = '".$delayReason."',
                costCenterID_FK = '".$dtmCostCenter."',
                dtmPlannerID_FK = '".$dtmPlanner."',
                isConsol = '".$isConsol."',
                dtmRequestBy = '".$dtmRequestBy."',
                dtmRemarks = '".$dtmRemark."'";

                if($driverName2 != null)
                {
                    $sql .=", driver2ID_FK = '$driverName2' ";
                }

                if(isset($_POST['lockupf'])){$sql .=", lockupf = '".getDatePHP($_POST['lockupf'])."' ";}
                if(isset($_POST['lockupdockTime'])){$sql .=", lockupdockTime = '".getDatePHP($_POST['lockupdockTime'])."' ";}
                if(isset($_POST['lockupcompleteLoadingTime'])){$sql .=", lockupcompleteLoadingTime = '".getDatePHP($_POST['lockupcompleteLoadingTime'])."' ";}
                if(isset($_POST['lockupdepartureTimeFromPost'])){$sql .=", lockupdepartureTimeFromPost = '".getDatePHP($_POST['lockupdepartureTimeFromPost'])."' ";}
                
                if(isset($_POST['lockuparrivalTimeAtPost'])){$sql .=", lockuparrivalTimeAtPost = '".getDatePHP($_POST['lockuparrivalTimeAtPost'])."' ";}
                if(isset($_POST['lockupdestinationDockTime'])){$sql .=", lockupdestinationDockTime = '".getDatePHP($_POST['lockupdestinationDockTime'])."' ";}
                if(isset($_POST['lockupcompleteUnloadTime'])){$sql .=", lockupcompleteUnloadTime = '".getDatePHP($_POST['lockupcompleteUnloadTime'])."' ";}
                if(isset($_POST['lockupdepartTimeFromPost'])){$sql .=", lockupdepartTimeFromPost = '".getDatePHP($_POST['lockupdepartTimeFromPost'])."' ";}


                $sql .=" WHERE dtmID_PK = ".$id_PK;

                if (mysqli_query($conn, $sql)) 
                {
                    echo "DTM updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
        }
        if($fromPage == 30 && $addOrEdit != 3)
        {
            $companyName  = $_POST['companyName'];
            $loadCapacity = $_POST['loadCapacity'];
            $transportCharges = $_POST['transportCharges'];
            $consoleRate = $_POST['consoleRate'];
            $origin = $_POST['origin'];
            $destination = $_POST['destination'];
            
            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO transportrate (
                    companyID_FK,loadTransport,rates,consoleRate,origin,destination
                )
                VALUES (
                '".$companyName."','".$loadCapacity."','".$transportCharges."','".$consoleRate."','".$origin."','".$destination."')";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Transport rates created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE transportrate SET 
                companyID_FK = '".$companyName."',
                loadTransport = '".$loadCapacity."',
                rates = '".$transportCharges."',
                consoleRate = '".$consoleRate."',
                origin = '".$origin."',
                destination = '".$destination."'
                WHERE id = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Transport rates updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }

            }
        }

        if($fromPage == 31 && $addOrEdit != 3)
        {
            $origin  = $_POST['origin'];
            $destination = $_POST['destination'];
            $rates = $_POST['rates'];
            $loadTransport	 = $_POST['loadTransport'];
            $noOfDrivers= $_POST['noOfDrivers'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO servicefeeratesplace (
                    origin,destination,rates,loadTransport,noOfDrivers
                )
                VALUES (
                '".$origin."','".$destination."','".$rates."','".$loadTransport."','".$noOfDrivers."')";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "service fee created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE transportrate SET 
                companyID_FK = '".$companyName."',
                loadTransport = '".$loadCapacity."',
                rates = '".$transportCharges."'
                WHERE id = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "Transport rates updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }

            }
        }
        if($fromPage == 32 && $addOrEdit != 3)
        {
            $idPK  = $_POST['idPK'];
            $dsfGross = $_POST['dsfGross'];
           
            $driver1 = $_POST['driver1'];
            
            if(isset($_POST['driver2']))
            {
                $driver2 = $_POST['driver2'];
            }

            $loadCap = $_POST['loadCap'];
            $isovernight = $_POST['isovernight'];
            $detention = $_POST['detention'];
            $isnightDeliveries = $_POST['isnightDeliveries'];
            $issunday = $_POST['issunday'];
            $ispublic = $_POST['ispublic'];
            $iscancelllation = $_POST['iscancelllation'];
            $islockupDay = $_POST['islockupDay'];
            $islockupNight = $_POST['islockupNight'];
            $remarkDsf = $_POST['remarkDsf'];

            $dsfOvernight = $_POST['dsfOvernight'];
            $dsfNightDelivery = $_POST['dsfNightDelivery'];
            $dsfSunday = $_POST['dsfSunday'];
            $dsfPublic = $_POST['dsfPublic'];
            $dsfCancellation = $_POST['dsfCancellation'];
            $dsfLockupDay = $_POST['dsfLockupDay'];
            $dsfLockupNight = $_POST['dsfLockupNight'];

            $driverNo = $_POST['driverNo'];
            $dtmID = $_POST['dtmID'];

            // $total_service = 0;
            // $total_service += $dsfGross  ;

            // if($night == "yes")
            // {
            //     $total_service +=10;
            // }
            // if($detention)
            // {
            //     $total_service += $detention;
            // }
            // if($sunday == "yes")
            // {
            //     $total_service +=30;
            // }
            // if($public == "yes")
            // {
            //     $total_service +=45;
            // }

            // echo $total_service;


            // CONTINUE HERE
            if($addOrEdit == 1)
            {
                if($driverNo == 1)
                {
                    $sql = "INSERT INTO adddriverservicefee (
                        driverID_FK,dtmID_FK,dsfComplete,
                        dsfGross,detention,isovernight,isnightDeliveries,
                        issunday,ispublic,iscancelllation,
                        islockupDay,islockupNight,remarkDsf,
                        dsfOvernight,dsfNightDelivery,dsfSunday,
                        dsfPublic,dsfCancellation,dsfLockupDay,dsfLockupNight
                    )
                    VALUES (
                    '".$driver1."','".$dtmID."',0,
                    '".$dsfGross."','".$detention."','".$isovernight."','".$isnightDeliveries."'
                    ,'".$issunday."','".$ispublic."','".$iscancelllation."'
                    ,'".$islockupDay."','".$islockupNight."','".$remarkDsf."'
                    ,'".$dsfOvernight."','".$dsfNightDelivery."','".$dsfSunday."'
                    ,'".$dsfPublic."','".$dsfCancellation."','".$dsfLockupDay."'
                    ,'".$dsfLockupNight."')";
    
                    if (mysqli_query($conn, $sql)) 
                    {
                        echo "service fee created successfully";
                    } 
                    else 
                    {
                        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                    }
                }
                else
                {
                    $sql = "INSERT INTO adddriverservicefee (
                        driverID_FK,dtmID_FK,dsfComplete,
                        dsfGross,detention,isovernight,isnightDeliveries,
                        issunday,ispublic,iscancelllation,
                        islockupDay,islockupNight,remarkDsf,
                        dsfOvernight,dsfNightDelivery,dsfSunday,
                        dsfPublic,dsfCancellation,dsfLockupDay,dsfLockupNight
                    )
                    VALUES (
                    '".$driver1."','".$dtmID."',0,
                    '".$dsfGross."','".$detention."','".$isovernight."','".$isnightDeliveries."'
                    ,'".$issunday."','".$ispublic."','".$iscancelllation."'
                    ,'".$islockupDay."','".$islockupNight."','".$remarkDsf."'
                    ,'".$dsfOvernight."','".$dsfNightDelivery."','".$dsfSunday."'
                    ,'".$dsfPublic."','".$dsfCancellation."','".$dsfLockupDay."'
                    ,'".$dsfLockupNight."')";
    
                    if (mysqli_query($conn, $sql)) 
                    {
                        echo "service fee 1 created successfully";
                    } 
                    else 
                    {
                        echo "Error: 1 " . $sql . "<br>" . mysqli_error($conn);
                    }

                    $sql2 = "INSERT INTO adddriverservicefee (
                        driverID_FK,dtmID_FK,dsfComplete,
                        dsfGross,detention,isovernight,isnightDeliveries,
                        issunday,ispublic,iscancelllation,
                        islockupDay,islockupNight,remarkDsf,
                        dsfOvernight,dsfNightDelivery,dsfSunday,
                        dsfPublic,dsfCancellation,dsfLockupDay,dsfLockupNight
                    )
                    VALUES (
                    '".$driver2."','".$dtmID."',0,
                    '".$dsfGross."','".$detention."','".$isovernight."','".$isnightDeliveries."'
                    ,'".$issunday."','".$ispublic."','".$iscancelllation."'
                    ,'".$islockupDay."','".$islockupNight."','".$remarkDsf."'
                    ,'".$dsfOvernight."','".$dsfNightDelivery."','".$dsfSunday."'
                    ,'".$dsfPublic."','".$dsfCancellation."','".$dsfLockupDay."'
                    ,'".$dsfLockupNight."')";
    
                    if (mysqli_query($conn, $sql2)) 
                    {
                        echo "service fee 2 created successfully";
                    } 
                    else 
                    {
                        echo "Error: 2 " . $sql2 . "<br>" . mysqli_error($conn);
                    }
                }
               
                $sql = "UPDATE dtmlist SET 
                dsfServiceFee = '1'
                WHERE dtmID_PK = '".$dtmID."'";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "dtmlist updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                // $idPK  = $_POST['idPK'];
                // $dsfGross = $_POST['dsfGross'];
            
                // $driver1 = $_POST['driver1'];
                
                // if(isset($_POST['driver2']))
                // {
                //     $driver2 = $_POST['driver2'];
                // }

                // $loadCap = $_POST['loadCap'];
                // $isovernight = $_POST['isovernight'];
                // $detention = $_POST['detention'];
                // $isnightDeliveries = $_POST['isnightDeliveries'];
                // $issunday = $_POST['issunday'];
                // $ispublic = $_POST['ispublic'];
                // $iscancelllation = $_POST['iscancelllation'];
                // $islockupDay = $_POST['islockupDay'];
                // $islockupNight = $_POST['islockupNight'];
                // $remarkDsf = $_POST['remarkDsf'];

                // $driverNo = $_POST['driverNo'];
                // $dtmID = $_POST['dtmID'];
                
                $sql = "UPDATE adddriverservicefee SET 
                dsfGross = '".$dsfGross."',
                detention = '".$detention."',
                isovernight = '".$isovernight."',
                isnightDeliveries = '".$isnightDeliveries."',
                issunday = '".$issunday."',
                ispublic = '".$ispublic."',
                iscancelllation = '".$iscancelllation."',
                islockupDay = '".$islockupDay."',
                islockupNight = '".$islockupNight."',
                remarkDsf = '".$remarkDsf."',
                dsfOvernight = '".$dsfOvernight."',
                dsfNightDelivery = '".$dsfNightDelivery."',
                dsfSunday = '".$dsfSunday."',
                dsfPublic = '".$dsfPublic."',
                dsfCancellation = '".$dsfCancellation."',
                dsfLockupDay = '".$dsfLockupDay."',
                dsfLockupNight = '".$dsfLockupNight."'
                WHERE id = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "service fee updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }

            }
        }
        if($fromPage == 41 && $addOrEdit != 3)
        {
            $loadTrans  = $_POST['loadTrans'];
            $overnight = $_POST['overnight'];
            $nightDeliveries = $_POST['nightDeliveries'];
            $sunday	 = $_POST['sunday'];
            $public= $_POST['public'];
            $cancel = $_POST['cancel'];
            $lockupDay	 = $_POST['lockupDay'];
            $lockupNight= $_POST['lockupNight'];
            $consol = $_POST['consol'];

            if($addOrEdit == 1)
            {
                $sql = "INSERT INTO additionalservicefee (
                    loadTrans,overnight,nightDeliveries,sunday,public,cancel,lockupDay,lockupNight,consol
                )
                VALUES (
                '".$loadTrans."','".$overnight."','".$nightDeliveries."','".$sunday."','".$public."','".$cancel."','".$lockupDay."','".$lockupNight."','".$consol."')";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "additional service fee created successfully";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
            if($addOrEdit == 2)
            {
                $id_PK = $_POST['id'];

                $sql = "UPDATE additionalservicefee SET 
                loadTrans = '".$loadTrans."',
                overnight = '".$overnight."',
                nightDeliveries = '".$nightDeliveries."',
                sunday = '".$sunday."',
                public = '".$public."',
                cancel = '".$cancel."',
                lockupDay = '".$lockupDay."',
                lockupNight = '".$lockupNight."',
                consol = '".$consol."'
                WHERE id = ".$id_PK."";

                if (mysqli_query($conn, $sql)) 
                {
                    echo "additional service fee updated successfully";
                }
                else 
                {
                    echo "Error: <br>" . mysqli_error($conn);
                }

            }
        }

        if($fromPage == 1 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];

            $sql = "UPDATE pointzone SET 
                showThis = '0'
                WHERE pointzoneID_PK = ".$idpk."";

            if (mysqli_query($conn, $sql)) 
            {

                echo "Points deleted successfully";
            }
            else 
            {
                echo "Error: <br>" . mysqli_error($conn);
            }
        }
        if($fromPage == 2 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];

            $sql = "UPDATE zones SET 
            showThis = '0'
            WHERE zonesID_PK = ".$idpk."";

            if (mysqli_query($conn, $sql)) 
            {

                echo "Zones deleted successfully";
            }
            else 
            {
                echo "Error: <br>" . mysqli_error($conn);
            }
        }
        if($fromPage == 3 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE costcenter SET 
            showThis = '0'
            WHERE costCenterID_PK = ".$idpk."";

            if (mysqli_query($conn, $sql)) 
            {

                echo "Cost center deleted successfully";
            }
            else 
            {
                echo "Error: <br>" . mysqli_error($conn);
            }
        }
        if($fromPage == 4 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE company SET 
            showThis = '0'
            WHERE companyID_PK = ".$idpk."";

            if (mysqli_query($conn, $sql)) 
            {

                echo "Agent deleted successfully";
            }
            else 
            {
                echo "Error: <br>" . mysqli_error($conn);
            }
        }
        if($fromPage == 5 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE user SET showThis = 0 WHERE userID_PK = ".$idpk;

            if (mysqli_query($conn, $sql)) {
                echo "User deleted successfully";
            } else {
                echo "Error deleting record: " . mysqli_error($conn);
            }
        }
        if($fromPage == 6 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE driver SET showThis = 0 WHERE driverID_PK = ".$idpk;

            if (mysqli_query($conn, $sql)) {
                echo "Driver deleted successfully";
            } else {
                echo "Error deleting record: " . mysqli_error($conn);
            }
        }
        if($fromPage == 7 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE trucks SET showThis = 0 WHERE truckID_PK = ".$idpk;

            if (mysqli_query($conn, $sql)) {
                echo "Truck deleted successfully";
            } else {
                echo "Error deleting record: " . mysqli_error($conn);
            }
        }
        if($fromPage == 30 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE transportrate SET showThis = 0 WHERE id = ".$idpk;

            if (mysqli_query($conn, $sql)) {
                echo "Transport Rate deleted successfully";
            } else {
                echo "Error deleting record: " . mysqli_error($conn);
            }
        }
        if($fromPage == 31 && $addOrEdit == 3)
        {
            $idpk = $_POST['idpk'];
            $sql = "UPDATE servicefeeratesplace SET showThis = 0 WHERE id = ".$idpk;

            if (mysqli_query($conn, $sql)) {
                echo "Transport Rate deleted successfully";
            } else {
                echo "Error deleting record: " . mysqli_error($conn);
            }
        }

    }
    else
    {
    }
}