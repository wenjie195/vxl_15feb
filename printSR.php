<?php
ini_set('max_execution_time', 300);
set_time_limit(300);
echo $monthThis = $_POST['monthThis'];
echo $yearThis = $_POST['yearThis'];


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

define('NUMBER_OF_COLUMNS', 37); 
require 'generalFunction.php';
require 'phpexcel/vendor/autoload.php';
ob_end_clean();
$date = strtotime(sprintf('%s-%s-01',$_POST['yearThis'],$_POST['monthThis']));
renderCalenderMonth($date);



// function putBorderStyleRow($sheet,$cellAlphabet,$type = null,$colour = null)
// {
//      if($type && $type == 1)
//      {
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
//      }
//      if($type && $type == 2)
//      {
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           if($colour)
//           {
//                $sheet->getStyle($cellAlphabet)
//                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
//                $sheet->getStyle($cellAlphabet)
//                ->getFill()->getStartColor()->setARGB($colour);
//           }

//      }
//      else
//      {
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//           $sheet->getStyle($cellAlphabet)
//           ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//      }
     
// }    
function putBorderStyle($sheet,$cellAlphabet,$cellNo)
{
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
     $sheet->getStyle($cellAlphabet.$cellNo)
     ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
}
function putBorderStyleRow($sheet,$rowCell,$color)
{
     $styleArray = [
     'borders' => [
         'outline' => [
             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
             'color' => ['argb' => '000000'],
         ],
     ],
     'fill' => [
          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
          'startColor' => [
              'argb' => $color,
          ],
      ],
 ];

 $sheet->getStyle($rowCell)->applyFromArray($styleArray);
}

function renderCalenderMonth($date)
{
$conn = connDB();
$day = date('d', $date);
$month = date('m', $date);
$year = date('Y', $date);
$daysInMonth = cal_days_in_month(0, $month, $year);

$TransportChargeArray = array();

$sqlo = " SELECT transportcharge.transportcharge,transportcharge.faf,dtmlist.dtmPickupDate,trucks.truckPlateNo,trucks.truckCapacity,dtmlist.isConsol FROM ((transportcharge 
INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
INNER JOIN trucks ON transportcharge.truckID_FK = trucks.truckID_PK)
WHERE transportcharge.isInvoiceNoAdded LIKE 1 
AND dtmlist.dtmPickupDate >= '$year-$month-01 00:00:00' 
AND dtmlist.dtmPickupDate <= '$year-$month-$daysInMonth 23:59:59'";
     
$result = mysqli_query($conn,$sqlo);

if (mysqli_num_rows($result) > 0) 
{
     while($row = mysqli_fetch_array($result))
     { 
          $thisTrans = new TransportChargeByTrucks;
          $thisTrans->getTransportChargeAll($row['transportcharge'],date("d",strtotime($row['dtmPickupDate'])),$row['truckPlateNo'],$row['faf'],$row['truckCapacity'],$row['isConsol']);
          array_push($TransportChargeArray,$thisTrans);
     }
}
else
{
     
}



$firstDay = mktime(0,0,0,$month, 1, $year);
$title = strftime('%B', $firstDay);
$dayOfWeek = date('D', $firstDay);
$grandTotal = 0; 
/* Get the name of the week days */
$timestamp = strtotime('next Sunday');
$weekDays = array();

for ($i = 0; $i < NUMBER_OF_COLUMNS; $i++) {
    $weekDays[] = strftime('%a', $timestamp);
    $timestamp = strtotime('+1 day', $timestamp);
}
$blank = date('w', strtotime("{$year}-{$month}-01"));
$blank2 = date('w', strtotime("{$year}-{$month}-01"));



$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->getColumnDimension('A')->setWidth(20);
$sheet->getColumnDimension('B')->setWidth(20);
$sheet->getColumnDimension('AN')->setWidth(20);
$sheet->getColumnDimension('AO')->setWidth(20);

$sheet->setCellValue('A1', 'Trucks Plate No');
$sheet->setCellValue('B1', 'Trucks Capacity');
$sheet->mergeCells('A1:A3');
$sheet->mergeCells('B1:B3');
$sheet->setCellValue('AO2', 'No of trips');
$sheet->mergeCells('AO2:AO3');


putBorderStyleRow($sheet,'A1:A3','00FF7F');

$sheet->setCellValue('C1', $title." ".$year);
$sheet->mergeCells('C1:AO1');
putBorderStyleRow($sheet,'C1:AO1','FFD700');
putBorderStyleRow($sheet,'C2:AM3','FFFFFF');

$sheet->getStyle('C1:AO1')->getAlignment()->setHorizontal('center');
$sheet->getStyle('C2:AO2')->getAlignment()->setHorizontal('center');
$sheet->getStyle('C3:AO3')->getAlignment()->setHorizontal('center');

$spreadsheet->getActiveSheet()->fromArray( $weekDays, NULL, 'C2' );
$sheet->setCellValue('AN2', 'Total');

$counterBlank = 0;
$counterDays = 0;

for($x='B'; $x != 'AN'; $x++)
{
     putBorderStyleRow($sheet,$x.'2:'.$x.'3','FA8072');
     if($counterBlank <= $blank)
     {
          $sheet->setCellValue($x . '3', '');
         
     }
     else
     {
          if($counterDays+1 <=$daysInMonth)
          {
               $sheet->setCellValue($x . '3',$counterDays+1);
               $counterDays++;
          }
     }
     $counterBlank++;
}

$costCenterDisplay = "SELECT truckPlateNo,truckCapacity FROM trucks ORDER BY truckCapacity ASC";
$costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);

$noOfRows = mysqli_num_rows($costCenterDisplayQuery);
$companyStartCounter = 4;
$tripArray = array();
$tripX = array();
$chargesArray = array();  

if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
{
     while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
     {
          $counterBlankField = 0;
          $counterDaysField = 0;
          $sheet->setCellValue('A'.$companyStartCounter , $urow1['truckPlateNo']);
          $sheet->setCellValue('B'.$companyStartCounter , $urow1['truckCapacity']);
          putBorderStyle($sheet,'A',$companyStartCounter);
          putBorderStyle($sheet,'B',$companyStartCounter);
          $total = 0;
          $totalTrip = 0;
          for($x='C'; $x != 'AN'; $x++)
          {
               putBorderStyleRow($sheet,$x.$companyStartCounter,'FFFFFF');
               $sheet->getColumnDimension($x)->setWidth(10);
               $sheet->getStyle('C'. $companyStartCounter.':'.'AN'. $companyStartCounter)->getAlignment()->setHorizontal('center');
               if($counterBlankField < $blank)
               {
                    $sheet->setCellValue($x . $companyStartCounter,'');
               }
               else
               {
                    if($counterDaysField+1 <=$daysInMonth)
                    {
                         $value = 0;
                         $tripNo = 0;

                         for($cnt = 0;$cnt < count($TransportChargeArray);$cnt++)
                         {
                              if($TransportChargeArray[$cnt]->getDtmBookingDate() == $counterDaysField+1 && $TransportChargeArray[$cnt]->getTruckPlateNo() == $urow1['truckPlateNo'])
                              {
                                   $fafPercentageAfter = ($TransportChargeArray[$cnt]->getFaf() / 100) * $TransportChargeArray[$cnt]->getTransportCharge();
                                   $value += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                   $total += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                   if($TransportChargeArray[$cnt]->getIsConsol() != 1)
                                   {
                                        $tripNo++;
                                        $totalTrip++;
                                   }
                                   $grandTotal += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                              }
                         }
                         if($tripNo != 0 )
                         {
                              array_push($tripArray,$tripNo);
                              array_push($tripX,$x);
                              array_push($chargesArray,$value);
                              $sheet->setCellValue($x . $companyStartCounter,$value);
                              $sheet->getStyle($x . $companyStartCounter)->getNumberFormat()->setFormatCode('0.00');  
                              $sheet->getStyle($x . $companyStartCounter)->getAlignment()->setWrapText(true);
                         }
                         else
                         {
                              $sheet->setCellValue($x . $companyStartCounter,'0');
                              $sheet->getStyle($x . $companyStartCounter)->getNumberFormat()->setFormatCode('0.00');  
                         }

                         $sheet->setCellValue('AO'.$companyStartCounter,$totalTrip);
                         $sheet->getStyle('AN'. $companyStartCounter)->getNumberFormat()->setFormatCode('0.00');  
                         $sheet->getStyle('AO'.$companyStartCounter)->getAlignment()->setHorizontal('center');
                         $sheet->setCellValue('AN'.$companyStartCounter,'=SUM(C'.$companyStartCounter.':AM'.($companyStartCounter).')');
                         $counterDaysField++;
                    }
               }
               $counterBlankField++;
          }
          $companyStartCounter++;
     }
}
$sheet->setCellValue('A'. $companyStartCounter,'DAILY CHARGES');
$sheet->setCellValue('A'. ($companyStartCounter+1),'DAILY TRIP TOTAL');
$sheet->setCellValue('AN'. $companyStartCounter,'=SUM(AN4:AN'.($companyStartCounter-1).')');
$sheet->getStyle('AN'. $companyStartCounter)->getNumberFormat()->setFormatCode('0.00');  
$sheet->setCellValue('AO'. ($companyStartCounter+1),'=SUM(AO4:AO'.($companyStartCounter-1).')');
$sheet->getStyle('AN'.$companyStartCounter)->getAlignment()->setHorizontal('center');
$sheet->getStyle('AO'.$companyStartCounter)->getAlignment()->setHorizontal('center');
$sheet->getStyle('AO'.($companyStartCounter+1))->getAlignment()->setHorizontal('center');
$sheet->getStyle('C'.($companyStartCounter+1).':AM'.($companyStartCounter+1))->getAlignment()->setHorizontal('center');
$sheet->mergeCells('A'.$companyStartCounter.':B'.$companyStartCounter);
$sheet->mergeCells('A'.($companyStartCounter+1).':B'.($companyStartCounter+1));
putBorderStyleRow($sheet,'C'.$companyStartCounter,'FFFFFF');
putBorderStyleRow($sheet,'A'.($companyStartCounter+1).':AM'.($companyStartCounter+1),'FFFFFF');
// putBorderStyleRow($sheet,'A1:A3','FFFFFF');
putBorderStyleRow($sheet,'B1:B3','FFFFFF');
putBorderStyleRow($sheet,'A'.$companyStartCounter.':AM'.$companyStartCounter,'FFFFFF');
putBorderStyleRow($sheet,'AN2:AN'.($companyStartCounter-1),'99FFFF');
putBorderStyleRow($sheet,'AO2:AO'.$companyStartCounter,'99AFFF');
putBorderStyleRow($sheet,'AN'.$companyStartCounter,'FFFFFF');
putBorderStyleRow($sheet,'AN'. $companyStartCounter,'FFF8DC');
putBorderStyleRow($sheet,'AO2:AO3','FFA8DC');
putBorderStyleRow($sheet,'AO'. $companyStartCounter,'FFF8DC');
putBorderStyleRow($sheet,'AO'. ($companyStartCounter+1),'FFF8DC');
putBorderStyleRow($sheet,'AO4:AO'.$companyStartCounter,'FFFFFF');
putBorderStyleRow($sheet,'A'. $companyStartCounter,'ADFF2F');
putBorderStyleRow($sheet,'A'.($companyStartCounter+1).':AO'.($companyStartCounter+1),'FAB701');
putBorderStyleRow($sheet,'B1:B3','00FF7F');



for($newCounter = 4;$newCounter < $companyStartCounter;$newCounter++)
{
     putBorderStyleRow($sheet,'AN'.$newCounter,'99FFFF');
     putBorderStyleRow($sheet,'AO'.$newCounter,'99AFFF');
}
//  array_push($tripArray,$tripNo);
// array_push($tripX,$x);
for($c='C'; $c != 'AN'; $c++)
{
     $countDailyTrips = 0;
     $chargesDailyTrips = 0;
     for($cs = 0; $cs < count($tripX);$cs++)
     {
          if($tripX[$cs] == $c)
          {
               $countDailyTrips += $tripArray[$cs];
               $chargesDailyTrips += $chargesArray[$cs];
          }
          $sheet->setCellValue($c. $companyStartCounter,$chargesDailyTrips);
          $sheet->getStyle($c. $companyStartCounter)->getNumberFormat()->setFormatCode('0.00');  
          $sheet->getStyle($c.$companyStartCounter)->getAlignment()->setHorizontal('center');
          $sheet->setCellValue($c. ($companyStartCounter+1),$countDailyTrips);
          putBorderStyleRow($sheet,$c. ($companyStartCounter),'FFFFFF');
          putBorderStyleRow($sheet,$c. ($companyStartCounter+1),'FFFFFF');
     }
}





$filename = 'filename="salesRevenueByTruck'.$month.'_'.$year.'.xlsx"';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; '.$filename);

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
}
?>
