-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2019 at 04:46 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_vxl`
--

-- --------------------------------------------------------

--
-- Table structure for table `adddriverservicefee`
--

CREATE TABLE `adddriverservicefee` (
  `id` int(255) NOT NULL,
  `driverID_FK` int(255) NOT NULL,
  `dtmID_FK` int(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dsfComplete` int(10) NOT NULL DEFAULT '0',
  `dsfGross` double(10,2) NOT NULL,
  `detention` double(10,2) NOT NULL DEFAULT '0.00',
  `isovernight` int(10) NOT NULL,
  `isnightDeliveries` int(10) NOT NULL,
  `issunday` int(10) NOT NULL,
  `ispublic` int(10) NOT NULL,
  `iscancelllation` int(10) NOT NULL,
  `islockupDay` int(10) NOT NULL,
  `islockupNight` int(10) NOT NULL,
  `remarkDsf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `additionalservicefee`
--

CREATE TABLE `additionalservicefee` (
  `id` int(255) NOT NULL,
  `loadTrans` varchar(255) NOT NULL,
  `overnight` double(10,2) NOT NULL DEFAULT '0.00',
  `nightDeliveries` double(10,2) NOT NULL DEFAULT '0.00',
  `sunday` double(10,2) NOT NULL DEFAULT '0.00',
  `public` double(10,2) NOT NULL DEFAULT '0.00',
  `lockupNight` double(10,2) NOT NULL DEFAULT '0.00',
  `lockupDay` double(10,2) NOT NULL DEFAULT '0.00',
  `cancel` double(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `companyID_PK` int(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `companyShortForm` varchar(10) NOT NULL,
  `companyDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `companyDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `costcenter`
--

CREATE TABLE `costcenter` (
  `costCenterID_PK` int(255) NOT NULL,
  `costCenterName` varchar(255) NOT NULL,
  `costCenterDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `costCenterDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costcenter`
--

INSERT INTO `costcenter` (`costCenterID_PK`, `costCenterName`, `costCenterDateUpdated`, `costCenterDateCreated`, `showThis`) VALUES
(15, 'NONE', '2019-05-29 01:50:40', '2019-05-29 01:50:40', 1),
(16, 'AIM', '2019-06-17 01:46:12', '2019-06-17 01:46:12', 1),
(17, 'AIS', '2019-06-17 01:46:22', '2019-06-17 01:46:22', 1),
(18, 'CONSOL', '2019-06-17 01:46:28', '2019-06-17 01:46:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driverID_PK` int(255) NOT NULL,
  `driverDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `driverDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `driverName` varchar(255) NOT NULL,
  `driverNickName` varchar(255) NOT NULL,
  `driverICno` varchar(255) NOT NULL,
  `driverPhoneNo` varchar(15) NOT NULL,
  `insertedByID_FK` int(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driverID_PK`, `driverDateUpdated`, `driverDateCreated`, `driverName`, `driverNickName`, `driverICno`, `driverPhoneNo`, `insertedByID_FK`, `showThis`) VALUES
(1, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhammad Azmi Bin Shapee', 'Azmi', '970126075049', '0164095049', 1, 1),
(2, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Zainuddin Bin Mat Said', 'Zainuddin', '841104075645', '0111249998', 1, 1),
(3, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Adam Bin Omar', 'Adam', '920328075543', '0174460560', 1, 1),
(4, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Ku Risyam Bin ku Bahador', 'Risyam', '840522025945', '0194770338', 1, 1),
(5, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Ahmad Nazri Bin Basir', 'Nazri ', '750530075695', '0194401338', 1, 1),
(6, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Hatta Bin Mohd Isa', 'Hatta', '820617025149', '0112055119', 1, 1),
(7, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhammad Fifi Azri Bin Mazlan', 'Azri', '960428075547', '0194736742', 1, 1),
(8, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Nasir Bin Ramli', 'Nasir', '860403355683', '0135973061', 1, 1),
(9, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhamat Faisal Bin Othman', 'Faisal', '831111025137', '0124431338', 1, 1),
(10, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Norhazuan Bin Zakaria', 'Norhazuan', '761215086101', '0194402338', 1, 1),
(11, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhammad Adib Bin Mohd Arshad', 'Adib', '910622075537', '0189192625', 1, 1),
(12, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhamad Hanafi Bin Hamzi', 'Hanafi', '931209085515', '0172006399', 1, 1),
(13, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Shah Rizal Bin Abdul Sahak', 'Rizal', '870427385341', '0124706339', 1, 1),
(14, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Adli Shafaril Bin Shafaiee', 'Shafaril', '910914086609', '0124719338', 1, 1),
(15, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhamad Zambri Bin Jamil', 'Zambri', '810421075647', '0174285977', 1, 1),
(16, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Ezad Bin Ellias', 'Ezad', '830302075415', '0194090338', 1, 1),
(17, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Noor Bin Bakar', 'Noor', '860331265637', '0194736754', 1, 1),
(18, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Hidayat Bin Abd Latif', 'Hidayat', '750422085191', '0124856338', 1, 1),
(19, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Fazli Bin Jamaludin ', 'Fazli', '770808075293', '0135120239', 1, 1),
(20, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Nazrul Hafiz Bin Abdul Latif', 'Hafiz', '891009026103', '0124838399', 1, 1),
(21, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Azizul Bin Md Nawi', 'Azizul', '961215295049', '0124694883', 1, 1),
(22, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhamad Husni Bin Che Mat Nazan', 'Husni', '940226035645', '0122284339', 1, 1),
(23, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Alhafiz Shukiran Bin Norahaisham', 'Shukiran', '941212075736', '0194736746', 1, 1),
(24, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Muhammad Hamizan Bin Razalli', 'Hamizan', '930226086099', '0194491203', 1, 1),
(25, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Samsuri Bin Hussain', 'Samsuri', '761203075085', '0194760338', 1, 1),
(26, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Aizat Bin Mohd Noor', 'Aizat', '860829355541', '0174408399', 1, 1),
(27, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Ezlee Bin Abdul Manap', 'Ezlee', '810528025623', '0194736764', 1, 1),
(28, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Hafeez Bin Housni', 'Hafeez', '920924075275', '0124716338', 1, 1),
(29, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohamad Efendi Bin Abdul Halim', 'Efendi', '780905025895', '0179791339', 1, 1),
(30, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohd Zahir Bin Hashim', 'Zahir', '831011075583', '0124341338', 1, 1),
(31, '2019-06-19 02:43:04', '2019-06-19 02:43:04', 'Mohamad Ridzuan Bin Wan Min', 'Ridzuan', '900105086511', '0124445338', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtmlist`
--

CREATE TABLE `dtmlist` (
  `dtmID_PK` int(255) NOT NULL,
  `dtmDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dtmDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dtmDGF` varchar(255) DEFAULT NULL,
  `companyID_FK` int(255) NOT NULL,
  `dtmBookingDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dtmBookingTime` time NOT NULL,
  `dtmPickupDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `dtmPickupTime` time NOT NULL,
  `dtmAdhoc` varchar(50) NOT NULL,
  `dtmOriginPointID_FK` int(255) NOT NULL,
  `dtmOriginZoneID_FK` int(255) NOT NULL,
  `dtmDestinationPointID_FK` int(255) NOT NULL,
  `dtmDestinationZoneID_FK` int(255) NOT NULL,
  `truckID_FK` int(255) NOT NULL,
  `driverID_FK` int(255) NOT NULL,
  `driver2ID_FK` int(255) DEFAULT NULL,
  `dtmUrgentMemo` int(10) NOT NULL,
  `dtmOriginF` time DEFAULT NULL,
  `dtmOriginDockTime` time DEFAULT NULL,
  `dtmOriginCompleteLoadingTime` time DEFAULT NULL,
  `dtmOriginDepartureTimeFromPost` time DEFAULT NULL,
  `dtmDestinationArrivalTimeAtPost` time DEFAULT NULL,
  `dtmDestinationDockTime` time DEFAULT NULL,
  `dtmDestinationCompleteUnloadTime` time DEFAULT NULL,
  `dtmDestinationDepartTimeFromPost` time DEFAULT NULL,
  `loadCap` varchar(50) NOT NULL,
  `dtmQuantityCarton` int(255) DEFAULT NULL,
  `dtmIsQuantity` int(10) NOT NULL,
  `dtmQuantityPalllets` int(255) DEFAULT NULL,
  `dtmIsPallets` int(10) NOT NULL,
  `dtmQuantityCages` int(255) DEFAULT NULL,
  `dtmIsCages` int(10) NOT NULL,
  `dtmTripOnTime` int(10) NOT NULL,
  `dtmDelayReasons` varchar(255) DEFAULT NULL,
  `costCenterID_FK` int(255) DEFAULT NULL,
  `dtmPlannerID_FK` int(255) NOT NULL,
  `dtmRequestBy` varchar(255) NOT NULL,
  `dtmRemarks` varchar(255) DEFAULT NULL,
  `dtmIsPaidDriverServiceFee` int(20) NOT NULL,
  `dtmIsTransportCharge` int(10) DEFAULT NULL,
  `dtmIsFinished` int(10) NOT NULL DEFAULT '0',
  `dsfServiceFee` int(10) NOT NULL DEFAULT '0',
  `lockupf` timestamp NULL DEFAULT NULL,
  `lockupdockTime` timestamp NULL DEFAULT NULL,
  `lockupcompleteLoadingTime` timestamp NULL DEFAULT NULL,
  `lockupdepartureTimeFromPost` timestamp NULL DEFAULT NULL,
  `lockuparrivalTimeAtPost` timestamp NULL DEFAULT NULL,
  `lockupdestinationDockTime` timestamp NULL DEFAULT NULL,
  `lockupcompleteUnloadTime` timestamp NULL DEFAULT NULL,
  `lockupdepartTimeFromPost` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(255) NOT NULL,
  `invoiceNo` int(255) NOT NULL,
  `transportID_FK` int(255) NOT NULL,
  `isSelected` int(10) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pointzone`
--

CREATE TABLE `pointzone` (
  `pointzoneID_PK` int(255) NOT NULL,
  `pointzonePlaceName` varchar(255) NOT NULL,
  `pointzoneDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pointzoneDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `servicefeeratesplace`
--

CREATE TABLE `servicefeeratesplace` (
  `id` int(255) NOT NULL,
  `origin` int(255) NOT NULL,
  `destination` int(255) NOT NULL,
  `loadTransport` varchar(255) NOT NULL,
  `rates` double(10,2) NOT NULL,
  `noOfDrivers` int(20) NOT NULL,
  `showThis` int(10) NOT NULL DEFAULT '1',
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `sessionID_PK` int(255) NOT NULL,
  `sessionDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sessionLogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessionLogout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sessionBehaviour` varchar(50) NOT NULL DEFAULT 'NOTHING',
  `userID_FK` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`sessionID_PK`, `sessionDateUpdated`, `sessionLogin`, `sessionLogout`, `sessionBehaviour`, `userID_FK`) VALUES
(1, '2019-06-19 02:42:54', '2019-06-19 02:42:54', '0000-00-00 00:00:00', 'NOTHING', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transportcharge`
--

CREATE TABLE `transportcharge` (
  `id` int(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `operatingHour` int(10) NOT NULL,
  `transportcharge` double(20,10) NOT NULL,
  `faf` double(20,10) NOT NULL,
  `truckID_FK` int(255) NOT NULL,
  `companyID_FK` int(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dtmID_FK` int(255) NOT NULL,
  `isInvoiceNoAdded` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transportrate`
--

CREATE TABLE `transportrate` (
  `id` int(255) NOT NULL,
  `companyID_FK` int(255) NOT NULL,
  `loadTransport` varchar(255) NOT NULL,
  `rates` double(10,2) NOT NULL,
  `consoleRate` double(10,2) NOT NULL DEFAULT '0.00',
  `showThis` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trucks`
--

CREATE TABLE `trucks` (
  `truckID_PK` int(255) NOT NULL,
  `truckPlateNo` varchar(255) NOT NULL,
  `truckCapacity` varchar(255) NOT NULL,
  `truckModel` varchar(255) NOT NULL,
  `truckMade` varchar(255) NOT NULL,
  `truckCustomBond` varchar(255) NOT NULL,
  `truckDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `truckDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `truckCustomExpired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trucks`
--

INSERT INTO `trucks` (`truckID_PK`, `truckPlateNo`, `truckCapacity`, `truckModel`, `truckMade`, `truckCustomBond`, `truckDateUpdated`, `truckDateCreated`, `truckCustomExpired`, `showThis`) VALUES
(1, 'PGU 1348', '3 Ton', 'MTB150DX', 'Hicom', 'KE.PD(83)418/12-752 Klt.7(19)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-06-29 16:00:00', 1),
(2, 'PJR 833', '3 Ton', 'WU410-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(6)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-03-21 16:00:00', 1),
(3, 'PJR 933', '3 Ton', 'WU410-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(3)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-03-21 16:00:00', 1),
(4, 'PKS 2133', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(86)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(5, 'PLF 5633', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(89)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(6, 'PLF 6833', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.8(18)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(7, 'PKS 7933', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(65)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(8, 'PLF 7833', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.8(21)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(9, 'PLF 6933', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(68)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(10, 'PLQ 5633', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(35)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-07-07 16:00:00', 1),
(11, 'PLQ 7533', '3 Ton', 'WU710-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(32)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-07-07 16:00:00', 1),
(12, 'PNK 6533', '3 Ton', 'WU410-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(26)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-05-05 16:00:00', 1),
(13, 'PNK 9633', '3 Ton', 'WU410-R', 'Hino', 'KE.PD(83)418/12-752 Klt.7(30)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-05-05 16:00:00', 1),
(14, 'PNU 5833', '3 Ton', 'WU410-R', 'Hino', 'KE.PD(83)418/12-752 Klt.8(48)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2021-02-25 16:00:00', 1),
(15, 'PNU 6833', '3 Ton', 'WU410-R', 'Hino', 'KE.PD(83)418/12-752 Klt.8(44)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2021-02-25 16:00:00', 1),
(16, 'PNU 8233', '3 Ton', 'WU410-R', 'Hino ', 'KE.PD(83)418/12-572 Klt.8(52)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2021-02-25 16:00:00', 1),
(17, 'PJC 5133', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.7(74)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(18, 'PJC 5233', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.8(24)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(19, 'PJC 6933', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.7(80)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(20, 'PJC 9733', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.8(27)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(21, 'PJD 1333', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.8(30)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(22, 'PJD 1733', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.7(71)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(23, 'PJR 6933', '20 Ft / 10 Ton', 'FR1K', 'Hino', 'KE.PD(83)418/12-752 Klt.7(9)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-03-21 16:00:00', 1),
(24, 'PJR 9633', '20 Ft / 10 Ton', 'FR1K', 'Hino', 'KE.PD(83)418/12-752 Klt.7(22)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-05-30 16:00:00', 1),
(25, 'PJT 5633', '20 Ft / 10 Ton', 'FR1K', 'Hino', 'KE.PD(83)418/12-752 Klt.7(62)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-08-30 16:00:00', 1),
(26, 'PJT 6533', '20 Ft / 10 Ton', 'FR1K', 'Hino', 'KE.PD(83)418/12-752 Klt.7(56)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-08-30 16:00:00', 1),
(27, 'PJT 6733', '20 Ft / 10 Ton', 'FR1K', 'Hino', 'KE.PD(83)418/12-752 Klt.7(59)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-08-30 16:00:00', 1),
(28, 'PKT 8233', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.8(03)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(29, 'PKT 9533', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.8(15)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(30, 'PLF 8733', '20 Ft / 10 Ton', 'FG1JPPB', 'Hino', 'KE.PD(83)418/12-752 Klt.7(95)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(31, 'PLU 2833', '20 Ft / 10 Ton', 'CD45', 'Nissan ', 'KE.PD(83)418/12-752 Klt.7(98)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(32, 'PLU 2933', '20 Ft / 10 Ton', 'CD45', 'Nissan', 'KE.PD(83)418/12-752 Klt.7(83)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(33, 'PLV 5733', '20 Ft / 10 Ton', 'CD53', 'Nissan ', 'KE.PD(83)418/12-752 Klt.7(77)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(34, 'PLF 5133', '20 Ft / 10 Ton', 'FG1JPPB', 'Hino', 'KE.PD(83)418/12-752 Klt.7(92)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(35, 'PMS 9733', '20 Ft / 10 Ton', 'FG1JPPB', 'Hino', 'KE.PD(83)418/12-752 Klt.8(33)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-11-05 16:00:00', 1),
(36, 'PNL 8633', '20 Ft / 10 Ton', 'FG1JPPB', 'Hino', 'KE.PD(83)418/12-752(47)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-06-19 16:00:00', 1),
(37, 'PNL 9633', '20 Ft / 10 Ton', 'FG1JPPB', 'Hino', 'KE.PD(83)418/12-752(43)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-06-21 16:00:00', 1),
(38, 'PNU 9733', '20 Ft / 10 Ton', 'FG1JPPB', 'Hino', 'KE.PD(83)418/12-752 Klt.8(40)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2021-02-25 16:00:00', 1),
(39, 'PJT 5733', '20 Ft / 10 Ton', 'FR1K', 'Hino', 'KE.PD(83)418/12-752 Klt.7(50)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-08-30 16:00:00', 1),
(40, 'BJG 9524 T/BB 9653', '40 Ft / Q6', '1835LS', 'Mercedes', 'KE.PD(83)418/12-752 Klt.8(12)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(41, 'PJU 1833 T/P 8313', '40 Ft / Q6', 'SH4F', 'Hino', 'KE.PD(83)418/12-752 Klt.7(53)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-08-30 16:00:00', 1),
(42, 'PLM 5933 T/BC 823', '40 Ft / Q6', 'CK551', 'Nissan', 'KE.PD(83)418/12-752 Klt.8(09)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(43, 'PMJ 7833 T/PA 500', '40 Ft / Q6', 'SG2PDP', 'Hino', 'KE.PD(83)418/12-752 Klt.8(06)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-09-19 16:00:00', 1),
(44, 'PMN 7933 T/WC 242', '40 Ft / Q6', 'SG2PDP', 'Hino', 'KE.PD(83)418/12-752 Klt.7(16)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-03-21 16:00:00', 1),
(45, 'PMV 7933 T/PA 867', '40 Ft / Q6', 'GXR774VU', 'ISUZU', 'KE.PD(83)418/12-752 Klt.6(20)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2021-02-28 16:00:00', 1),
(46, 'PNC 7533 T/PA 1149', '40 Ft / Q6', 'GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.6(29)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2019-09-05 16:00:00', 1),
(47, 'PNC 7633 T/PA 1157', '40 Ft / Q6', 'GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.6(25)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2019-09-05 16:00:00', 1),
(48, 'PNC 7833 T/PA 1175', '40 Ft / Q6', ' GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.6(37)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2019-09-23 16:00:00', 1),
(49, 'PNC 7933 T/PA1178', '40 Ft / Q6', 'GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.6(33)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2019-09-23 16:00:00', 1),
(50, 'PNF 1333 T/PA 1250', '40 Ft / Q6', 'GKE42T08MD', 'UD Trucks ', 'KE.PD(83)418/12-752 Klt.6(40)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2019-11-28 16:00:00', 1),
(51, 'PNF 2133', '40 Ft / Q6', 'GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.6(44)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-11-28 16:00:00', 1),
(52, 'PNH 2633 T/PA 1420', '45 Ft / Q7', 'GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.7(39)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-05-29 16:00:00', 1),
(53, 'PNH 5133', '45 Ft / Q7', 'GKE42T08MD', 'UD Trucks', 'KE.PD(83)418/12-752 Klt.7(13)', '2019-06-19 02:46:13', '2019-06-19 02:46:13', '2020-03-18 16:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID_PK` int(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userNickName` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userLevel` int(10) NOT NULL,
  `userTele` varchar(10) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userInsertedBy` int(255) NOT NULL,
  `userAddress` varchar(255) NOT NULL,
  `userState` varchar(20) NOT NULL,
  `userIC` varchar(255) NOT NULL,
  `showThis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID_PK`, `userName`, `userNickName`, `userPassword`, `userLevel`, `userTele`, `userEmail`, `userDateUpdated`, `userDateCreated`, `userInsertedBy`, `userAddress`, `userState`, `userIC`, `showThis`) VALUES
(1, 'Muhammad Hafeezul Anwar Bin Roslan', 'Hafeezul', '1', 1, '0106664914', 'hafeezdzeko374@gmail.com', '2019-06-13 04:12:11', '2019-02-08 02:22:40', 1, 'Taman Desa Tasik,Sungai Besi', 'WP Kuala Lumpur', '940311105409', 1),
(23, 'Amir', 'Muammad', '123', 2, '0123741070', 'angsa@gmail.com', '2019-05-29 01:48:47', '2019-05-29 01:48:47', 1, 'Kubang Semang,Tasek Gelugor', 'Pulau Pinang', '900310120987', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `zonesID_PK` int(255) NOT NULL,
  `zonesName` varchar(255) NOT NULL,
  `zonesState` varchar(255) NOT NULL,
  `zonesDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `zonesDateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `showThis` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driverID_FK` (`driverID_FK`),
  ADD KEY `dtmID_FK` (`dtmID_FK`);

--
-- Indexes for table `additionalservicefee`
--
ALTER TABLE `additionalservicefee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`companyID_PK`);

--
-- Indexes for table `costcenter`
--
ALTER TABLE `costcenter`
  ADD PRIMARY KEY (`costCenterID_PK`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driverID_PK`);

--
-- Indexes for table `dtmlist`
--
ALTER TABLE `dtmlist`
  ADD PRIMARY KEY (`dtmID_PK`),
  ADD KEY `companyID_FK` (`companyID_FK`),
  ADD KEY `costCenterID_FK` (`costCenterID_FK`),
  ADD KEY `driverID_FK` (`driverID_FK`),
  ADD KEY `dtmDestinationPointID_FK` (`dtmDestinationPointID_FK`),
  ADD KEY `dtmDestinationZoneID_FK` (`dtmDestinationZoneID_FK`),
  ADD KEY `dtmOriginPointID_FK` (`dtmOriginPointID_FK`),
  ADD KEY `dtmOriginZoneID_FK` (`dtmOriginZoneID_FK`),
  ADD KEY `dtmPlannerID_FK` (`dtmPlannerID_FK`),
  ADD KEY `truckID_FK` (`truckID_FK`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transportID_FK` (`transportID_FK`);

--
-- Indexes for table `pointzone`
--
ALTER TABLE `pointzone`
  ADD PRIMARY KEY (`pointzoneID_PK`);

--
-- Indexes for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `origin` (`origin`),
  ADD KEY `destination` (`destination`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`sessionID_PK`),
  ADD KEY `userID_FK` (`userID_FK`);

--
-- Indexes for table `transportcharge`
--
ALTER TABLE `transportcharge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyID_FK` (`companyID_FK`),
  ADD KEY `truckID_FK` (`truckID_FK`),
  ADD KEY `dtmID_FK` (`dtmID_FK`);

--
-- Indexes for table `transportrate`
--
ALTER TABLE `transportrate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companyID_FK` (`companyID_FK`);

--
-- Indexes for table `trucks`
--
ALTER TABLE `trucks`
  ADD PRIMARY KEY (`truckID_PK`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID_PK`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`zonesID_PK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `additionalservicefee`
--
ALTER TABLE `additionalservicefee`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `companyID_PK` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `costcenter`
--
ALTER TABLE `costcenter`
  MODIFY `costCenterID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driverID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `dtmlist`
--
ALTER TABLE `dtmlist`
  MODIFY `dtmID_PK` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pointzone`
--
ALTER TABLE `pointzone`
  MODIFY `pointzoneID_PK` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `sessionID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transportcharge`
--
ALTER TABLE `transportcharge`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transportrate`
--
ALTER TABLE `transportrate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trucks`
--
ALTER TABLE `trucks`
  MODIFY `truckID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID_PK` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `zonesID_PK` int(255) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adddriverservicefee`
--
ALTER TABLE `adddriverservicefee`
  ADD CONSTRAINT `adddriverservicefee_ibfk_1` FOREIGN KEY (`driverID_FK`) REFERENCES `driver` (`driverID_PK`),
  ADD CONSTRAINT `adddriverservicefee_ibfk_2` FOREIGN KEY (`dtmID_FK`) REFERENCES `dtmlist` (`dtmID_PK`);

--
-- Constraints for table `dtmlist`
--
ALTER TABLE `dtmlist`
  ADD CONSTRAINT `dtmlist_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_2` FOREIGN KEY (`costCenterID_FK`) REFERENCES `costcenter` (`costCenterID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_3` FOREIGN KEY (`driverID_FK`) REFERENCES `driver` (`driverID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_4` FOREIGN KEY (`dtmDestinationPointID_FK`) REFERENCES `pointzone` (`pointzoneID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_5` FOREIGN KEY (`dtmDestinationZoneID_FK`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_6` FOREIGN KEY (`dtmOriginPointID_FK`) REFERENCES `pointzone` (`pointzoneID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_7` FOREIGN KEY (`dtmOriginZoneID_FK`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_8` FOREIGN KEY (`dtmPlannerID_FK`) REFERENCES `user` (`userID_PK`),
  ADD CONSTRAINT `dtmlist_ibfk_9` FOREIGN KEY (`truckID_FK`) REFERENCES `trucks` (`truckID_PK`);

--
-- Constraints for table `servicefeeratesplace`
--
ALTER TABLE `servicefeeratesplace`
  ADD CONSTRAINT `servicefeeratesplace_ibfk_1` FOREIGN KEY (`origin`) REFERENCES `zones` (`zonesID_PK`),
  ADD CONSTRAINT `servicefeeratesplace_ibfk_2` FOREIGN KEY (`destination`) REFERENCES `zones` (`zonesID_PK`);

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`userID_FK`) REFERENCES `user` (`userID_PK`);

--
-- Constraints for table `transportcharge`
--
ALTER TABLE `transportcharge`
  ADD CONSTRAINT `transportcharge_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`),
  ADD CONSTRAINT `transportcharge_ibfk_2` FOREIGN KEY (`truckID_FK`) REFERENCES `trucks` (`truckID_PK`),
  ADD CONSTRAINT `transportcharge_ibfk_3` FOREIGN KEY (`dtmID_FK`) REFERENCES `dtmlist` (`dtmID_PK`);

--
-- Constraints for table `transportrate`
--
ALTER TABLE `transportrate`
  ADD CONSTRAINT `transportrate_ibfk_1` FOREIGN KEY (`companyID_FK`) REFERENCES `company` (`companyID_PK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
