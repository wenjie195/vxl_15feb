var URL_POINTS = "points"; // OR places
var URL_ZONES = "zones";
var URL_COSTCENTER = "costcenter";
var URL_COMPANY = "company";// OR agents
var URL_USERS = "users";
var URL_DRIVERS = "drivers";
var URL_TRUCKS = "trucks";
var URL_DTM = "dtm";// var URL_SSIP = "ssip";
var URL_DRIVER_SERVICE_FEE = "driverservicefee"; 
var URL_TRANSPORT_CHARGES = "transportcharges"; 
var URL_INVOICE = "invoice"; 

var pn = null;
var f = null;
var s = null;

function initializeFireBase()
{
   let db = firebase.firestore();

   db.settings({
   timestampsInSnapshots: true
   });
   return db;
}
function incrementCounter(type) 
{
   let db = initializeFireBase();
   let dbReferenceAdd;

    if(type == 1)
    {
        dbReferenceAdd = db.collection(URL_POINTS).doc("listen");
    }
    if(type == 2)
    {
        dbReferenceAdd = db.collection(URL_ZONES).doc("listen");
    }
    if(type == 3)
    {
        dbReferenceAdd = db.collection(URL_COSTCENTER).doc("listen");
    }
    if(type == 4)
    {
        dbReferenceAdd = db.collection(URL_COMPANY).doc("listen");
    }
    if(type == 5)
    {
        dbReferenceAdd = db.collection(URL_USERS).doc("listen");
    }
    if(type == 6)
    {
        dbReferenceAdd = db.collection(URL_DRIVERS).doc("listen");
    }
    if(type == 7)
    {
        dbReferenceAdd = db.collection(URL_TRUCKS).doc("listen");
    }
    if(type == 8)
    {
        dbReferenceAdd = db.collection(URL_DTM).doc("listen");
    }
    if(type == 9)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 20)
    {
        dbReferenceAdd = db.collection(URL_TRANSPORT_CHARGES).doc("listen");
    }
    if(type == 21)
    {
        dbReferenceAdd = db.collection(URL_INVOICE).doc("listen");
    }
    if(type == 22)
    {
        dbReferenceAdd = db.collection(URL_INVOICE).doc("listen");
    }
    if(type == 31)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 32)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 33)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 41)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 42)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
   return db.runTransaction(t => {
       return t.get(dbReferenceAdd).then(doc => {
           const new_count = doc.data().counter + 1;
           t.update(dbReferenceAdd, { counter: new_count });
       });
   });
}
function decrementCounter(type) 
{
   let db = initializeFireBase();
   let dbReferenceAdd;

    if(type == 1)
    {
        dbReferenceAdd = db.collection(URL_POINTS).doc("listen");
    }
    if(type == 2)
    {
        dbReferenceAdd = db.collection(URL_ZONES).doc("listen");
    }
    if(type == 3)
    {
        dbReferenceAdd = db.collection(URL_COSTCENTER).doc("listen");
    }
    if(type == 4)
    {
        dbReferenceAdd = db.collection(URL_COMPANY).doc("listen");
    }
    if(type == 5)
    {
        dbReferenceAdd = db.collection(URL_USERS).doc("listen");
    }
    if(type == 6)
    {
        dbReferenceAdd = db.collection(URL_DRIVERS).doc("listen");
    }
    if(type == 7)
    {
        dbReferenceAdd = db.collection(URL_TRUCKS).doc("listen");
    }
    if(type == 8)
    {
        dbReferenceAdd = db.collection(URL_DTM).doc("listen");
    }
    if(type == 9)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 30)
    {
        dbReferenceAdd = db.collection(URL_TRANSPORT_CHARGES).doc("listen");
    }
    if(type == 31)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 32)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 33)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 41)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
    if(type == 42)
    {
        dbReferenceAdd = db.collection(URL_DRIVER_SERVICE_FEE).doc("listen");
    }
   return db.runTransaction(t => {
       return t.get(dbReferenceAdd).then(doc => {
           const new_count = doc.data().counter - 1;
           t.update(dbReferenceAdd, { counter: new_count });
       });
   });
}
function ajaxDriverFee()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVER_SERVICE_FEE);
    let pageName = 'feeDriverServiceShow.php';
    let fromPage = 33;

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            $('#showDriverFee').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,fromPage);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+fromPage).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,fromPage);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxDTM()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DTM);
    let pageName = 'dtmShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: 8
            }
        })
        .done(function(message) 
        {
            $('#showDTM').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,8);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+8).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+8).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,8);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxTruck()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_TRUCKS);
    let pageName = 'truckShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: 7
            }
        })
        .done(function(message) 
        {
            $('#showTruck').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,7);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+7).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+7).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,7);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxDriverServiceFee(num)// for the driver to finish //
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVER_SERVICE_FEE);
    let pageName = 'dsfShow.php';
    let fromPage = 0;

    if(num == 1)
    {
        fromPage = 9;
    }
    else
    {
        fromPage = 10;
    }

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            if(fromPage == 9)
            {
                $('#showUnselectedDriverServiceFee').html(message);
                  // Setting up filter,pagination and search
                if(pn == null && f == null && s == null)
                {
                    checkCondition(null,null,null,1,fromPage);
                }
                else
                {
                    if(s != null)
                    {
                        document.getElementById('search'+fromPage).value = s;
                    }
                    if(f != null)
                    {
                        document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                    }
                    checkCondition(pn,f,s,0,fromPage);
                }
            }
            else
            {
                $('#showDriverServiceFee').html(message);
                  // Setting up filter,pagination and search
                if(pn == null && f == null && s == null)
                {
                    checkCondition(null,null,null,1,fromPage);
                }
                else
                {
                    if(s != null)
                    {
                        document.getElementById('search'+fromPage).value = s;
                    }
                    if(f != null)
                    {
                        document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                    }
                    checkCondition(pn,f,s,0,fromPage);
                }
            }          
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxTransportCharges(fromPage)
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_TRANSPORT_CHARGES);
    let pageName = 'transportChargesShow.php';
    

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            $('#showTransportCharges').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,fromPage);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+fromPage).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,fromPage);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxDriver()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVERS);
    let pageName = 'driverShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: 6
            }
        })
        .done(function(message) 
        {
            $('#showDriver').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,6);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+6).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+6).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,6);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxEachServiceFee(driverID)
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVER_SERVICE_FEE);
    let pageName = 'eachDriverServiceFeeShow.php';
    let fromPage = 42;

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage,
                driverID:driverID
            }
        })
        .done(function(message) 
        {
            $('#showEachServiceFee').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,fromPage);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+fromPage).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,fromPage);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxFee()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVER_SERVICE_FEE);
    let pageName = 'feeShow.php';
    let fromPage = 32;

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            $('#showFee').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,fromPage);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+fromPage).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,fromPage);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxUser()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_USERS);
    let pageName = 'CDShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: 5
            }
        })
        .done(function(message) 
        {
            $('#showUser').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,5);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+5).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+5).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,5);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxAdditionalFeeRates()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVER_SERVICE_FEE);
    let fromPage = 41;

    refDB.onSnapshot(function(doc) 
    {
    
    pageName = 'setAdditionalFeeRatesShow.php';
    $.ajax({
        url: pageName,
        type: 'POST',
        async: false,
        data:
        {
            fromPage: 41
        }
    })
    .done(function(message) 
    {
        $('#showAdditionalFeeRates').html(message);
        // Setting up filter,pagination and search
        if(pn == null && f == null && s == null)
        {
            checkCondition(null,null,null,1,fromPage);
        }
        else
        {
            if(s != null)
            {
                document.getElementById('search'+fromPage).value = s;
            }
            if(f != null)
            {
                document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
            }
            checkCondition(pn,f,s,0,fromPage);
        }
    })
    .fail(function() 
    {
        alert( "Error : There is a server problem OR there is no internet connection" );
    });
});
}
function ajaxFeeRates()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_DRIVER_SERVICE_FEE);
    let fromPage = 31;

    refDB.onSnapshot(function(doc) 
    {
    
    pageName = 'setFeeRatesShow.php';
    $.ajax({
        url: pageName,
        type: 'POST',
        async: false,
        data:
        {
            fromPage: 31
        }
    })
    .done(function(message) 
    {
        $('#showFeeRates').html(message);
        // Setting up filter,pagination and search
        if(pn == null && f == null && s == null)
        {
            checkCondition(null,null,null,1,fromPage);
        }
        else
        {
            if(s != null)
            {
                document.getElementById('search'+fromPage).value = s;
            }
            if(f != null)
            {
                document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
            }
            checkCondition(pn,f,s,0,fromPage);
        }
    })
    .fail(function() 
    {
        alert( "Error : There is a server problem OR there is no internet connection" );
    });
});
}
function ajaxTranscharge()
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_TRANSPORT_CHARGES);
    let fromPage = 30;

    refDB.onSnapshot(function(doc) 
    {
    
    pageName = 'setTransChargeShow.php';
    $.ajax({
        url: pageName,
        type: 'POST',
        async: false,
        data:
        {
            fromPage: 30
        }
    })
    .done(function(message) 
    {
        $('#showTransCharge').html(message);
        // Setting up filter,pagination and search
        if(pn == null && f == null && s == null)
        {
            checkCondition(null,null,null,1,fromPage);
        }
        else
        {
            if(s != null)
            {
                document.getElementById('search'+fromPage).value = s;
            }
            if(f != null)
            {
                document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
            }
            checkCondition(pn,f,s,0,fromPage);
        }
    })
    .fail(function() 
    {
        alert( "Error : There is a server problem OR there is no internet connection" );
    });
});
}
function ajaxCoordinator(fromPage)//value represents data that i want to show
{
    let db = initializeFireBase();
    let refDB;
    let pageName;
    
    if(fromPage == 1)//places
    {
        refDB = db.collection(URL_POINTS);
        pageName = 'CDShow.php';
    }
    if(fromPage == 2)//zones
    {
        refDB = db.collection(URL_ZONES);
        pageName = 'CDShow.php';
    }
    if(fromPage == 3)//cost centers
    {
        refDB = db.collection(URL_COSTCENTER);
        pageName = 'CDShow.php';
    }
    if(fromPage == 4)//agents
    {
        refDB = db.collection(URL_COMPANY);
        pageName = 'CDShow.php';
    }

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            if(fromPage == 1)
            {
                $('#showPlaces').html(message);
            }
            if(fromPage == 2)
            {
                $('#showZones').html(message);
            }
            if(fromPage == 3)
            {
                $('#showCostCenter').html(message);
            }
            if(fromPage == 4)
            {
                $('#showAgent').html(message);
            }

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,fromPage);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+fromPage).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,fromPage);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function ajaxInvoice(fromPage)
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_INVOICE);
    let pageName = 'invoiceShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            $('#showInvoice').html(message);

            // Setting up filter,pagination and search
            if(pn == null && f == null && s == null)
            {
                checkCondition(null,null,null,1,fromPage);
            }
            else
            {
                if(s != null)
                {
                    document.getElementById('search'+fromPage).value = s;
                }
                if(f != null)
                {
                    document.getElementById('filter'+fromPage).selectedIndex = "'"+(f-1)+"'";
                }
                checkCondition(pn,f,s,0,fromPage);
            }
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function checkCondition(pageNo,filter,searchWord,type,fromPage)
{
    var condition = 0;
    var url_ajax;

    if(type == 1)
    {
        condition = 1;
        pageNo = 1
        filter = 1
        searchWord = "";
    }
    else
    {
        if(filter == null && searchWord == null)
        {
            if(s != null)
            {
                searchWord = s;
            }
            else
            {
                searchWord = "";
            }
            if(f != null)
            {
                filter = f;
            }
            else
            {
                filter = 1;
            }
            pn = pageNo;
            
        }
        if(pageNo == null && searchWord == null)// means filter has value
        {
            if(s != null)
            {
                searchWord = s;
            }
            else
            {
                searchWord = "";
            }
    
            if(pn != null)
            {
                pageNo = pn;
            }
            else
            {
                pageNo = 1;
            }
            f = filter;
        }
        if(pageNo == null && filter == null)// means searchword has value
        {
            if(f != null)
            {
                filter = f;
            }
            else
            {
                filter = 1;
            }
    
            if(pn != null)
            {
                pageNo = pn;
            }
            else
            {
                pageNo = 1;
            }
    
            if(document.getElementById("search"+fromPage).value != null && document.getElementById("search"+fromPage).value !="")
            {
                searchWord = document.getElementById("search"+fromPage).value;
            }
            else
            {
                searchWord = "";
            }
            s = searchWord;
        }
        if(pageNo != null && filter != null && searchWord != null)
        {
            s = searchWord;
            pn = pageNo;
            f = filter;
        }
    }
    if(fromPage == 1)
    {
        url_ajax = 'CDShowTable.php';
    }
    if(fromPage == 2)
    {
        url_ajax = 'CDShowTable.php';
    }
    if(fromPage == 3)
    {
        url_ajax = 'CDShowTable.php';
    }
    if(fromPage == 4)
    {
        url_ajax = 'CDShowTable.php';
    }
    if(fromPage == 5)
    {
        url_ajax = 'CDShowTable.php';
    }
    if(fromPage == 6)
    {
        url_ajax = 'driverShowTable.php';
    }
    if(fromPage == 7)
    {
        url_ajax = 'truckShowTable.php';
    }
    if(fromPage == 8)
    {
        url_ajax = 'dtmShowTable.php';
    }
    if(fromPage == 9)
    {
        url_ajax = 'dsfShowTable.php';
    }
    if(fromPage == 10)
    {
        url_ajax = 'dsfShowTable.php';
    }
    if(fromPage == 20 || fromPage == 21)
    {
        url_ajax = 'transportChargesShowTable.php';
    }
    if(fromPage == 22)
    {
        url_ajax = 'invoiceShowTable.php';
    }
    if(fromPage == 23)
    {
        url_ajax = 'salesReportShowTable.php';
    }
    if(fromPage == 30)
    {
        url_ajax = 'setTransChargeShowTable.php';
    }
    if(fromPage == 31)
    {
        url_ajax = 'setFeeRatesShowTable.php';
    }
    if(fromPage == 32)
    {
        url_ajax = 'feeShowTable.php';
    }
    if(fromPage == 33)
    {
        url_ajax = 'feeDriverServiceShowtable.php';
    }
    if(fromPage == 41)
    {
        url_ajax = 'setAdditionalFeeRatesShowTable.php';
    }
    if(fromPage == 42)
    {
        url_ajax = 'eachDriverServiceFeeShowTable.php';
    }
    $.ajax({
        url: url_ajax,
        type: 'POST',
        async: false,
        data:
        {
            fromPage:fromPage,
            pageNo:pageNo,
            filter:filter,
            searchWord:searchWord,
            condition:condition
        }
    })
    .done(function(message) 
    {
        $('#getTable'+fromPage).html(message);
    })
    .fail(function() 
    {
        alert( "error Ajax number 2" );
    });
    
}
function addDTMData(fromTable,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";

    if(fromTable == 8)
    {
        if(addOrEdit == 1)
        {
            message += "insert this dtm";
        }
        else
        {
            message += "edit this dtm";
        }
    }
    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        if(fromTable == 8)
        {
            addEditDTM(addOrEdit);
        }
    }
}

function addHRData(fromTable,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";

    if(fromTable == 6)
    {
        if(addOrEdit == 1)
        {
            message += "insert this driver";
        }
        else
        {
            message += "edit this driver";
        }
    }
    
    if(fromTable == 7)
    {
        if(addOrEdit == 1)
        {
            message += "insert this truck";
        }
        else
        {
            message += "edit this truck";
        }
    }

    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        if(fromTable == 6)
        {
            addEditDrivers(addOrEdit);
        }
        if(fromTable == 7)
        {
            addEditTrucks(addOrEdit);
        }
    }
}
function addTransportRates(fromTable,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";

    if(fromTable == 30)
    {
        if(addOrEdit == 1)
        {
            message += "insert this transport rates";
        }
        else
        {
            message += "edit this transport rates";
        }
    }
    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        if(fromTable == 30)
        {
            addEditTransportRates(addOrEdit);
        }
    }
}
function addAdminData(fromTable,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";

    if(fromTable == 1)
    {
        if(addOrEdit == 1)
        {
            message += "insert this place";
        }
        else
        {
            message += "edit this place";
        }
    }
    if(fromTable == 2)
    {
        if(addOrEdit == 1)
        {
            message += "insert this zone";
        }
        else
        {
            message += "edit this zone";
        }
    }
    if(fromTable == 3)
    {
        if(addOrEdit == 1)
        {
            message += "insert this cost center";
        }
        else
        {
            message += "edit this cost center";
        }
    }
    if(fromTable == 4)
    {
        if(addOrEdit == 1)
        {
            message += "insert this company";
        }
        else
        {
            message += "edit this company";
        }
    }
    if(fromTable == 5)
    {
        if(addOrEdit == 1)
        {
            message += "insert this user";
        }
        else
        {
            message += "edit this user";
        }
    }
    if(fromTable == 31)
    {
        if(addOrEdit == 1)
        {
            message += "insert this service fee";
        }
        else
        {
            message += "edit this service fee";
        }
    }
    if(fromTable == 41)
    {
        if(addOrEdit == 1)
        {
            message += "insert this additional service fee";
        }
        else
        {
            message += "edit this additional service fee";
        }
    }

    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        if(fromTable == 1)
        {
            addEditPlaces(addOrEdit);
        }
        if(fromTable == 2)
        {
            addEditZones(addOrEdit);
        }
        if(fromTable == 3)
        {
            addEditCostCenter(addOrEdit);
        }
        if(fromTable == 4)
        {
            addEditAgent(addOrEdit);
        }
        if(fromTable == 5)
        {
            addEditUser(addOrEdit);
        }
        if(fromTable == 31)
        {
            addEditFee(addOrEdit);
        }
        if(fromTable == 41)
        {
            addEditAdditionalFee(addOrEdit);
        }
    }
    
}
function deleteHR(fromPage,idpk)
{
    let modal = document.getElementById('myModal'+fromPage);
    let span = document.getElementsByClassName("close"+fromPage)[0];
    let header = document.getElementById('header'+fromPage);
    let confirm = document.getElementById('confirm'+fromPage);
    let cancel = document.getElementById('cancel'+fromPage);
    let text = document.getElementById('text'+fromPage);
    let message = "Are you sure to ";

    if(fromPage == 6)
    {
        message += "delete this driver";
    }
    if(fromPage == 7)
    {
        message += "delete this truck";
    }

    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        ajaxDeleteHRData(fromPage,idpk);
    }
}
function deleteAdmin(fromPage,idpk)
{
    let modal = document.getElementById('myModal'+fromPage);
    let span = document.getElementsByClassName("close"+fromPage)[0];
    let header = document.getElementById('header'+fromPage);
    let confirm = document.getElementById('confirm'+fromPage);
    let cancel = document.getElementById('cancel'+fromPage);
    let text = document.getElementById('text'+fromPage);
    let message = "Are you sure to ";

    if(fromPage == 1)
    {
        message += "delete this place";
    }
    if(fromPage == 2)
    {
        message += "delete this zone";
    }
    if(fromPage == 3)
    {
        message += "delete this cost center";
    }
    if(fromPage == 4)
    {
        message += "delete this company";
    }
    if(fromPage == 5)
    {
        message += "delete this user";
    }
    if(fromPage == 30)
    {
        message += "delete this transport charges";
    }
    if(fromPage == 31)
    {
        message += "delete this service fee";
    }

    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        ajaxDeleteCoordinatorData(fromPage,idpk);
    }
}
function validateTextField(field,error)
{
    if(field == null || !field || field == "")
    {
        error++;
    }
    return error;
}
function addEditPlaces(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();

    error = validateTextField(field_1,error);

    if(error == 0)
    {
        var map = 
        {   
            addOrEdit:addOrEdit,
            fromPage: 1,
            pointzonePlaceName: field_1
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,1);
    }
}
function addEditTransportRates(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();
    let field_5 = $('#field_5').val();
    let field_6 = $('#field_6').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    error = validateTextField(field_3,error);
    error = validateTextField(field_4,error);
    error = validateTextField(field_5,error);
    error = validateTextField(field_6,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 30,
            companyName: field_1,
            loadCapacity: field_2,
            transportCharges: field_3,
            consoleRate:field_4,
            origin:field_5,
            destination:field_6
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            $('#field_3').val("");
            $('#field_4').val("");
            ajaxAddTransportRates(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddTransportRates(map);
        }
    }
    else
    {
        generalModal("",2,2);
    }
}
function addEditZones(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 2,
            zonesName: field_1,
            zonesState: field_2
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,2);
    }
}
function addEditCostCenter(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 3,
            costCenterName: field_1,
            companyID_FK:field_2
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,3);
    }
}
function addEditAgent(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 4,
            companyName: field_1,
            companyShortForm: field_2
        };

        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,4);
    }
}
function addEditAdditionalFee(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();
    let field_5 = $('#field_5').val();
    let field_6 = $('#field_6').val();
    let field_7 = $('#field_7').val();
    let field_8 = $('#field_8').val();
    let field_9 = $('#field_9').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    error = validateTextField(field_3,error);
    error = validateTextField(field_4,error);
    error = validateTextField(field_5,error);
    error = validateTextField(field_6,error);
    error = validateTextField(field_7,error);
    error = validateTextField(field_8,error);
    error = validateTextField(field_9,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 41,
            loadTrans: field_1,
            overnight: field_2,
            nightDeliveries: field_3,
            sunday: field_4,
            public: field_5,
            cancel: field_6,
            lockupDay: field_7,
            lockupNight: field_8,
            consol: field_9
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            $('#field_3').val("");
            $('#field_4').val("");

            $('#field_5').val("");
            $('#field_6').val("");
            $('#field_7').val("");
            $('#field_8').val("");
            $('#field_9').val("");
            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,2);
    }
}
function addEditFee(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();
    let field_5 = $('#field_5').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    error = validateTextField(field_3,error);
    error = validateTextField(field_4,error);
    error = validateTextField(field_5,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 31,
            origin: field_1,
            destination: field_2,
            rates: field_3,
            loadTransport: field_4,
            noOfDrivers: field_5
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            $('#field_3').val("");
            $('#field_4').val("");
            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,2);
    }
}
function getValuefromCheckedbox(checkBox)
{
    if(checkBox.is(':checked'))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
function addEditServiceFeeDriver2(addOrEdit,idPK)
{
    console.log(1);
    let error = 0;

    let grossDsf = $('#dsfGross').val();
    let overnight = $('#overnight');
    let nightDeliveries = $('#nightDeliveries');
    let sunday = $('#sunday');
    let public = $('#public');
    let cancelllation = $('#cancelllation');
    let lockupDay = $('#lockupDay');
    let lockupNight = $('#lockupNight');
    let remarkDsf = $('#remarkDsf').val();
    let detentioncharge = $('#detentioncharge').val();

    let dsfOvernight = $('#dsfOvernight').val();
    let dsfNightDelivery = $('#dsfNightDelivery').val();
    let dsfSunday = $('#dsfSunday').val();
    let dsfPublic = $('#dsfPublic').val();
    let dsfCancellation = $('#dsfCancellation').val();
    let dsfLockupDay = $('#dsfLockupDay').val();
    let dsfLockupNight = $('#dsfLockupNight').val();


    let dtmID = $('#dtmID').val();
    let driver1 = $('#driver1').val();
    let driver2 = $('#driver2').val();
    let driverNo = $('#driverNo').val();
    let loadCap = $('#loadCap').val();

    console.log(error);

    error = validateTextField(remarkDsf,error);
    error = validateTextField(detentioncharge,error);

    if(error == 0)
    {
        var map = 
        {
            idPK:idPK,
            dtmID:dtmID,
            driverNo:driverNo,
            driver1:driver1,
            driver2:driver2,
            addOrEdit:addOrEdit,
            fromPage: 32,
            dsfGross: grossDsf,
            detention:detentioncharge,
            loadCap:loadCap,
            remarkDsf:remarkDsf,
            dsfOvernight:dsfOvernight,
            dsfNightDelivery:dsfNightDelivery,
            dsfSunday:dsfSunday,
            dsfPublic:dsfPublic,
            dsfCancellation:dsfCancellation,
            dsfLockupDay:dsfLockupDay,
            dsfLockupNight:dsfLockupNight,
            isovernight:getValuefromCheckedbox(overnight),
            isnightDeliveries:getValuefromCheckedbox(nightDeliveries),
            issunday:getValuefromCheckedbox(sunday),
            ispublic:getValuefromCheckedbox(public),
            iscancelllation:getValuefromCheckedbox(cancelllation),
            islockupDay:getValuefromCheckedbox(lockupDay),
            islockupNight:getValuefromCheckedbox(lockupNight)
        };
        if(addOrEdit == 1)
        {

            ajaxAddCoordinatorData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        generalModal("",2,2);
    }
}
function addEditUser(addOrEdit)
{
    let error = 0;
    let specError = "";

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();
    let field_5 = $('#field_5').val();
    let field_6 = $('#field_6').val();
    let field_7 = $('#field_7').val();
    let field_8 = $('#field_8').val();

    let icRGEX = /^\d{6}\d{2}\d{4}$/;
    let phoneRGEX = /^(\+?6?01)[0-46-9]-*[0-9]{7,8}$/;
    let emailRGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    
    let icResult = icRGEX.test($('#field_4').val());
    let phoneResult = phoneRGEX.test($('#field_5').val());
    let emailResult = emailRGEX.test($('#field_6').val());

    if(phoneResult == false && icResult == false && emailResult == false)
    {
        specError += "<br>Please enter valid phone number ,valid IC No and valid email";
        error++;
    }
    else
    {
        specError += "<br>Please enter a ";
        if(phoneResult == false)
        {
            specError += "valid phone number,";
            error++;
        }
        if(icResult == false)
        {
            specError += "valid IC Number,";
            error++;
        }
        if(emailResult == false)
        {
            specError += "valid Email.";
            error++;
        }
    }

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    error = validateTextField(field_3,error);
    error = validateTextField(field_4,error);
    error = validateTextField(field_5,error);
    error = validateTextField(field_6,error);
    error = validateTextField(field_7,error);
    error = validateTextField(field_8,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 5,
            userName: field_1,
            userNickName: field_2,
            userPosition: field_3,
            userIC: field_4,
            userTele: field_5,
            userEmail: field_6,
            userAddress: field_7,
            userState: field_8
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            $('#field_4').val("");
            $('#field_5').val("");
            $('#field_6').val("");
            $('#field_7').val("");
            $('#field_8').val("");
            ajaxAddCoordinatorData(map);
            
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddCoordinatorData(map);
        }
    }
    else
    {
        let header = document.getElementById('noticeHeader');
        let text = document.getElementById('noticeText');
        let modal = document.getElementById('noticeModal');
        let span = document.getElementsByClassName("closeNoticeModal")[0];

        header.innerHTML = "Error! ";
        text.innerHTML = "The data entered is not valid! <br>"+specError;

        modal.style.display = "block";

        span.onclick = function() {
        modal.style.display = "none";
        }
    
        window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        }
    }

}
function addEditDrivers(addOrEdit)
{
    let error = 0;
    let specError = "";

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();

    let icRGEX = /^\d{6}\d{2}\d{4}$/;
    let phoneRGEX = /^(\+?6?01)[0-46-9]-*[0-9]{7,8}$/;

    let icResult = icRGEX.test($('#field_3').val());
    let phoneResult = phoneRGEX.test($('#field_4').val());

    if(phoneResult == false && icResult == false)
    {
        specError += "<br>Please enter valid phone number and valid IC No";
        error++;
    }
    else
    {
        specError += "<br>Please enter a ";
        if(phoneResult == false)
        {
            specError += "valid phone number,";
            error++;
        }
        if(icResult == false)
        {
            specError += "valid IC Number,";
            error++;
        }
    }

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    error = validateTextField(field_3,error);
    error = validateTextField(field_4,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 6,
            driverName: field_1,
            driverNickName: field_2,
            driverICno: field_3,
            driverPhoneNo: field_4
        };

        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            $('#field_3').val("");
            $('#field_4').val("");
            ajaxAddHRData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddHRData(map);
        }
    }
    else
    {
        let header = document.getElementById('noticeHeader');
        let text = document.getElementById('noticeText');
        let modal = document.getElementById('noticeModal');
        let span = document.getElementsByClassName("closeNoticeModal")[0];

        header.innerHTML = "Error! ";
        text.innerHTML = "The data entered is not valid! <br>"+specError;

        modal.style.display = "block";

        span.onclick = function() {
        modal.style.display = "none";
        }
    
        window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        }
    }
}
function addEditTrucks(addOrEdit)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();
    let field_5 = $('#field_5').val();
    let field_6 = $('#field_6').val();

    error = validateTextField(field_1,error);
    error = validateTextField(field_2,error);
    error = validateTextField(field_3,error);
    error = validateTextField(field_4,error);
    error = validateTextField(field_5,error);
    error = validateTextField(field_6,error);

    if(error == 0)
    {
        var map = 
        {
            addOrEdit:addOrEdit,
            fromPage: 7,
            truckPlateNo: field_1,
            truckCapacity: field_2,
            truckMade: field_3,
            truckModel: field_4,
            truckCustomBond: field_5,
            truckCustomExpired: field_6
        };
        if(addOrEdit == 1)
        {
            $('#field_1').val("");
            $('#field_2').val("");
            $('#field_3').val("");
            $('#field_4').val("");
            $('#field_5').val("");
            $('#field_6').val("");
            ajaxAddHRData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddHRData(map);
        }
    }
    else
    {
        generalModal("",2,7);
    }
}
function addEditServiceFeeDriver(addOrEdit)
{
    let error = 0;
    let load = "None";
    let dc = 0;

    error = validateTextField($('#input-dsf-doNo').val(),error);
    error = validateTextField($('#input-dsf-dtmID_FK').val(),error);
    error = validateTextField($('#input-dsf-night').val(),error);
    error = validateTextField($('#input-dsf-public').val(),error);
    if($('#input-dsf-load'))
    {
        error = validateTextField($('#input-dsf-load').val(),error);
        load = $('#input-dsf-load').val();
    }

    if($('#secDriver').is(':checked'))
    {
        error = validateTextField($('#input-dsf-dc').val(),error);
        dc = $('#input-dsf-dc').val();
    }

    let map =
        {
            addOrEdit:addOrEdit,
            fromPage: 9,
            night: $('#input-dsf-night').val(),
            public: $('#input-dsf-public').val(),
            load: load,
            dc: dc,
            dsfDONumber: $('#input-dsf-doNo').val(),
            dtmID_FK: $('#input-dsf-dtmID_FK').val()
        };

    if(error == 0)
    {
        if(addOrEdit == 1)
        {
            ajaxAddDriverServiceFee(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddDriverServiceFee(map);
        }
    }
    else
    {
        generalModal("",2,8);
    }    
}
function addEditDTM(addOrEdit)
{
    let aa,ab,ac,ad,ae,af,ag,ah;
    let error = 0;
    let message = "";

    error = validateTextField($('#input-ssip-company').val(),error);
    error = validateTextField($('#input-ssip-bookingDate').val(),error);
    error = validateTextField($('#input-ssip-bookingTime').val(),error);
    error = validateTextField($('#input-ssip-originPoint').val(),error);
    error = validateTextField($('#input-ssip-destinationPoint').val(),error);
    error = validateTextField($('#input-ssip-originZone').val(),error);
    error = validateTextField($('#input-ssip-destinationZone').val(),error);
    error = validateTextField($('#input-dtm-costCenter').val(),error);
    error = validateTextField($('#input-ssip-pickupDate').val(),error);
    error = validateTextField($('#input-ssip-pickupTime').val(),error);
    error = validateTextField($('#input-ssip-truckPlateNo').val(),error);
    error = validateTextField($('#input-ssip-driverName').val(),error);
    error = validateTextField($('#input-ssip-urgentMemo').val(),error);
    // console.log("batch 1 = "+error);
    var isConsol = 0;
    var isBooleanConsol = $('#input-ssip-isConsol').prop("checked"); 
    // console.log("isBooleanConsol = "+isBooleanConsol);

    if(isBooleanConsol == true)
    {
        isConsol = 1;
    }

    // console.log("isConsol = "+isConsol);

  
    if(error == 0)
    {
        message += "<br>-> Please fill all empty fields.";

        if($('#input-ssip-pickupTime').val().length != 4 
        || $('#input-ssip-bookingTime').val().length != 4
        || isNaN($('#input-ssip-pickupTime').val())
        || isNaN($('#input-ssip-bookingTime').val()))
        {
            error++;
            message += "<br>-> Please check Your booking/pickup time format.";
        }
        else
        {
            var hourPT = parseInt($('#input-ssip-pickupTime').val().slice(0,2));
            var minPT = parseInt($('#input-ssip-pickupTime').val().slice(2,4));

            var hourBT = parseInt($('#input-ssip-bookingTime').val().slice(0,2));
            var minBT = parseInt($('#input-ssip-bookingTime').val().slice(2,4));

            if(hourPT > 23 || hourBT > 23 || minPT > 59  || minBT > 59 )
            {
                error++;
                message += "<br>-> Please check Your booking/pickup time hour and minute format.";
            }
        }

        if(isNaN($('#input-ssip-bookingDate').val()) 
        || isNaN($('#input-ssip-pickupDate').val()) 
        || $('#input-ssip-bookingDate').val().length != 8 
        || $('#input-ssip-pickupDate').val().length != 8)
        {
            error++;
            message += "<br>-> Please check Your booking/pickup date format.";
        }
    }

    // console.log("batch 1 = "+error);
    if(addOrEdit == 2)
    {
        
        error = validateTextField($('#input-ssip-f').val(),error);
        error = validateTextField($('#input-ssip-originDockTime').val(),error);
        error = validateTextField($('#input-ssip-originCompleteLoadingTime').val(),error);
        error = validateTextField($('#input-ssip-originDepartureTimeFromPost').val(),error);
        error = validateTextField($('#input-ssip-destArrivalTimeAtPost').val(),error);
        error = validateTextField($('#input-ssip-destDockTime').val(),error);
        error = validateTextField($('#input-ssip-destCompleteUnloadTime').val(),error);
        error = validateTextField($('#input-ssip-destDepartTimeFromPost').val(),error);
    
        // console.log("batch edit 1 = "+error);


        if($('#input-ssip-f').val().length != 4 
        || $('#input-ssip-originDockTime').val().length != 4
        || $('#input-ssip-originCompleteLoadingTime').val().length != 4
        || $('#input-ssip-originDepartureTimeFromPost').val().length != 4
        || isNaN($('#input-ssip-f').val())
        || isNaN($('#input-ssip-originDockTime').val())
        || isNaN($('#input-ssip-originCompleteLoadingTime').val())
        || isNaN($('#input-ssip-originDepartureTimeFromPost').val()))
        {
            error++;
            message += "<br>-> Please check Your origin time format.";
        }
        else
        {
            var hourF = parseInt($('#input-ssip-f').val().slice(0,2));
            var houroriginDockTime = parseInt($('#input-ssip-originDockTime').val().slice(0,2));
            var houroriginCompleteLoadingTime = parseInt($('#input-ssip-originCompleteLoadingTime').val().slice(0,2));
            var houroriginDepartureTimeFromPost = parseInt($('#input-ssip-originDepartureTimeFromPost').val().slice(0,2));

            var minF = parseInt($('#input-ssip-f').val().slice(2,4));
            var minoriginDockTime = parseInt($('#input-ssip-originDockTime').val().slice(2,4));
            var minoriginCompleteLoadingTime = parseInt($('#input-ssip-originCompleteLoadingTime').val().slice(2,4));
            var minoriginDepartureTimeFromPost = parseInt($('#input-ssip-originDepartureTimeFromPost').val().slice(2,4));

            if(     hourF > 23 
                ||  houroriginDockTime > 23 
                ||  houroriginCompleteLoadingTime > 23 
                ||  houroriginDepartureTimeFromPost > 23 
                ||  minF > 59  
                ||  minoriginDockTime > 59  
                ||  minoriginCompleteLoadingTime > 59  
                ||  minoriginDepartureTimeFromPost > 59 )
            {
                error++;
                message += "<br>-> Please check Your origin time hour and minute format.";
            }
        }

        if($('#input-ssip-destArrivalTimeAtPost').val().length != 4
        || $('#input-ssip-destDockTime').val().length != 4
        || $('#input-ssip-destCompleteUnloadTime').val().length != 4
        || $('#input-ssip-destDepartTimeFromPost').val().length != 4
        || isNaN($('#input-ssip-destArrivalTimeAtPost').val())
        || isNaN($('#input-ssip-destDockTime').val())
        || isNaN($('#input-ssip-destCompleteUnloadTime').val())
        || isNaN($('#input-ssip-destDepartTimeFromPost').val()))
        {
            error++;
            message += "<br>-> Please check Your destination time format.";
        }
        else
        {
            var hourdestArrivalTimeAtPost = parseInt($('#input-ssip-destArrivalTimeAtPost').val().slice(0,2));
            var hourdestDockTime = parseInt($('#input-ssip-destDockTime').val().slice(0,2));
            var hourdestCompleteUnloadTime = parseInt($('#input-ssip-destCompleteUnloadTime').val().slice(0,2));
            var hourdestDepartTimeFromPost = parseInt($('#input-ssip-destDepartTimeFromPost').val().slice(0,2));

            var mindestArrivalTimeAtPost = parseInt($('#input-ssip-destArrivalTimeAtPost').val().slice(2,4));
            var mindestDockTime = parseInt($('#input-ssip-destDockTime').val().slice(2,4));
            var mindestCompleteUnloadTime = parseInt($('#input-ssip-destCompleteUnloadTime').val().slice(2,4));
            var mindestDepartTimeFromPost = parseInt($('#input-ssip-destDepartTimeFromPost').val().slice(2,4));

            if(     hourdestArrivalTimeAtPost > 23 
                ||  hourdestDockTime > 23 
                ||  hourdestCompleteUnloadTime > 23 
                ||  hourdestDepartTimeFromPost > 23 
                ||  mindestArrivalTimeAtPost > 59  
                ||  mindestDockTime > 59  
                ||  mindestCompleteUnloadTime > 59  
                ||  mindestDepartTimeFromPost > 59 )
            {
                error++;
                message += "<br>-> Please check Your destination time hour and minute format.";
            }
        }
        

        aa = $('#date_F_checked').prop("checked");
        ab = $('#date_dockTime_checked').prop("checked");
        ac = $('#date_completeLoadingTime_checked').prop("checked");
        ad = $('#date_departureTimeFromPost_checked').prop("checked");
        ae = $('#date_arrivalTimeAtPost_checked').prop("checked");
        af = $('#date_destinationDockTime_checked').prop("checked");
        ag = $('#date_completeUnloadTime_checked').prop("checked");
        ah = $('#date_departTimeFromPost_checked').prop("checked");

        if(aa == true)
        {
            error = validateTextField($('#lockup-f').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-f').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-f').val()) || $('#lockup-f').val().length != 8)
            {
                error++;
            }
        }
        if(ab == true)
        {
            error = validateTextField($('#lockup-dockTime').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-dockTime').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-dockTime').val()) || $('#lockup-dockTime').val().length != 8)
            {
                error++;
            }
        }
        if(ac == true)
        {
            error = validateTextField($('#lockup-completeLoadingTime').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-completeLoadingTime').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-completeLoadingTime').val()) || $('#lockup-completeLoadingTime').val().length != 8)
            {
                error++;
            }
        }
        if(ad == true)
        {
            error = validateTextField($('#lockup-originDepartureTimeFromPost').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-originDepartureTimeFromPost').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-originDepartureTimeFromPost').val()) || $('#lockup-originDepartureTimeFromPost').val().length != 8)
            {
                error++;
            }
        }
        if(ae == true)
        {
            error = validateTextField($('#lockup-arrivalTimeAtPost').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-arrivalTimeAtPost').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-arrivalTimeAtPost').val()) || $('#lockup-arrivalTimeAtPost').val().length != 8)
            {
                error++;
            }
        }
        if(af == true)
        {
            error = validateTextField($('#lockup-destinationDockTime').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-destinationDockTime').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-destinationDockTime').val()) || $('#lockup-destinationDockTime').val().length != 8)
            {
                error++;
            }
        }
        if(ag == true)
        {
            error = validateTextField($('#lockup-completeUnloadTime').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-completeUnloadTime').val(),error);
                if(error > 0)
                {
                    message += "<br>->transport dates must be more than pickup date !";
                }
            }
            if(isNaN($('#lockup-completeUnloadTime').val()) || $('#lockup-completeUnloadTime').val().length != 8)
            {
                error++;
            }
        }
        if(ah == true)
        {
            error = validateTextField($('#lockup-departTimeFromPost').val(),error);
            if(error == 0)
            {
                error = validateLockupDate(1,$('#input-ssip-pickupDate').val(),$('#lockup-departTimeFromPost').val(),error);
                if(error > 0)
                {
                    message += "<br>->pickup date must be less than transport dates !";
                }
            }
            if(isNaN($('#lockup-departTimeFromPost').val()) || $('#lockup-departTimeFromPost').val().length != 8)
            {
                error++;
            }
        }
        // console.log("batch 2 edit = "+error);

        if(error == 0)
        {
            if(aa == true && ab == true)
            {
                error = validateLockupDate(1,$('#lockup-f').val(),$('#lockup-dockTime').val(),error);
            }
            if(ab == true && ac == true)
            {
                error = validateLockupDate(1,$('#lockup-dockTime').val(),$('#lockup-completeLoadingTime').val(),error);
            }
            if(ac == true && ad == true)
            {
                error = validateLockupDate(1,$('#lockup-completeLoadingTime').val(),$('#lockup-originDepartureTimeFromPost').val(),error);
            }
            if(ad == true && ae == true)
            {
                error = validateLockupDate(1,$('#lockup-originDepartureTimeFromPost').val(),$('#lockup-arrivalTimeAtPost').val(),error);
            }
            if(ae == true && af == true)
            {
                error = validateLockupDate(1,$('#lockup-arrivalTimeAtPost').val(),$('#lockup-destinationDockTime').val(),error);
            }
            if(af == true && ag == true)
            {
                error = validateLockupDate(1,$('#lockup-destinationDockTime').val(),$('#lockup-completeUnloadTime').val(),error);
            }
            if(ag == true && ah == true)
            {
                error = validateLockupDate(1,$('#lockup-completeUnloadTime').val(),$('#lockup-departTimeFromPost').val(),error);
            }
            // console.log("batch 3 edit = "+error);

            if(error > 0)
            {
                message += "<br>-> Wrong lockup dates !";
            }
        }

        if(error == 0)
        {
            error = checkChekedCheckboxInDTM(aa,ab,error);
            error = checkChekedCheckboxInDTM(ab,ac,error);
            error = checkChekedCheckboxInDTM(ac,ad,error);
            error = checkChekedCheckboxInDTM(ad,ae,error);
            error = checkChekedCheckboxInDTM(ae,af,error);
            error = checkChekedCheckboxInDTM(af,ag,error);
            error = checkChekedCheckboxInDTM(ag,ah,error);
    
            if(error > 0)
            {
                message += "<br>-> Please check the lockup dates !";
            }
        }
        // console.log("batch 4 edit = "+error);
    }

    error = validateTextField($('#input-ssip-loadCap').val(),error);
    error = validateTextField($('#input-ssip-tripOnTime').val(),error);
    error = validateTextField($('#input-dtm-requestBy').val(),error);
    // console.log("batch 2 = "+error);

    let cartons = $('#input-ssip-quantityTypeCartons').prop('checked');
    let pallets = $('#input-ssip-quantityTypePallets').prop('checked');
    let cages = $('#input-ssip-quantityTypeCages').prop('checked');

    let cartonsVAL = 0 ;
    let palletsVAL  = 0;
    let cagesVAL  = 0 ;

    let qc1 = 0 ;
    let qc2 = 0 ;
    let qc3 = 0 ;

    let secondDrv = null;

    if(cartons == false && pallets == false && cages == false)
    {
        error++;
    }
    if(cartons == true)
    {
        cartonsVAL = 1;
        qc1 = $('#input-ssip-quantityCartons').val();
    }
    if(pallets == true)
    {
        palletsVAL = 1;
        qc2 = $('#input-ssip-quantityPallets').val();
    }
    if(cages == true)
    {
        cagesVAL = 1;
        qc3 = $('#input-ssip-quantityCages').val();
    }

    if($('#secDriver').prop('checked') && $('#input-ssip-driverName2').val() != null)
    {
        secondDrv = $('#input-ssip-driverName2').val();
    }

    // console.log("batch 3 = "+error);

    let vxlDONO = null;
    // console.log($('#input-dtm-vxlDoNo').val());

    if($('#input-dtm-vxlDoNo').val() != null && $('#input-dtm-vxlDoNo').val() != "")
    {
        vxlDONO = ""+$('#input-dtm-vxlDoNo').val()+"";
    }

    let map =
        {
            addOrEdit:addOrEdit,
            fromPage: 8,
            dgfRef: $('#input-ssip-dgfRef').val(),
            pickupDate: $('#input-ssip-pickupDate').val(),
            delayReason: $('#input-ssip-delayReason').val(),
            dtmCostCenter: $('#input-dtm-costCenter').val(),
            dtmRemark: $('#input-dtm-remark').val(),
            quantityTypeCartons: cartonsVAL,
            quantityTypePallets: palletsVAL,
            quantityTypeCages: cagesVAL,
            quantityCartons: qc1,
            quantityPallets: qc2,
            quantityCages: qc3,
            company: $('#input-ssip-company').val(),
            bookingDate: $('#input-ssip-bookingDate').val(),
            bookingTime: $('#input-ssip-bookingTime').val(),
            originPoint: $('#input-ssip-originPoint').val(),
            destinationPoint: $('#input-ssip-destinationPoint').val(),
            originZone: $('#input-ssip-originZone').val(),
            destinationZone: $('#input-ssip-destinationZone').val(),
            adhoc:  vxlDONO,
            pickupTime: $('#input-ssip-pickupTime').val(),
            truckPlateNo: $('#input-ssip-truckPlateNo').val(),
            driverName: $('#input-ssip-driverName').val(),
            driverName2: secondDrv,
            urgentMemo: $('#input-ssip-urgentMemo').val(),
            loadCap: $('#input-ssip-loadCap').val(),
            tripOnTime: $('#input-ssip-tripOnTime').val(),
            dtmRequestBy: $('#input-dtm-requestBy').val(),
            isConsol:isConsol
        };

    if(addOrEdit == 2)
    {
        map.f = $('#input-ssip-f').val();
        map.originDockTime = $('#input-ssip-originDockTime').val();
        map.originCompleteLoadingTime= $('#input-ssip-originCompleteLoadingTime').val();
        map.originDepartureTimeFromPost= $('#input-ssip-originDepartureTimeFromPost').val();
        map.destArrivalTimeAtPost= $('#input-ssip-destArrivalTimeAtPost').val();
        map.destDockTime= $('#input-ssip-destDockTime').val();
        map.destCompleteUnloadTime= $('#input-ssip-destCompleteUnloadTime').val();
        map.destDepartTimeFromPost= $('#input-ssip-destDepartTimeFromPost').val();

        
        if(aa == true){map.lockupf = $('#lockup-f').val();}
        if(ab == true){map.lockupdockTime = $('#lockup-dockTime').val();}
        if(ac == true){map.lockupcompleteLoadingTime = $('#lockup-completeLoadingTime').val();}
        if(ad == true){map.lockupdepartureTimeFromPost = $('#lockup-originDepartureTimeFromPost').val();}
        if(ae == true){map.lockuparrivalTimeAtPost = $('#lockup-arrivalTimeAtPost').val();}
        if(af == true){map.lockupdestinationDockTime = $('#lockup-destinationDockTime').val();}
        if(ag == true){map.lockupcompleteUnloadTime = $('#lockup-completeUnloadTime').val();}
        if(ah == true){map.lockupdepartTimeFromPost = $('#lockup-departTimeFromPost').val();}

        if(aa == false && ab == false && ac == false && ad == false && ae == false && af == false && ag == false&& ah == false)
        {
            if(!validateDeliveryTime(map.f,map.originDockTime,map.originCompleteLoadingTime,map.originDepartureTimeFromPost)){
                generalModalForDeliveryTimeError("Some error occurred in \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        
            if(!validateDeliveryTime(map.destArrivalTimeAtPost,map.destDockTime,map.destCompleteUnloadTime,map.destDepartTimeFromPost)){
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" section\nThe time cannot be less than the previous field");
                return;
            }
            if(!validateDeliveryTimeOriginAndDest(map.originDepartureTimeFromPost, map.destArrivalTimeAtPost)){
                generalModalForDeliveryTimeError("Some error occurred in \"Origin Departure Time\"  and \"Destination Arrival Time\" \nThe time cannot be less than the previous field");
                return;
            }
        }
        
        if(aa == true && ab == true)
        {
            if(!validateDeliveryDateAndTime(map.f,map.originDockTime,$('#lockup-f').val(),$('#lockup-dockTime').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        } 
        if(ab == true && ac == true)
        {
            if(!validateDeliveryDateAndTime(map.originDockTime,map.originCompleteLoadingTime,$('#lockup-dockTime').val(),$('#lockup-completeLoadingTime').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        } 
        if(ac == true && ad == true)
        {
            if(!validateDeliveryDateAndTime(map.originCompleteLoadingTime,map.originDepartureTimeFromPost,$('#lockup-completeLoadingTime').val(),$('#lockup-originDepartureTimeFromPost').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        } 
        if(ad == true && ae == true)
        {
            if(!validateDeliveryDateAndTime(map.originDepartureTimeFromPost,map.destArrivalTimeAtPost,$('#lockup-originDepartureTimeFromPost').val(),$('#lockup-arrivalTimeAtPost').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        } 
        if(ae == true && af == true)
        {
            if(!validateDeliveryDateAndTime(map.destArrivalTimeAtPost,map.destDockTime,$('#lockup-arrivalTimeAtPost').val(),$('#lockup-destinationDockTime').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        } 
        if(af == true && ag == true)
        {
            if(!validateDeliveryDateAndTime(map.destDockTime,map.destCompleteUnloadTime,$('#lockup-destinationDockTime').val(),$('#lockup-completeUnloadTime').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        } 
        if(ag == true && ah == true)
        {
            if(!validateDeliveryDateAndTime(map.destCompleteUnloadTime,map.destDepartTimeFromPost,$('#lockup-completeUnloadTime').val(),$('#lockup-departTimeFromPost').val()))
            {
                generalModalForDeliveryTimeError("Some error occurred in \"Destination\" AND \"Origin\" section\nThe time cannot be less than the previous field");
                return;
            }
        }
        
    }
 
    if(error == 0)
    {
        if(addOrEdit == 1)
        {
            ajaxAddDTMData(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddDTMData(map);
        }
    }
    else
    {
        generalModal(message,2,8);
    }    
}
function validateDeliveryDateAndTime(mapObject1,mapObject2,lockDate1,lockDate2)
{
    if(lockDate1 && lockDate2)
    {
        let time1 = parseInt(mapObject1.split(":")[0] + mapObject1.split(":")[1]);
        let time2 = parseInt(mapObject2.split(":")[0] + mapObject2.split(":")[1]);

        let lockupYear1 = parseInt(lockDate1.slice(0,4));
        let lockupMonth1 = parseInt(lockDate1.slice(4,6));
        let lockupDay1 = parseInt(lockDate1.slice(6,8));

        let lockupYear2 = parseInt(lockDate2.slice(0,4));
        let lockupMonth2 = parseInt(lockDate2.slice(4,6));
        let lockupDay2 = parseInt(lockDate2.slice(6,8));

        console.log(lockupDay1+"/"+lockupMonth1+"/"+lockupYear1);
        // console.log(lockupMonth1);
        // console.log(lockupDay1);

        console.log(lockupDay2+"/"+lockupMonth2+"/"+lockupYear2);
        // console.log(lockupYear2);
        // console.log(lockupMonth2);
        // console.log(lockupDay2);
        
        if(lockupYear1 <= lockupYear2)
        {
            if(lockupMonth1 == lockupMonth2)
            {
                if(lockupDay1 == lockupDay2)
                {
                    if(!isNaN(time1) && !isNaN(time2))
                    {
                        if( time1 <= time2)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else if(lockupDay1 < lockupDay2)
                {
                    if(!isNaN(time1) && !isNaN(time2))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else if(lockupMonth1 < lockupMonth2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }



    }
    else
    {
       return false;
    }
}
function checkChekedCheckboxInDTM(bool1,bool2,error)
{
    if(bool1 == true && bool2 == false)
    {
        error++;
    }
    return error;
}
function validateLockupDate(type,lockDate1,lockDate2,error)
{
    if(lockDate1 && lockDate2)
    {
        let lockupYear1 = parseInt(lockDate1.slice(0,4));
        let lockupMonth1 = parseInt(lockDate1.slice(4,6));
        let lockupDay1 = parseInt(lockDate1.slice(6,8));

        let lockupYear2 = parseInt(lockDate2.slice(0,4));
        let lockupMonth2 = parseInt(lockDate2.slice(4,6));
        let lockupDay2 = parseInt(lockDate2.slice(6,8));

        console.log(lockupYear1);
        console.log(lockupMonth1);
        console.log(lockupDay1);

        console.log(lockupYear2);
        console.log(lockupMonth2);
        console.log(lockupDay2);
        
        if(type == 0)
        {
            // if(lockupYear1 <= lockupYear2)
            // {
            //     console.log(error+" 1");
            //     if(lockupMonth1 <= lockupMonth2)
            //     {
            //         console.log(error+" 2");
            //         if(lockupDay1 < lockupDay2)
            //         {
            //             console.log(error+" 3"); 
            //         }
            //         else
            //         {
            //             console.log(error+" 4");
            //             error++;
            //         }
            //     }
            //     else
            //     {
            //         console.log(error+" 5");
            //         error++;
            //     }
            // }
            // else
            // {
            //     console.log(error+" 6");
            //     error++;
            // }
        }
        else
        {
            // if(lockupYear1 < lockupYear2)
            // {
            //     if(lockupMonth1 < lockupMonth2)
            //     {
                    
            //     }
            //     else
            //     {
            //         if(lockupMonth1 == lockupMonth2)
            //         {
                        
            //             if(lockupDay1 <= lockupDay2)
            //             {
                            
            //             }
            //             else
            //             {
            //                 error++;
            //             }
            //         }
            //         else
            //         {
            //             error++;
            //         }
            //     }
            // }
            // else
            // {
            //     if(lockupYear1 == lockupYear2)
            //     {
            //         if(lockupMonth1 < lockupMonth2)
            //         {
                        
            //         }
            //         else
            //         {
            //             if(lockupMonth1 == lockupMonth2)
            //             {
                            
            //                 if(lockupDay1 <= lockupDay2)
            //                 {
                                
            //                 }
            //                 else
            //                 {
            //                     error++;
            //                 }
            //             }
            //             else
            //             {
            //                 error++;
            //             }
            //         }
            //     }
            //     else
            //     {
            //         error++;
            //     }
            // }
        }
    }
    else
    {
        // error++;
    }
    return error;
}

function validateDeliveryTimeOriginAndDest(timeStr1, timeStr2){
    try{
        let time1 = parseInt(timeStr1.split(":")[0] + timeStr1.split(":")[1]);
        let time2 = parseInt(timeStr2.split(":")[0] + timeStr2.split(":")[1]);

        if(isNaN(time1) || isNaN(time2) || time2 < time1){
            return false;
        }else{
            return true;
        }
    }catch (e){
        console.error(e);
        return false;
    }
}
function validateDeliveryTime(timeStr1, timeStr2, timeStr3, timeStr4){
    try{
        let time1 = parseInt(timeStr1.split(":")[0] + timeStr1.split(":")[1]);
        let time2 = parseInt(timeStr2.split(":")[0] + timeStr2.split(":")[1]);
        let time3 = parseInt(timeStr3.split(":")[0] + timeStr3.split(":")[1]);
        let time4 = parseInt(timeStr4.split(":")[0] + timeStr4.split(":")[1]);

        if(isNaN(time1) || isNaN(time2) || isNaN(time3) || isNaN(time4) || time4 < time3 || time3 < time2 || time2 < time1){
            return false;
        }else{
            return true;
        }
    }catch (e){
        console.error(e);
        return false;
    }
}

function generalModalForDeliveryTimeError(msg)
{
    let header = document.getElementById('noticeHeader');
    let text = document.getElementById('noticeText');
    let modal = document.getElementById('noticeModal');
    let span = document.getElementsByClassName("closeNoticeModal")[0];

    header.innerHTML = "Error ";
    text.innerHTML = msg;

    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    };

    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
}

function generalModal(message,type,fromPage)
{
    let header = document.getElementById('noticeHeader');
    let text = document.getElementById('noticeText');
    let modal = document.getElementById('noticeModal');
    let span = document.getElementsByClassName("closeNoticeModal")[0];

    if(type == 1)// 1 for success add
    {
        header.innerHTML = "Success ";
        text.innerHTML = "The data is successfully entered! ";
        if(fromPage == 8)
        {
            text.innerHTML = "DTM successfully Inserted! ";
            window.location.replace("dtmHome.php");
        }
        if(fromPage == 9)
        {
            text.innerHTML = "Driver service fee successfully recorded! ";
            window.location.replace("dsfHome.php");
        }
        if(fromPage == 20)
        {
            text.innerHTML = "Transport charge is added! ";
            window.location.replace("transportChargesHome.php");
        }
        if(fromPage == 21)
        {
            text.innerHTML = "Invoice is added! ";
            window.location.replace("transportChargeView.php");
        }
        if(fromPage == 22)
        {
            text.innerHTML = "Invoice is updated! ";
            window.location.replace("invoiceView.php");
        }
        if(fromPage == 31)
        {
            text.innerHTML = "Service Fee is added! ";
            window.location.replace("financeHome.php");
        }
        if(fromPage == 32)
        {
            text.innerHTML = "Service Fee is added! ";
            window.location.replace("dsfServiceView.php");
        }
        if(fromPage == 33)
        {
            text.innerHTML = "Service Fee is added! ";
            window.location.replace("financeHome.php");
        }
    }
    if(type == 3)// 3 for success edit
    {
        header.innerHTML = "Success ";
        text.innerHTML = "The data is successfully updated! ";

        if(fromPage >= 1 && fromPage <=4)
        {
            window.location.replace("CDHome.php");
        }
        if(fromPage == 5)
        {
            window.location.replace("usersHome.php");
        }
        if(fromPage == 6)
        {
            window.location.replace("driverHome.php");
        }
        if(fromPage == 7)
        {
            window.location.replace("truckHome.php");
        }
        if(fromPage == 8)
        {
            window.location.replace("dtmHome.php");
        }
        if(fromPage == 9)
        {
            window.location.replace("dsfHome.php");
        }
        if(fromPage == 20)
        {
            window.location.replace("transportChargeView.php");
        }
        if(fromPage == 32)
        {
             window.location.replace("dsfServiceView.php");
        }
    }
    if(type == 2)// 2 for error
    {
        header.innerHTML = "Error ";
        if(fromPage == 8)
        {
            text.innerHTML = "Please fill in all the data! <br>"+message;
            // window.location.replace("dtmHome.php");
        }
        else
        {
            text.innerHTML = "Please fill in all the data! ";
        }
    }

    modal.style.display = "block";

    span.onclick = function() {
    modal.style.display = "none";
    }

    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    }
}
function ajaxAddDTMData(map)
{
    $.ajax({
        method: "POST",
        url: "CDFormHandler.php",
        data:map
        })
        .done(function(message) 
        {
            console.log(message);
            incrementCounter(map.fromPage);
            if(map.addOrEdit == 1)
            {
                generalModal("",1,map.fromPage);
            }
            else
            {
                generalModal("",3,map.fromPage);
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function ajaxAddTransportRates(map)
{
    $.ajax({
        method: "POST",
        url: "CDFormHandler.php",
        data:map
        })
        .done(function(message) 
        {
            console.log(message);
            incrementCounter(20);

            if(map.addOrEdit == 1)
            {
                generalModal("",1,map.fromPage);
            }
            else
            {
                generalModal("",3,map.fromPage);
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function ajaxAddCoordinatorData(map)
{
    $.ajax({
        method: "POST",
        url: "CDFormHandler.php",
        data:map
        })
        .done(function(message) 
        {
            console.log(message);
            incrementCounter(map.fromPage);

            if(map.addOrEdit == 1)
            {
                generalModal("",1,map.fromPage);
            }
            else
            {
                generalModal("",3,map.fromPage);
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function ajaxAddHRData(map)
{
    $.ajax({
        method: "POST",
        url: "CDFormHandler.php",
        data:map
        })
        .done(function(message) 
        {
            console.log(message);
            incrementCounter(map.fromPage);

            if(map.addOrEdit == 1)
            {
                generalModal("",1,map.fromPage);
            }
            else
            {
                generalModal("",3,map.fromPage);
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function ajaxDeleteCoordinatorData(fromPage,idpk)
{
    $.ajax({
        method: "POST",
        url: "CDFormHandler.php",
        data:
        {
            fromPage:fromPage,
            idpk:idpk,
            addOrEdit:3
        }
        })
        .done(function(message) 
        {
            console.log(message);
            decrementCounter(fromPage);
            
            let header = document.getElementById('noticeHeader'+fromPage);
            let text = document.getElementById('noticeText'+fromPage);
            let modal = document.getElementById('noticeModal'+fromPage);
            let span = document.getElementsByClassName("closeNoticeModal"+fromPage)[0];

            header.innerHTML = "Success ";
            text.innerHTML = "The data is successfully deleted! ";
            modal.style.display = "block";

            span.onclick = function() {
            modal.style.display = "none";
            }
        
            window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function ajaxDeleteHRData(fromPage,idpk)
{
    $.ajax({
        method: "POST",
        url: "CDFormHandler.php",
        data:
        {
            fromPage:fromPage,
            idpk:idpk,
            addOrEdit:3
        }
        })
        .done(function(message) 
        {
            console.log(message);
            decrementCounter(fromPage);
            
            let header = document.getElementById('noticeHeader'+fromPage);
            let text = document.getElementById('noticeText'+fromPage);
            let modal = document.getElementById('noticeModal'+fromPage);
            let span = document.getElementsByClassName("closeNoticeModal"+fromPage)[0];

            header.innerHTML = "Success ";
            text.innerHTML = "The data is successfully deleted! ";
            modal.style.display = "block";

            span.onclick = function() {
            modal.style.display = "none";
            }
        
            window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function getOtherPlaces(originID)
{
    $.ajax({
        method: "POST",
        url: "getOriginHandler.php",
        data:
        {
            originID:originID
        }
        })
        .done(function(message) 
        {
            $('#showOriginZones').html(message);
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function getOtherCapacity(companyID)
{
    $.ajax({
        method: "POST",
        url: "getCompanyHandler.php",
        data:
        {
            companyID:companyID
        }
        })
        .done(function(message) 
        {
            $('#showgetLoadCap').html(message);
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function reasonForLate() 
{

    let hideElem = document.getElementById('hideThis');
    let isOnTime = $('#input-ssip-tripOnTime').val();
    if(isOnTime === "yes" || !isOnTime ) {
        hideElem.style.display = "none";
        hideElem.value = "";
    }
    else {
        hideElem.style.display = "block";
    }
}
function checkProperties(obj) 
{
    for (var key in obj) 
    {
        if (obj[key] !== null && obj[key] != "")
        {
            console.log(obj[key]);
            return false;
        }
    }
    return true;
}
function changePassword()
{

    let header = document.getElementById('noticeHeader');
    let text = document.getElementById('noticeText');
    let modal = document.getElementById('noticeModal');
    let span = document.getElementsByClassName("closeNoticeModal")[0];
    let error = 0;
    let specError = "";
    let currPass = $('#changeOldPassword').val();
    let newPass =$('#changeNewPassword').val();
    let comparePass =$('#checkNewPassword').val();

    error = validateTextField(currPass,error);
    error = validateTextField(newPass,error);
    error = validateTextField(comparePass,error);

    if(comparePass != newPass)
    {
        error+=1 ;
        specError+="Password Doesn`t Match!<br>";
    }
    
    if(error == 0)
    {
        $.ajax({
            method: "POST",
            url: "settingFormHandler.php",
            data:
            {
                currPass:currPass,
                newPass:newPass,
                comparePass:comparePass
            }
            })
            .fail(function() 
            {
                alert( "error" );
            });
    }
    else
    {
        

        header.innerHTML = "Error! ";
        text.innerHTML = "Please fill all password credentials!<br>"+specError;

        modal.style.display = "block";

        span.onclick = function() {
        modal.style.display = "none";
        }
    
        window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        }
    }
}
function addSerFee(idPK,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";


        if(addOrEdit == 1)
        {
            message += "add Service Fee ?";
        }
        else
        {
            message += "edit Service Fee ?";
        }

    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";

        addEditServiceFeeDriver2(addOrEdit,idPK);
    }
}
function addTransportCharge(idPK,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";


        if(addOrEdit == 1)
        {
            message += "add this transport charges ?";
        }
        else
        {
            message += "edit transport charges ?";
        }

    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";

        addEditTransportCharges(addOrEdit,idPK);
    }
}
function payToThisDriver(nickName,fromTable,addOrEdit)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";

    if(fromTable == 9)
    {
        if(addOrEdit == 1)
        {
            message += "add this service fee to "+nickName;
        }
        else
        {
            message += "edit this dtm";
        }
    }
    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";
        if(fromTable == 9)
        {
            addEditServiceFeeDriver(addOrEdit);
        }
    }
}
function addEditTransportCharges(addOrEdit,idPK)
{
    let error = 0;

    error = validateTextField($('#field_1').val(),error);
    error = validateTextField($('#field_2').val(),error);
    error = validateTextField($('#field_3').val(),error);
    error = validateTextField($('#field_4').val(),error);

    let map =
    {
        addOrEdit:addOrEdit,
        fromPage: 20,
        idPK:idPK,
        remarks: $('#field_1').val(),
        operatingHours: $('#field_2').val(),
        transportCharges: $('#field_3').val(),
        faf: $('#field_4').val(),
    };

    if(error == 0)
    {
        if(addOrEdit == 1)
        {
            map.truckID = $('#truckID').val();
            map.companyID = $('#companyID').val();
            
            // console.log(Object.keys(map).length);
            ajaxAddTransportCharge(map);
        }
        else
        {
            map.id = $('#idpk').val();
            ajaxAddTransportCharge(map);
        }
    }
    else
    {
        generalModal("",2,8);
    }    
}
function ajaxAddTransportCharge(map)
{
    $.ajax({
        method: "POST",
        url: "transportChargeFormHandler.php",
        data:map
        })
        .done(function(message) 
        {
            console.log(message);
            incrementCounter(map.fromPage);
            if(map.addOrEdit == 1)
            {
                generalModal("",1,map.fromPage);
            }
            else
            {
                generalModal("",3,map.fromPage);
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function ajaxAddDriverServiceFee(map)
{
    $.ajax({
        method: "POST",
        url: "dsfFormHandler.php",
        data:map
        })
        .done(function(message) 
        {
            console.log(message);
            incrementCounter(map.fromPage);
            if(map.addOrEdit == 1)
            {
                generalModal("",1,map.fromPage);
            }
            else
            {
                generalModal("",3,map.fromPage);
            }
       })
       .fail(function() 
       {
           alert( "error" );
       });
}
function getAllSelectedAsOneInvoice()
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to ";
    let viewIn = document.getElementsByName("tableType");
    let invoiceNO = document.getElementById("invoiceNO");
    let getcheckedArr = [];

    for(let counter = 0 ; counter < viewIn.length ; counter++ )
    {
        if(viewIn[counter].checked == true)
        {
            getcheckedArr.push(viewIn[counter].value);
        }
    }
    // alert(getcheckedArr.length);
    // console.log(getcheckedArr);
    console.log(invoiceNO.value);
    if(getcheckedArr.length > 0 && invoiceNO != null && invoiceNO.value != "")
    {
        message += "add an invoice ?";
        header.innerHTML = "Confirmation !";
        text.innerHTML = message;
        modal.style.display = "block";

        span.onclick = function() 
        {
            modal.style.display = "none";
        }
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
            }
        }
        confirm.style.display = "block";
        cancel.style.display = "block";
        cancel.onclick = function() 
        {
            modal.style.display = "none";
        }
        confirm.onclick = function() 
        {
            modal.style.display = "none";
            
            let map =
            {
                addOrEdit:1,
                fromPage:21,
                getcheckedArr: getcheckedArr,
                invoiceNO:invoiceNO.value
            };

            passAjaxInvoice(map);
            
        }
    }
    else
    {
        message = "Please check the selection below AND insert an invoice no !";
        header.innerHTML = "Notice !";
        text.innerHTML = message;
        modal.style.display = "block";

        span.onclick = function() 
        {
            modal.style.display = "none";
        }
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
            }
        }
        confirm.style.display = "none";
        cancel.style.display = "none";
    }
    
}
function passAjaxInvoice(map)
{
    // let db = initializeFireBase();
    // let refDB = db.collection(URL_INVOICE);

    // refDB.onSnapshot(function(doc) 
    // {
        $.ajax({
            method: "POST",
            url: "transportChargeFormHandler.php",
            data:map
            })
            .done(function(message) 
            {
                console.log(message);
                incrementCounter(map.fromPage);
                if(map.addOrEdit == 1)
                {
                    generalModal("",1,map.fromPage);
                }
                else
                {
                    generalModal("",3,map.fromPage);
                }
            })
            .fail(function() 
            {
                alert( "error" );
            });
    // });
}
function confirmFinishedDTM(checkbox)
{
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure that ";
            
    if(checkbox.checked == true)
    {
        message += "this dtm is finished?";

        header.innerHTML = "Confirmation !";
        text.innerHTML = message;
        modal.style.display = "block";
    
        span.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            return false;
        }
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
                checkbox.checked = false;
                return false;
            }
        }
        cancel.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            return false;
        }
        confirm.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = true;
            
            let map =
            {
                change:1,
                id:checkbox.value,
                fromPage:8
            };
    
            $.ajax({
                method: "POST",
                url: "dtmIsfinishedFormHandler.php",
                data:map
                })
                .done(function(message) 
                {
                    console.log(message);
                    putjsNotice("Notice",message);
                    incrementCounter(map.fromPage);
                    incrementCounter(20);
                    incrementCounter(32);
                })
                .fail(function() 
                {
                    alert( "error" );
                });
        }
    }
    else
    {
        message += "this dtm is not correct?";

        header.innerHTML = "Confirmation !";
        text.innerHTML = message;
        modal.style.display = "block";
    
        span.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = true;
            return false;
        }
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
                checkbox.checked = true;
                return false;
            }
        }
        cancel.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = true;
            return false;
        }
        confirm.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            
            let map =
            {
                change:0,
                id:checkbox.value,
                fromPage:8
            };
    
            $.ajax({
                method: "POST",
                url: "dtmIsfinishedFormHandler.php",
                data:map
                })
                .done(function(message) 
                {
                    console.log(message);
                    incrementCounter(map.fromPage);
                    incrementCounter(20);
                    incrementCounter(32);
                })
                .fail(function() 
                {
                    alert( "error" );
                });
        }
    }
}
function confirmFinishedFee(checkbox)
{

    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure that ";
    
    if(checkbox.checked == true)
    {
        message += "this sevice is finished?";

        header.innerHTML = "Confirmation !";
        text.innerHTML = message;
        modal.style.display = "block";

        span.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            return false;
        }
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
                checkbox.checked = false;
                return false;
            }
        }
        cancel.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            return false;
        }
        confirm.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = true;
            
            let map =
            {
                change:1,
                id:checkbox.value,
                fromPage:33
            };

            $.ajax({
                method: "POST",
                url: "checkedfeeHandler.php",
                data:map
                })
                .done(function(message) 
                {
                    console.log(message);
                    incrementCounter(map.fromPage);
                })
                .fail(function() 
                {
                    alert( "error" );
                });
        }
    }
    else
    {
        message += "this sevice is not finished/correct?";

        header.innerHTML = "Confirmation !";
        text.innerHTML = message;
        modal.style.display = "block";

        span.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            return false;
        }
        window.onclick = function(event) 
        {
            if (event.target == modal) 
            {
                modal.style.display = "none";
                checkbox.checked = false;
                return false;
            }
        }
        cancel.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            return false;
        }
        confirm.onclick = function() 
        {
            modal.style.display = "none";
            checkbox.checked = false;
            
            let map =
            {
                change:0,
                id:checkbox.value,
                fromPage:33
            };

            $.ajax({
                method: "POST",
                url: "checkedfeeHandler.php",
                data:map
                })
                .done(function(message) 
                {
                    console.log(message);
                    incrementCounter(map.fromPage);
                })
                .fail(function() 
                {
                    alert( "error" );
                });
        }
    }

}
function handleChangeDTM(checkbox) 
{
    if(confirmFinishedDTM(checkbox))
    {

    }
    else
    {
        checkbox.checked = false;
    }
}
function handleServiceFee(checkbox) {
    if(confirmFinishedFee(checkbox))
    {

    }
    else
    {
        checkbox.checked = false;
    }
}
function handleChange(checkbox) {
    if(checkbox.checked == true)
    {
        let map =
        {
            change:1,
            id:checkbox.value,
            fromPage:21
        };

        $.ajax({
            method: "POST",
            url: "invoiceFormHandler.php",
            data:map
            })
            .done(function(message) 
            {
                console.log(message);
                incrementCounter(map.fromPage);
            })
            .fail(function() 
            {
                alert( "error" );
            });
    }
    else
    {
        let map =
        {
            change:0,
            id:checkbox.value,
            fromPage:21
        };

        $.ajax({
            method: "POST",
            url: "invoiceFormHandler.php",
            data:map
            })
            .done(function(message) 
            {
                console.log(message);
                incrementCounter(map.fromPage);
            })
            .fail(function() 
            {
                alert( "error" );
            });
    }
}
function ajaxShowSalesReport(fromPage)
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_TRANSPORT_CHARGES);
    let pageName = 'salesReportShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            $('#showSalesReport').html(message);
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function clickThisSales(fromPage)
{
    let error = 0;

    error = validateTextField($('#month').val(),error);
    error = validateTextField($('#year').val(),error);

    let map =
        {
            month:$('#month').val(),
            year:$('#year').val()
        };

    $.ajax({
        method: "POST",
        url: "salesReportShowTable.php",
        data:map
        })
        .done(function(message) 
        {
            $('#getSales').html(message);
            console.log(message);
        })
        .fail(function() 
        {
            alert( "error" );
        });
}
function ajaxShowRevenue(fromPage)
{
    let db = initializeFireBase();
    let refDB = db.collection(URL_TRANSPORT_CHARGES);
    let pageName = 'salesRevenueShow.php';

    refDB.onSnapshot(function(doc) 
    {
        $.ajax({
            url: pageName,
            type: 'POST',
            async: false,
            data:
            {
                fromPage: fromPage
            }
        })
        .done(function(message) 
        {
            $('#showSalesRevenue').html(message);
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
    });
}
function clickThisRevenue(fromPage)
{
    let error = 0;

    error = validateTextField($('#month').val(),error);
    error = validateTextField($('#year').val(),error);

    let map =
        {
            month:$('#month').val(),
            year:$('#year').val()
        };

    $.ajax({
        method: "POST",
        url: "salesRevenueShowTable.php",
        data:map
        })
        .done(function(message) 
        {
            $('#getRevenue').html(message);
            console.log(message);
        })
        .fail(function() 
        {
            alert( "error" );
        });
}
function detentionCharges(grossDSF,dt)
{
    getAllCheckBox(grossDSF,dt,document.getElementById('dsfOvernight')
    ,document.getElementById('dsfNightDelivery')
    ,document.getElementById('dsfSunday')
    ,document.getElementById('dsfPublic')
    ,document.getElementById('dsfCancellation')
    ,document.getElementById('dsfLockupDay')
    ,document.getElementById('dsfLockupNight'));
}
function dsfOptions(grossDSF,typeValue,checkBox,option)
{
    if(checkBox.checked == true)
    {
        option.value = typeValue;
        option.disabled = false;
        getAllCheckBox(grossDSF,document.getElementById('detentioncharge')
                            ,document.getElementById('dsfOvernight')
                            ,document.getElementById('dsfNightDelivery')
                            ,document.getElementById('dsfSunday')
                            ,document.getElementById('dsfPublic')
                            ,document.getElementById('dsfCancellation')
                            ,document.getElementById('dsfLockupDay')
                            ,document.getElementById('dsfLockupNight'));
    }
    else
    {
        option.value = 0;
        option.disabled = true;
        getAllCheckBox(grossDSF,document.getElementById('detentioncharge')
                            ,document.getElementById('dsfOvernight')
                            ,document.getElementById('dsfNightDelivery')
                            ,document.getElementById('dsfSunday')
                            ,document.getElementById('dsfPublic')
                            ,document.getElementById('dsfCancellation')
                            ,document.getElementById('dsfLockupDay')
                            ,document.getElementById('dsfLockupNight'));
    }
}
function getAllCheckBox(grossDSF,detention,a,b,c,d,e,f,g)
{
    let total_dsf = 0;

    total_dsf += parseFloat(grossDSF);
    if(detention.value)
    {
        total_dsf += parseFloat(detention.value);
    }
    else
    {
        total_dsf += 0;
    }
    
    total_dsf += parseFloat(getEachCheckBox(a));
    total_dsf += parseFloat(getEachCheckBox(b));
    total_dsf += parseFloat(getEachCheckBox(c));
    total_dsf += parseFloat(getEachCheckBox(d));
    total_dsf += parseFloat(getEachCheckBox(e));
    total_dsf += parseFloat(getEachCheckBox(f));
    total_dsf += parseFloat(getEachCheckBox(g));

    document.getElementById('option8').innerHTML ="$ "+toFixedNumber(total_dsf);
}
function getEachCheckBox(checkbox)
{
    if(checkbox.value)
    { 
        return parseFloat(checkbox.value);
    }
    else
    {
        return 0;
    }
}
function toFixedNumber(number) {
    const spitedValues = String(number.toLocaleString()).split('.');
    let decimalValue = spitedValues.length > 1 ? spitedValues[1] : '';
    decimalValue = decimalValue.concat('00').substr(0,2);

    return ''+spitedValues[0] + '.' + decimalValue;
}
function putjsNotice(title,paragraph)
{
    var header = title;
    var text = paragraph;

    document.getElementById('noticeHeader').innerHTML = header;
    document.getElementById('noticeText').innerHTML = text;
    
    // Get the modal
    var modal = document.getElementById('noticeModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName('closeNoticeModal')[0];

    modal.style.display = 'block';
    

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = 'none';
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
    }
}
function checkInvoicePrint()
{
    
    let from = document.getElementById('fromDate');
    let to = document.getElementById('toDate');
    let company = document.getElementById('company');
    let costcenter = document.getElementById('input-dtm-costCenter');

    let error = 0;

    error = validateTextField($('#invoiceNo').val(),error);
    error = validateTextField($('#company').val(),error);
    error = validateTextField($('#input-dtm-costCenter').val(),error);

    if(error > 0)
    {
        putjsNotice("ERROR ","Please Enter Company, Cost Center And Invoice No");
        event.preventDefault();
    }
    else
    {
        if(from.value != null && to.value != null && from.value != "" && to.value != "")
        {
            if(!isNaN(from.value) && !isNaN(to.value))
            {
                error = validateLockupDate(1,from.value,to.value,error);
                if(error != 0)
                {
                    // error
                    putjsNotice("ERROR ","Date to must be more than date from");
                    event.preventDefault();
                }
                else
                {
                    
                }
            }
            else
            {
                // error
                console.log(isNaN(from));
                putjsNotice("ERROR ","The dates is empty/not a number");
                event.preventDefault();
            }
          
        }
        else
        {
            putjsNotice("ERROR ","Please Enter the dates");
            event.preventDefault();
            //error
        }
    }
}
function checkDTMDATE()
{
    let from = document.getElementById('fromDate');
    let to = document.getElementById('toDate');
    let error = 0;

    if(from.value != null && to.value != null && from.value != "" && to.value != "")
    {
        if(!isNaN(from.value) && !isNaN(to.value))
        {
            error = validateLockupDate(1,from.value,to.value,error);
            if(error != 0)
            {
                // error
                putjsNotice("ERROR ","Date to must be more than date from");
                event.preventDefault();
            }
            else
            {
                
            }
        }
        else
        {
            // error
            console.log(isNaN(from));
            putjsNotice("ERROR ","The dates is empty/not a number");
            event.preventDefault();
        }
      
    }
    else
    {
        putjsNotice("ERROR ","Please Enter the dates");
        event.preventDefault();
        //error
    }
}
function checkDSFDate()
{
    let from = document.getElementById('fromDate');
    let to = document.getElementById('toDate');
    let error = 0;

    if(from.value != null && to.value != null && from.value != "" && to.value != "")
    {
        if(!isNaN(from.value) && !isNaN(to.value))
        {
            error = validateLockupDate(1,from.value,to.value,error);
            if(error != 0)
            {
                // error
                putjsNotice("ERROR ","Date to must be more than date from");
                event.preventDefault();
            }
            else
            {
                
            }
        }
        else
        {
            // error
            console.log(isNaN(from));
            putjsNotice("ERROR ","The dates is empty/not a number");
            event.preventDefault();
        }
      
    }
    else
    {
        putjsNotice("ERROR ","Please Enter the dates");
        event.preventDefault();
        //error
    }
}
function getThisCompanyToChangeCostCenter(getSelectObject,type,costID,formType)
{
    let valueType;

    if(type == 1)
    {
        valueType = getSelectObject.value;
    }
    else
    {
        valueType = getSelectObject.options[getSelectObject.selectedIndex].value;
    }


    $.ajax({
        method: "POST",
        url: "showCostcenterByCompany.php",
        data:{
            companyID: valueType,
            type:type,
            costID:costID,
            formType:formType
        }
        })
        .done(function(message) 
        {
            if(formType == 1)
            {
                $('#showCostCenterByCompany').html(message);
            }
            else if(formType == 2)
            {
                $('#showCostCenterByCompany1').html(message);
            }            
            
            // console.log(valueType);
        })
        .fail(function() 
        {
            alert( "error" );
        });
}
function updateInvoicing(invoiceID)
{
    let error = 0;

    let field_1 = $('#field_1').val();
    let field_2 = $('#field_2').val();
    let field_3 = $('#field_3').val();
    let field_4 = $('#field_4').val();
    let field_5 = $('#field_5').val();

    // console.log(field_1);
    // console.log(field_2);
    // console.log(field_3);
    // console.log(field_4);
    // console.log(field_5);

    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    let header = document.getElementById('header');
    let confirm = document.getElementById('confirm');
    let cancel = document.getElementById('cancel');
    let text = document.getElementById('text');
    let message = "Are you sure to edit this invoice charges ?";


    header.innerHTML = "Confirmation !";
    text.innerHTML = message;
    modal.style.display = "block";

    span.onclick = function() 
    {
        modal.style.display = "none";
    }
    window.onclick = function(event) 
    {
        if (event.target == modal) 
        {
            modal.style.display = "none";
        }
    }
    cancel.onclick = function() 
    {
        modal.style.display = "none";
    }
    confirm.onclick = function() 
    {
        modal.style.display = "none";

        error = validateTextField(field_1,error);
        error = validateTextField(field_2,error);
        error = validateTextField(field_3,error);
        error = validateTextField(field_4,error);
        error = validateTextField(field_5,error);

        if(error == 0)
        {
            let map = 
            {
                invoiceID_PK:invoiceID,
                transportRemarks: field_1,
                operatingHours: field_2,
                transportCharges: field_3,
                faf: field_4,
                invoiceNo: field_5
            };

            $.ajax({
            method: "POST",
            url: "transportEditFormHandler.php",
            data:map
            })
            .done(function(message) 
            {
                console.log(message);
                incrementCounter(22);
                generalModal("",1,22);
            })
            .fail(function() 
            {
                alert( "error" );
            });
        }
        else
        {

        }
    }
}
function checkAllCheckBox(thisChk)
{
    let viewIn = document.getElementsByName("tableType");
    if(thisChk.value == 0)
    {
        for(let counter = 0 ; counter < viewIn.length ; counter++ )
        {
            viewIn[counter].checked = true;
        }
        thisChk.value = 1;
        thisChk.innerHTML="Unselect All Invoice";
    }
    else
    {
        for(let counter = 0 ; counter < viewIn.length ; counter++ )
        {
            viewIn[counter].checked = false;
        }
        thisChk.value = 0;
        thisChk.innerHTML="Select All Invoice";
    }
    
}