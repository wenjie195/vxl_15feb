<?php
require 'generalFunction.php';
define('NUMBER_OF_COLUMNS', 37);    

$monthThis = $_POST['month'];
$yearThis = $_POST['year'];

function renderCalenderMonth($date,$monthThis,$yearThis) 
{

     $conn = connDB();
     $day = date('d', $date);
     $month = date('m', $date);
     $year = date('Y', $date);
     $daysInMonth = cal_days_in_month(0, $month, $year);



     $TransportChargeArray = array();
     $trips = 0;

     $sqlo = " SELECT transportcharge.transportcharge,transportcharge.faf,dtmlist.dtmPickupDate,trucks.truckPlateNo,trucks.truckCapacity,dtmlist.isConsol FROM ((transportcharge 
          INNER JOIN dtmlist ON transportcharge.dtmID_FK = dtmlist.dtmID_PK)
          INNER JOIN trucks ON transportcharge.truckID_FK = trucks.truckID_PK)
          WHERE transportcharge.isInvoiceNoAdded LIKE 1 
          AND dtmlist.dtmPickupDate >= '$year-$month-01 00:00:00' 
          AND dtmlist.dtmPickupDate <= '$year-$month-$daysInMonth 23:59:59'";
     // echo $sqlo;
          $result = mysqli_query($conn,$sqlo);

          if (mysqli_num_rows($result) > 0) 
          {
               while($row = mysqli_fetch_array($result))
               { 
                    if($row['isConsol'] == 0)
                    {
                         $trips++;
                    }
                    // $totalCharges += ();
                    $thisTrans = new TransportChargeByTrucks;
                    $thisTrans->getTransportChargeAll($row['transportcharge'],date("d",strtotime($row['dtmPickupDate'])),$row['truckPlateNo'],$row['faf'],$row['truckCapacity'],$row['isConsol']);
                    array_push($TransportChargeArray,$thisTrans);
               }
               // echo $trips;
          }
          else
          {
              
          }


  $firstDay = mktime(0,0,0,$month, 1, $year);
  $title = strftime('%B', $firstDay);
  $dayOfWeek = date('D', $firstDay);
  $grandTotal = 0; 
  $grandTripTotal = 0; 
  /* Get the name of the week days */
  $timestamp = strtotime('next Sunday');
  $weekDays = array();

  for ($i = 0; $i < NUMBER_OF_COLUMNS; $i++) {
      $weekDays[] = strftime('%a', $timestamp);
      $timestamp = strtotime('+1 day', $timestamp);
  }
  $blank = date('w', strtotime("{$year}-{$month}-01"));
  ?>
<form action="printSR.php" method="POST">
     <input type="hidden" name="monthThis" value="<?php echo $monthThis ;?>">
     <input type="hidden" name="yearThis" value="<?php echo $yearThis ;?>">
     <button class="btn btn-warning" value="<?php //echo $_POST['fromPage'];?>" name="add" style="margin: 0px -74px 16px 15px;">
          Print (Please Click Show First)
     </button>
</form>
  <table class="table table-bordered table-responsive table-hovered table-striped dtmTableNoWrap text-center" style="style=”margin:1em auto;">
	<tr>
          <th rowspan="3">Trucks Plate No</th>
          <th rowspan="3">Trucks Capacity</th>
		<th colspan="<?php echo NUMBER_OF_COLUMNS +2?>" class="text-center"> <?php echo $title ?> <?php echo $year ?> </th>
     </tr>
	<tr>
		
		<?php foreach($weekDays as $key => $weekDay) { ?>
			<td class="text-center"><?php echo $weekDay ?></td>
          <?php } ?>
               <td rowspan="2"><strong>Total Sales</strong></td>
               <td rowspan="2"><strong>Total Trip</strong></td>
	</tr>
	<tr>
          <?php 
               for($i = 0; $i < $blank; $i++)
               { 
          ?>
		<td></td>
          <?php
               } 
          ?>
		 
          <?php 
          for($i = 1; $i <= $daysInMonth; $i++)
          { 
               if($day == $i)
               { 
                    ?>
          		<td><strong><?php echo $i; ?></strong></td>
               <?php 
               }
               else
               {    
               ?>
				<td><?php echo $i;?></td>
               <?php 
               } 
               if(($i + $blank) % NUMBER_OF_COLUMNS == 0)
               { 
               ?>
	</tr>
	<tr>
               <?php 
               } 
          }  
          for($i = 0; ($i + $blank + $daysInMonth) % NUMBER_OF_COLUMNS != 0; $i++)
          { ?>
			<td></td>
          <?php 
          } 
          ?>
      </tr>
      <?php
         
         $tripArray = array();
         $tripX = array();         
         $chargesArray = array();         

          $costCenterDisplay = "SELECT truckPlateNo,truckCapacity FROM trucks ORDER BY truckCapacity ASC ,truckPlateNo ASC";
          $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
          if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
          {
               while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
               {
                    
                    $total = 0;
                    $tripTotal = 0;
                    echo "<td>".$urow1['truckPlateNo']."</td>";
                    echo "<td>".$urow1['truckCapacity']."</td>";
                    ?>
                         <?php 
                              for($i = 0; $i < $blank; $i++)
                              { 
                         ?>
                         <td></td>
                         <?php
                              } 
                         ?>
                         
                         <?php 
                         $total = 0;
                         for($i = 1; $i <= $daysInMonth; $i++)
                         { 
                              $value = 0;
                              $tripNo = 0;
                              for($cnt = 0;$cnt < count($TransportChargeArray);$cnt++)
                              {
                                   if($TransportChargeArray[$cnt]->getDtmBookingDate() == $i 
                                   && $TransportChargeArray[$cnt]->getTruckPlateNo() == $urow1['truckPlateNo'])
                                   {
                                        $fafPercentageAfter = ($TransportChargeArray[$cnt]->getFaf() / 100) * $TransportChargeArray[$cnt]->getTransportCharge();
                                        $value += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                        $total += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                        if($TransportChargeArray[$cnt]->getIsConsol() != 1)
                                        {
                                             $tripNo++;
                                             $tripTotal++;
                                             $grandTripTotal++;
                                        }
                                        $grandTotal += ($TransportChargeArray[$cnt]->getTransportCharge() + $fafPercentageAfter);
                                   }
                              }
                              if($tripNo != 0)
                              {
                                   array_push($tripArray,$tripNo);
                                   array_push($tripX,$i);
                                   array_push($chargesArray,$value);
                                   echo "<td>$tripNo trip (<strong>RM ".sprintf('%0.2f',$value)."</strong>)</td>";
                            
                              }
                              else {
                                   echo "<td>-</td>";
                              }
                                 
                              if(($i + $blank) % NUMBER_OF_COLUMNS == 0)
                              { 
                              ?>
                    </tr>
                    <tr>
                              <?php 
                              } 
                         }  
                         for($i = 0; ($i + $blank + $daysInMonth) % NUMBER_OF_COLUMNS != 0; $i++)
                         { ?>
                              <td></td>
                         <?php 
                         } 
                         ?>
                         <td><?php echo "$".sprintf('%0.2f',$total);?></td>
                         <td><?php echo $tripTotal;?></td>
                    </tr>
                    <?php
               }
          }
      ?>
      <tr>
          <td><strong>DAILY CHARGES</strong></td>
          <td></td>
          <?php 
               for($i = 0; $i < $blank; $i++)
               { 
          ?>
		<td></td>
          <?php
               } 
          ?>
		 
          <?php 
          for($i = 1; $i <= $daysInMonth; $i++)
          { 
               $chargesDailyTrips = 0;
               for($cs = 0; $cs < count($tripX);$cs++)
               {
                    if($tripX[$cs] == $i)
                    {
                         $chargesDailyTrips += $chargesArray[$cs];
                    }
               }
               if($chargesDailyTrips != 0)
               {
               ?>
                    <td><?php echo "RM ". sprintf('%0.2f',$chargesDailyTrips);?></td>
               <?php 
               }
               else 
               {
                    ?>
                    <td><?php echo "-";?></td>
               <?php 
               }
               if(($i + $blank) % NUMBER_OF_COLUMNS == 0)
               { 
               ?>
	</tr>
	<tr>
               <?php 
               } 
          }  
          for($i = 0; ($i + $blank + $daysInMonth) % NUMBER_OF_COLUMNS != 0; $i++)
          { ?>
			<td></td>
          <?php 
          } 
          ?>
          <td><strong><?php echo "RM ".sprintf('%0.2f',$grandTotal);?></strong></td>
          <td></td>
      </tr>
      <tr>
          <td><strong>DAILY TRIP</strong></td>
          <td></td>
          <?php 
               for($i = 0; $i < $blank; $i++)
               { 
          ?>
		<td></td>
          <?php
               } 
          ?>
		 
          <?php 
          for($i = 1; $i <= $daysInMonth; $i++)
          { 
               $countDailyTrips = 0;
               for($cs = 0; $cs < count($tripX);$cs++)
               {
                    if($tripX[$cs] == $i)
                    {
                         $countDailyTrips += $tripArray[$cs];
                    }
               }
               if($countDailyTrips != 0)
               {
               ?>
                    <td><?php echo $countDailyTrips;?></td>
               <?php 
               }
               else 
               {
                    ?>
                    <td><?php echo "-";?></td>
               <?php 
               }
               if(($i + $blank) % NUMBER_OF_COLUMNS == 0)
               { 
               ?>
	</tr>
	<tr>
               <?php 
               } 
          }  
          for($i = 0; ($i + $blank + $daysInMonth) % NUMBER_OF_COLUMNS != 0; $i++)
          { ?>
			<td></td>
          <?php 
          } 
          ?>
          
          <td></td>
          <td><strong><?php echo $grandTripTotal;?></strong></td>
      </tr>
	
  </table>

  <?php
}


date_default_timezone_set("Asia/Kuala_Lumpur");

$date = strtotime(sprintf('%s-%s-01', $yearThis, $monthThis));
renderCalenderMonth($date,$monthThis,$yearThis);
?>