<?php
require 'generalFunction.php';
$conn = connDB();

$fromPage = $_POST['fromPage'];
$condition = $_POST['condition'];
$pageNo = $_POST['pageNo'];
$filter = $_POST['filter'];
$searchWord = $_POST['searchWord'];

if($filter == null)
{
    $filter= 1;
}
if($pageNo == null)
{
    $filter= 1;
}
if($searchWord == null)
{
    $searchWord = "";
}
$sqlPageNo = 0;
$sqlPageNo = ($pageNo - 1) * 10;

if($filter == 1)
{
    if($fromPage == 20)
    {
        $orderBy = "dtmlist.dtmPickupDate";
    }
    if($fromPage == 21)
    {
        $orderBy = "dtmlist.dtmPickupDate";
    }
}
if($filter == 2)
{
    if($fromPage == 20)
    {
        $orderBy = "dtmlist.dtmDateCreated";
    }
    if($fromPage == 21)
    {
        $orderBy = "transportcharge.dateCreated";
    }
}
if($filter == 3)
{
    if($fromPage == 20)
    {
        $orderBy = "company.companyName";
    }
    if($fromPage == 21)
    {
        $orderBy = "company.companyName";
    }
}
if($filter == 4)
{
    if($fromPage == 20)
    {
        $orderBy = "trucks.truckPlateNo";
    }
    if($fromPage == 21)
    {
        $orderBy = "trucks.truckPlateNo";
    }
}
if($filter == 5)
{
    if($fromPage == 20)
    {
        $orderBy = "costcenter.costCenterName";
    }
    if($fromPage == 21)
    {
        $orderBy = "costcenter.costCenterName";
    }
}
if($filter == 6)
{
    if($fromPage == 20)
    {
        $orderBy = "dtmlist.dtmID_PK";
    }
    if($fromPage == 21)
    {
        $orderBy = "dtmlist.dtmID_PK";
    }
}
if($filter == 7)
{
    if($fromPage == 20)
    {
        $orderBy = "dtmlist.dtmAdhoc";
    }
    if($fromPage == 21)
    {
        $orderBy = "dtmlist.dtmAdhoc";
    }
}

$sql = "";
$sql2 = "";

if($fromPage == 20)
{
     $sql .= " SELECT * FROM (((dtmlist 
                INNER JOIN company ON company.companyID_PK = dtmlist.companyID_FK)
                INNER JOIN trucks ON trucks.truckID_PK = dtmlist.truckID_FK)
                INNER JOIN costcenter ON costcenter.costCenterID_PK = dtmlist.costCenterID_FK)
                WHERE dtmlist.dtmIsTransportCharge IS NULL AND dtmlist.dtmIsFinished = 1 ";

     $sql2 .= " SELECT COUNT(*) AS total2 FROM (((dtmlist 
                INNER JOIN company ON company.companyID_PK = dtmlist.companyID_FK)
                INNER JOIN trucks ON trucks.truckID_PK = dtmlist.truckID_FK)
                INNER JOIN costcenter ON costcenter.costCenterID_PK = dtmlist.costCenterID_FK)
                WHERE dtmlist.dtmIsTransportCharge IS NULL AND dtmlist.dtmIsFinished = 1 ";
}

if($fromPage == 21)
{
     $sql .= " SELECT * FROM ((((transportcharge 
        INNER JOIN dtmlist ON dtmlist.dtmID_PK = transportcharge.dtmID_FK)
        INNER JOIN company ON company.companyID_PK = dtmlist.companyID_FK)
        INNER JOIN trucks ON trucks.truckID_PK = dtmlist.truckID_FK)
        INNER JOIN costcenter ON costcenter.costCenterID_PK = dtmlist.costCenterID_FK) 
        WHERE transportcharge.isInvoiceNoAdded LIKE 0 ";

     $sql2 .= " SELECT COUNT(*) AS total2 FROM ((((transportcharge 
        INNER JOIN dtmlist ON dtmlist.dtmID_PK = transportcharge.dtmID_FK)
        INNER JOIN company ON company.companyID_PK = dtmlist.companyID_FK)
        INNER JOIN trucks ON trucks.truckID_PK = dtmlist.truckID_FK)
        INNER JOIN costcenter ON costcenter.costCenterID_PK = dtmlist.costCenterID_FK) 
        WHERE transportcharge.isInvoiceNoAdded LIKE 0 ";
}

if($searchWord != null && $searchWord != "")
{
     if($filter == 1)
     {
        if($fromPage == 20)
        {
            $sql .= " AND dtmlist.dtmPickupDate LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND dtmlist.dtmPickupDate LIKE '%".$searchWord."%'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND dtmlist.dtmPickupDate LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND dtmlist.dtmPickupDate LIKE '%".$searchWord."%'  ";
        }
     }
     else if($filter == 2)
     {
        if($fromPage == 20)
        {
            $sql .= " AND dtmlist.dtmDateCreated LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND dtmlist.dtmDateCreated LIKE '%".$searchWord."%'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND dtmlist.dtmDateCreated LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND dtmlist.dtmDateCreated LIKE '%".$searchWord."%'  ";
        }
     }
     else if($filter == 3 )
     {
        if($fromPage == 20)
        {
            $sql .= " AND company.companyName LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND company.companyName LIKE '%".$searchWord."%'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND company.companyName LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND company.companyName LIKE '%".$searchWord."%'  ";
        }
     }
     else if($filter == 4)
     {
        if($fromPage == 20)
        {
            $sql .= " AND trucks.truckPlateNo LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND trucks.truckPlateNo LIKE '%".$searchWord."%'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND trucks.truckPlateNo LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND trucks.truckPlateNo LIKE '%".$searchWord."%'  ";
        }
     }
     else if($filter == 5)
     {
        if($fromPage == 20)
        {
            $sql .= " AND costcenter.costCenterName LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND costcenter.costCenterName LIKE '%".$searchWord."%'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND costcenter.costCenterName LIKE '%".$searchWord."%'  ";	
            $sql2 .= " AND costcenter.costCenterName LIKE '%".$searchWord."%'  ";
        }
     }
     else if($filter == 6)
     {
        if($fromPage == 20)
        {
            $sql .= " AND dtmlist.dtmID_PK = '".$searchWord."'  ";	
            $sql2 .= " AND dtmlist.dtmID_PK = '".$searchWord."'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND dtmlist.dtmID_PK = '".$searchWord."'  ";	
            $sql2 .= " AND dtmlist.dtmID_PK = '".$searchWord."'  ";
        }
     }
     else if($filter == 7)
     {
        if($fromPage == 20)
        {
            $sql .= " AND dtmlist.dtmAdhoc = '".$searchWord."'  ";	
            $sql2 .= " AND dtmlist.dtmAdhoc = '".$searchWord."'  ";
        }
        else if($fromPage == 21)
        {
            $sql .= " AND dtmlist.dtmAdhoc = '".$searchWord."'  ";	
            $sql2 .= " AND dtmlist.dtmAdhoc = '".$searchWord."'  ";
        }
     }
}

if ($orderBy != "") 
{
    if($filter == 1 )
    {
        $sql .= " ORDER BY costcenter.costCenterName ASC, ".$orderBy." DESC , dtmlist.dtmPickupTime ASC ";
        $sql2 .= " ORDER BY costcenter.costCenterName ASC, ".$orderBy." DESC , dtmlist.dtmPickupTime ASC ";
    }
    else if($filter == 3 || $filter == 4)
    {
        $sql .= " ORDER BY ".$orderBy." ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC ";
    }
    else if($filter == 5)
    {
        $sql .= " ORDER BY ".$orderBy." ASC , dtmlist.dtmPickupDate ASC , dtmlist.dtmPickupTime ASC ";
        $sql2 .= " ORDER BY ".$orderBy." ASC , dtmlist.dtmPickupDate ASC , dtmlist.dtmPickupTime ASC ";
    }
    else 
    {
        $sql .= " ORDER BY ".$orderBy." DESC ";
        $sql2 .= " ORDER BY ".$orderBy." DESC ";
    }
}

$sql .=" LIMIT ".$sqlPageNo.",10 ";

if($condition == 1)
{
     if($fromPage == 20)
     {
          $initialSql = "SELECT COUNT(*) as total from (((dtmlist 
                INNER JOIN company ON company.companyID_PK = dtmlist.companyID_FK)
                INNER JOIN trucks ON trucks.truckID_PK = dtmlist.truckID_FK)
                INNER JOIN costcenter ON costcenter.costCenterID_PK = dtmlist.costCenterID_FK)
                WHERE dtmlist.dtmIsTransportCharge IS NULL AND dtmlist.dtmIsFinished = 1 ";
     }
     if($fromPage == 21)
     {
          $initialSql = "SELECT COUNT(*) as total from ((((transportcharge 
            INNER JOIN dtmlist ON dtmlist.dtmID_PK = transportcharge.dtmID_FK) 
            INNER JOIN company ON company.companyID_PK = dtmlist.companyID_FK)
            INNER JOIN trucks ON trucks.truckID_PK = dtmlist.truckID_FK)
            INNER JOIN costcenter ON costcenter.costCenterID_PK = dtmlist.costCenterID_FK) 
            WHERE transportcharge.isInvoiceNoAdded LIKE 0 ";
     }

     $result = mysqli_query($conn,$initialSql);
     $data = mysqli_fetch_assoc($result);
     $no_of_pages = 0;
     $no_of_pages = ceil($data['total'] / 10);
}
else
{
     $result2 = mysqli_query($conn,$sql2);
     $dataCount = mysqli_fetch_assoc($result2);
     $no_of_pages = 0;
     $no_of_pages = ceil($dataCount['total2'] / 10);
}

$querylisting = mysqli_query($conn,$sql);

generateDeleteModal($fromPage);
generateConfirmationDeleteModal($fromPage);
?>
<?php 
if($fromPage == 21)
{
?>
<button class="btn btn-warning mb-3 text-white " onclick="checkAllCheckBox(this);" value="0">Select All Invoice</button>
<?php 
}
?>
<script>$("#pagination"+<?php echo $fromPage;?>+" option").remove();</script>
<table class="table table-sm table-hovered table-bordered  dtmTableNoWrap table-striped table-responsive-xl removebottommargin dtmTableNoWrap"style="text-align:center;">
     <thead>
          <tr>
               <?php 
                    if($fromPage == 20)
                    {
                         ?>
                              <th>Selection</th>
                              <th>DTM No</th>
                              <th>VXL D/O No</th>
                              <th>Pickup Date</th>
                              <th>Pickup Time</th>
                              <th>From</th>
                              <th>To</th>
                              <th>Agent</th>
                              <th>Cost Centre</th>
                              <th>Truck No</th>
                              <th>CAP</th>
                              <th>Consol</th>
                              <th>Driver Name</th>
                              <th>Remark</th>
                              
                         <?php
                    }
                    if($fromPage == 21)
                    {
                         ?>
                              <th>Selection/<br>Invoice No</th>
                              <th>Option</th>
                              <th>DTM No</th>
                              <th>VXL D/O No</th>
                              <th>Pickup Date</th>
                              <th>Pickup Time</th>
                              <th>Agent</th>
                              <th>Cost Centre</th>
                              <th>From</th>
                              <th>To</th>
                              <th>Truck No</th>
                              <th>CAP</th>
                              <th>Consol</th>
                              <th>Driver Name</th>
                              <th>DTM Remark</th>
                              <th>Transport Charge Remark</th>
                              <th>Operating Hour</th>
                              <th>Transport/Consol Charge</th>
                              <th>FAF %</th>
                              <th>Total</th>
                         <?php
                    }
               ?>
          </tr>
     </thead>
     <tbody>
    <?php 
        if (mysqli_num_rows($querylisting) > 0) 
        {
            while($row = mysqli_fetch_array($querylisting))
            {
    ?>
     <tr>
          <?php 
               if($fromPage == 20)
               {  
          ?>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                        <form action="transportChargesAdd.php" method="POST" class="adminformEdit">
                            <input type="hidden" name="transportchargeID" value="<?php echo $fromPage;?>">
                            <button class="btn btn-success edtOpt" name="add" value="<?php echo $row['dtmID_PK'];?>">Add T.Charges</button>
                        </form>
                    </div>
                </td>
                <td>
                    <?php 
                        echo $row['dtmID_PK'];
                    ?>
               </td>
               <td>
                    <?php 
                    $vxlDO = "";
                    if($row['dtmAdhoc'])
                    {
                        $vxlDO = $row['dtmAdhoc'];
                    }
                        echo $vxlDO;
                    ?>
                </td>
               <td>
                    <?php 
                         $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                         echo $pickupDate;
                    ?>
               </td>
               <td>
                    <?php 
                         $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                         echo $pickupTime;
                    ?>
               </td>
               <td>
                    <?php 
                        $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
               </td>
               <td>
                    <?php 
                        $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
               </td>
               <td>
                    <?php 
                        $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['companyName'];
                            }
                        }
                    ?>
               </td>
               <td><?php 
                 
               $ee = "SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$row['costCenterID_FK'];
               $e = mysqli_query($conn,$ee);
               if (mysqli_num_rows($e) > 0) 
               {
                    while($urow1 = mysqli_fetch_array($e))
                    {
                         echo $adhoc  = $urow1['costCenterName'];
                    }
               }

               ?></td>
               <td>
                    <?php 
                        $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['truckPlateNo'];
                            }
                        }
                    ?>
               </td>
               <td>
                    <?php 
                        echo $row['loadCap'];
                    ?>
               </td>
               <td>
                    <?php 
                        if($row['isConsol'] == 1)
                        {
                            echo "CONSOL";
                        }
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
               <td class="text-left">
                    <?php 
                        $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo "1.".$urow1['driverName'];
                            }
                        }

                        if($row['driver2ID_FK'])
                        {
                              $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                              $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                              if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                              {
                                   while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                   {
                                        echo "<br>2.".$urow1['driverName'];
                                   }
                              }
                        }
                    ?>
               </td>
               <td style="text-align:left;"><?php 
               if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
                   $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
                {
                    echo "->Lockup/Detention<br>->".strtoupper($row['dtmRemarks']);
                }
                else
                {
                    echo "->".strtoupper($row['dtmRemarks']);
                }
                        ?></td>
               
            <?php 
               }
            ?>
            <?php 
                if($fromPage == 21)
                {  
            ?>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                        <?php 
                        $costCenterDisplay = "SELECT invoiceNo FROM invoice WHERE transportID_FK = ".$row['id'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['invoiceNo'];
                            }
                        }
                        else
                        {
                            ?>
                            <form action="transportChargesAdd.php" method="POST" class="adminformEdit">
                                <input type="checkbox" name="tableType" value="<?php echo $row['id'];;?>">
                            </form>
                            <?php
                        }
                        ?>
                    </div>
                </td>
                <td class="adminTableWidthTD">
                    <div class="adminAlignOptionInline">
                     <?php 
                        $costCenterDisplay = "SELECT invoiceNo FROM invoice WHERE transportID_FK = ".$row['id'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                            }
                        }
                        else
                        {
                            ?>
                            <form action="transportChargesEdit.php" method="POST" class="adminformEdit">
                                <input type="hidden" name="transportchargeID" value="<?php echo $fromPage;?>">
                                <button class="btn btn-warning edtOpt" name="edit" value="<?php echo $row['id'];?>">Edit</button>
                            </form>
                            <?php
                        }
                        ?>
                    </div>
                </td>
                <td>
                    <?php 
                        echo $row['dtmID_PK'];
                    ?>
               </td>
               <td>
                    <?php 
                    $vxlDO = "";
                    if($row['dtmAdhoc'])
                    {
                        $vxlDO = $row['dtmAdhoc'];
                    }
                        echo $vxlDO;
                    ?>
                </td>
                <td>
                    <?php 
                        $pickupDate = date("d M Y",strtotime($row['dtmPickupDate']));
                        echo $pickupDate;
                    ?>
                </td>
                <td>
                    <?php 
                         $pickupTime = date("G:i",strtotime($row['dtmPickupTime']));
                         echo $pickupTime;
                    ?>
               </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT companyName FROM company WHERE companyID_PK = ".$row['companyID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['companyName'];
                            }
                        }
                    ?>
                </td>
                <td><?php 
                  
                  $ee = "SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$row['costCenterID_FK'];
                  $e = mysqli_query($conn,$ee);
                  if (mysqli_num_rows($e) > 0) 
                  {
                       while($urow1 = mysqli_fetch_array($e))
                       {
                            echo $adhoc  = $urow1['costCenterName'];
                       }
                  }
                   ?></td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmOriginPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT pointzonePlaceName FROM pointzone WHERE pointzoneID_PK = ".$row['dtmDestinationPointID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['pointzonePlaceName'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        $costCenterDisplay = "SELECT truckPlateNo FROM trucks WHERE truckID_PK = ".$row['truckID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo $urow1['truckPlateNo'];
                            }
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $row['loadCap'];
                    ?>
                </td>
                <td>
                    <?php 
                        if($row['isConsol'] == 1)
                        {
                            echo "CONSOL";
                        }
                        else
                        {
                            echo "-";
                        }
                    ?>
                </td>
                <td>
                <?php 
                        $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driverID_FK'];
                        $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                        if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                        {
                            while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                            {
                                echo "1.".$urow1['driverName'];
                            }
                        }

                        if($row['driver2ID_FK'])
                        {
                              $costCenterDisplay = "SELECT driverName FROM driver WHERE driverID_PK = ".$row['driver2ID_FK'];
                              $costCenterDisplayQuery = mysqli_query($conn,$costCenterDisplay);
                              if (mysqli_num_rows($costCenterDisplayQuery) > 0) 
                              {
                                   while($urow1 = mysqli_fetch_array($costCenterDisplayQuery))
                                   {
                                        echo "<br>2.".$urow1['driverName'];
                                   }
                              }
                        }
                    ?>
                </td>
                <td style="text-align:left;">
                    <?php if( $row['lockupf'] || $row['lockupdockTime'] || $row['lockupcompleteLoadingTime'] || $row['lockupdepartureTimeFromPost'] ||
                   $row['lockuparrivalTimeAtPost'] || $row['lockupdestinationDockTime'] || $row['lockupcompleteUnloadTime'] || $row['lockupdepartTimeFromPost'])
                {
                    echo "->Lockup/Detention<br>->".strtoupper($row['dtmRemarks']);
                }
                else
                {
                    echo "->".strtoupper($row['dtmRemarks']);
                }?>
                </td>
                <td >
                    <?php echo strtoupper($row['remarks']);?>
                </td>
                <td>
                    <?php echo $row['operatingHour'];?>
                </td>
                <td>
                    <?php 
  
                    $ee = "SELECT costCenterName FROM costcenter WHERE costCenterID_PK = ".$row['costCenterID_FK'];
                    $e = mysqli_query($conn,$ee);
                    if (mysqli_num_rows($e) > 0) 
                    {
                        while($urow1 = mysqli_fetch_array($e))
                        {
                            if($urow1['costCenterName'] == "CONSOL")
                            {
                                echo "(consol) ";
                            }
                            echo "RM ".sprintf('%0.2f',$row['transportcharge']);
                        }
                    }
                    
                    ?>
                </td>
                <td>
                    <?php 
                    
                    // echo sprintf('%0.2f',$row['faf']);
                     $fafPercentageAfter = ($row['faf'] / 100) * $row['transportcharge'];
                     echo "(".sprintf('%0.2f',$row['faf'])."%)<br>RM ". sprintf('%0.2f',$fafPercentageAfter);
                    ?>
                </td>
                <td>
                    <?php 
                        echo "RM ".sprintf('%0.2f',$row['transportcharge'] + $fafPercentageAfter);
                    ?>
                </td>
          <?php 
               }
          ?>
     </tr>
    <?php 
            }
        }
        else
        {
            if($fromPage == 21)
            {
            ?>
            <tr>
                <td colspan="20" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
            }
            else 
            {
            ?>
            <tr>
                <td colspan="14" style="text-align:center;">No Records Found</td>
            </tr>
            <?php
            }
        }
    ?>
    </tbody>
</table>
<?php
    if($condition == 1)
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$data['total']);
    }
    else
    {
        noOfPages($no_of_pages,$pageNo,$filter,$fromPage,$dataCount['total2']);
    }
?>